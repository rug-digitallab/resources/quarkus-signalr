package nl.rug.digitallab.quarkus.signalr.extension.runtime.websockets;

import io.netty.handler.codec.http.QueryStringDecoder;
import io.quarkus.arc.Arc;
import io.vertx.core.Handler;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.RoutingContext;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnectorManager;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This is a Vert.x handler for the SignalR Websocket.
 */
@JBossLog
@RequiredArgsConstructor
public class SignalRWebsocket implements Handler<RoutingContext> {

    /**
     * The name of the Upgrade Header
     */
    private static final String UPGRADE = "Upgrade";

    /**
     * The value of the Upgrade Header
     */
    private static final String WEBSOCKET = "websocket";

    private static final String RECORD_SEPARATOR = "\u001E";

    /**
     * The list of active websockets
     */
    private List<ServerWebSocket> activeWebsockets = new ArrayList<>();

    /**
     * The path of the websocket
     */
    @NonNull
    private String path;

    /**
     * The SignalR connector. Lazily created from the
     * {@link SignalRConnectorManager}.
     */
    private SignalRConnector connector;

    @Override
    public void handle(RoutingContext event) {
        // If this is not a websocket upgrade, or the request has already ended, then
        // pass it on
        if (
                !WEBSOCKET.equalsIgnoreCase(event.request().getHeader(UPGRADE))
                        || event.request().isEnded()
        ) {
            event.next();
            return;
        }

        // Handle the websocket upgrade
        event.request().toWebSocket(evt -> {
            if (!evt.succeeded()) {
                log.warn("Failed to create websocket", evt.cause());
                return;
            }

            ServerWebSocket webSocket = evt.result();
            activeWebsockets.add(webSocket);

            UUID connectionToken = getIdFromQuery(webSocket.query());
            SignalRSession session = getConnector().getSession(connectionToken);

            session.setMessageSender(message ->
                webSocket.writeTextMessage(message + RECORD_SEPARATOR)
                // This should only be used for binary messages,
                // To keep in mind when implementing binary messages over WebSockets
                // webSocket.write(Buffer.buffer(message).appendByte((byte) 0x1E));
            );

            webSocket.handler(buffer ->
                    getConnector().handleMessage(session, buffer.toString()));

            webSocket.closeHandler(handler -> {
                activeWebsockets.remove(webSocket);
                connector.handleClose(session);
            });
        });
    }

    private UUID getIdFromQuery(String query) {
        QueryStringDecoder decoder = new QueryStringDecoder(query, false);
        List<String> values = decoder.parameters().get("id");
        if (values == null || values.size() != 1) {
            throw new IllegalArgumentException("Query must contain exactly one id parameter");
        }

        String id = values.get(0);
        return UUID.fromString(id);
    }

    /**
     * Gets the SignalR connector. If one doesn't exist, it will be lazily created,
     * so this method should never return null.
     *
     * @return - The SignalR connector
     */
    public SignalRConnector getConnector() {
        if (connector == null) {
            SignalRConnectorManager connectorManager = Arc.container().instance(SignalRConnectorManager.class).get();
            this.connector = connectorManager.getConnector(path);
        }

        return connector;
    }
}
