package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.cancellation;

import io.smallrye.mutiny.subscription.Cancellable;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRPacketListener;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.CancelInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * This will handle the cancellation of an ongoing streaming invocation.
 */
@JBossLog
public class CancelInvocationHandler implements SignalRPacketListener<CancelInvocation> {

    /**
     * This will cancel the ongoing streaming invocation.
     *
     * @param session - The session that received the packet.
     * @param packet - The packet that was received.
     */
    @Override
    public void onPacketReceived(SignalRSession session, CancelInvocation packet) {
        // Figure out which invocation to cancel
        String invocationId = packet.getInvocationId();

        if (!session.getInvocationManager().hasIncomingStreamInvocation(invocationId)) {
            log.warn("Received a cancel for an invocation that was not found: " + invocationId);
            return;
        }

        // Get the invocation and cancel it
        Cancellable pendingStream = session.getInvocationManager().removeIncomingStreamInvocation(invocationId);

        // Cancel the invocation
        pendingStream.cancel();
    }
}
