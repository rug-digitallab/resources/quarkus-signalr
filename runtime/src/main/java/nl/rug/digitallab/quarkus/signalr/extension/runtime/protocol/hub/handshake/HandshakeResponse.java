package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This the handshake response that is sent to the SignalR client.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HandshakeResponse {
    /** Optionally, an error if something happens on Handshake */
    private String error;
}
