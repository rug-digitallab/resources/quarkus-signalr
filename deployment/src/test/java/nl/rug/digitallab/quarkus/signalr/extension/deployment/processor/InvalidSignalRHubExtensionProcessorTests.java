package nl.rug.digitallab.quarkus.signalr.extension.deployment.processor;

import io.quarkus.test.QuarkusUnitTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;

/**
 * This test class is used to test the SignalRHubExtensionProcessor when an invalid SignalRHub is found.
 * <p>
 * This does not test all the cases of validity (as that is done in the SignalRHubClassValidatorTests), but it is to test
 * that the Quarkus build fails when an invalid SignalRHub is found.
 */
class InvalidSignalRHubExtensionProcessorTests {

    @RegisterExtension
    static final QuarkusUnitTest config = new QuarkusUnitTest()
            .withApplicationRoot(jar ->
                    jar.addClass(InvalidSignalRHub.class)
            ).assertException(exception -> {
                assertInstanceOf(RuntimeException.class, exception);

                RuntimeException runtimeException = (RuntimeException) exception;
                assertEquals("Client methods must be abstract! Offending method: clientMethod inside of " +
                        InvalidSignalRHub.class.getName(), runtimeException.getMessage());
            });

    @Test
    @DisplayName("Should throw exception when an invalid SignalRHub is found")
    // Ignoring the warning about no assertions in the test
    @SuppressWarnings("java:S2699")
    void testSignalRHubs() {
        // Should never be called. The exception is expected to be thrown by the QuarkusUnitTest extension
    }
}

