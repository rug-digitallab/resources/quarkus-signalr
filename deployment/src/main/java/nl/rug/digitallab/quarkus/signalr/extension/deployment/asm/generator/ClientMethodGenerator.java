package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import com.fasterxml.jackson.databind.JavaType;
import io.quarkus.deployment.util.AsmUtil;
import lombok.NonNull;
import lombok.SneakyThrows;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.SignatureGenericTypesExtractor;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.exceptions.HubGenerationException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.serialization.NestedTypeReferenceBuilder;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.jboss.jandex.DotName;
import org.jboss.jandex.Type.Kind;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.util.List;

/**
 * This class is used to generate the correct bytecode for a given client
 * method.
 */
public abstract class ClientMethodGenerator {
    /**
     * This method is used to check whether this generator can handle a given type.
     *
     * @param type - The type to check
     * @return - Whether this generator can handle the type
     */
    public abstract boolean canHandle(@NonNull Type type);

    /**
     * This method is used to generate the correct bytecode for a given client
     * method. It will generate the correct instructions to invoke the method on
     * the session object.
     *
     * @param method    - The definition of the method
     * @param mv        - The method visitor to use
     * @param arguments - The arguments of the method (without the session)
     */
    public abstract void generateClientMethodBytecode(ClientMethodDefinition method,
                                                      MethodVisitor mv, Type[] arguments);

    /**
     * This will generate the correct INVOKE instruction for a given method. It will
     * also do a lookup for the method in order to ensure that the correct
     * descriptor is used.
     *
     * @param mv         - The method visitor to use
     * @param clazz      - The class to which the method belongs
     * @param methodName - The name of the method
     * @param isStatic   - Whether the method is static or not
     * @param params     - The types of the parameters of the method
     */
    protected void visitMethodInvocation(MethodVisitor mv, Class<?> clazz, String methodName, boolean isStatic,
                                         Class<?>... params) {
        try {
            Method method = clazz.getMethod(methodName, params);

            // Get the internal name of the class
            String internalName = Type.getInternalName(clazz);

            // Get the descriptor for the method
            String descriptor = Type.getMethodDescriptor(method);

            int opCode = isStatic ? Opcodes.INVOKESTATIC : Opcodes.INVOKEVIRTUAL;

            // Add the invoke instruction
            mv.visitMethodInsn(opCode, internalName, methodName, descriptor, clazz.isInterface());
        } catch (NoSuchMethodException e) {
            throw new HubGenerationException("Method " + methodName + " not found in class " + clazz.getName(), e);
        }
    }

    /**
     * This method is used to generate the bytecode for the argument array
     * of the invoke method. This will use the offset of the local variables
     * to load the arguments into the stack.
     *
     * @param mv        - The method visitor to use
     * @param arguments - The arguments to load
     */
    protected void visitArgumentArray(MethodVisitor mv, Type[] arguments) {
        // Push the size of the array, we're preparing to create
        visitIntegerPushInstruction(mv, arguments.length);

        // Create the array
        mv.visitTypeInsn(Opcodes.ANEWARRAY, Type.getInternalName(Object.class));

        // For each argument, we will push the argument to the array
        for (int i = 0; i < arguments.length; i++) {
            // Duplicate the array reference, so we can store the parameter in it
            mv.visitInsn(Opcodes.DUP);

            // Push the index, which is the current argument
            visitIntegerPushInstruction(mv, i);

            // Load the parameter into the stack
            // In this case i+2 is needed in order to access the correct index in the local
            // variables (given through the parameters)
            mv.visitVarInsn(arguments[i].getOpcode(Opcodes.ILOAD), i + 2);

            // If the argument is a primitive, we need to box it
            if (arguments[i].getSort() != Type.OBJECT && arguments[i].getSort() != Type.ARRAY) {
                // This is the internal name for a primitive which is guaranteed to be a
                // single character
                char internalName = arguments[i].getInternalName().charAt(0);

                // I -> int, J -> long, F -> float, D -> double, etc etc
                String primitiveName = AsmUtil.PRIMITIVE_DESCRIPTOR_TO_PRIMITIVE_CLASS_LITERAL.get(internalName);
                DotName primitiveDotName = DotName.createSimple(primitiveName);

                // Get the primitive type
                org.jboss.jandex.Type primitiveType = org.jboss.jandex.Type.create(primitiveDotName, Kind.PRIMITIVE);

                // We let Quarkus handle boxing for us
                AsmUtil.boxIfRequired(mv, primitiveType);
            }

            // Store the parameter in the array
            mv.visitInsn(Opcodes.AASTORE);
        }
    }

    /**
     * This method is used to generate the bytecode for a string array.
     *
     * @param mv      - The method visitor to use
     * @param toPlace - The list of strings to place in the array
     */
    protected void visitStringArray(MethodVisitor mv, List<String> toPlace) {
        visitIntegerPushInstruction(mv, toPlace.size());

        // Create the array
        mv.visitTypeInsn(Opcodes.ANEWARRAY, Type.getInternalName(String.class));

        // For each argument, we will push the argument to the array
        for (int i = 0; i < toPlace.size(); i++) {
            // Duplicate the array reference and push index
            mv.visitInsn(Opcodes.DUP);
            visitIntegerPushInstruction(mv, i);

            // Load and store the parameter
            mv.visitLdcInsn(toPlace.get(i));
            mv.visitInsn(Opcodes.AASTORE);
        }
    }

    /**
     * This method is used to generate the appropriate IPUSH instruction for a given
     * value. It will use the most optimized instruction possible, either ICONST,
     * BIPUSH or SIPUSH.
     *
     * @param mv    - The method visitor to use
     * @param value - The value to push
     */
    protected void visitIntegerPushInstruction(MethodVisitor mv, int value) {
        if (value >= -1 && value <= 5) {
            // This is possible because
            // ICONST_m1 = 2 (0x2)
            // ICONST_0 - ICONST_5 = 3 (0x3) - 8 (0x8)
            // And so, by validating the value is between -1 and 5, we can use the
            // corresponding ICONST instruction
            mv.visitInsn(Opcodes.ICONST_0 + value);
        } else if (value >= Byte.MIN_VALUE && value <= Byte.MAX_VALUE) {
            mv.visitIntInsn(Opcodes.BIPUSH, value);
        } else if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE) {
            mv.visitIntInsn(Opcodes.SIPUSH, value);
        } else {
            // Technically, here we can use a LDC instruction, but if a method has more than
            // 32767 arguments, there is probably a bigger problem.
            throw new IllegalArgumentException("Value out of range for BIPUSH or SIPUSH: " + value);
        }
    }

    /**
     * This method is used to generate the bytecode to call the invoke method
     * for a given client method.
     * <p>
     * It is expected to only be called in method which have been validated to be
     * correct, as it requires the first argument of the enclosing method to be
     * a {@link SignalRSession}.
     * <p>
     * In the end, this will place the result of the invoke method on the stack,
     * without consuming any previous values.
     *
     * @param method    - The definition of the method
     * @param mv        - The method visitor to use
     * @param arguments - The arguments of the method (without the session)
     * @param unwrapOuterGenericLayer - Whether to unwrap the outer generic layer of the return type (such as a CompletableFuture)
     */
    protected void visitSessionInvokeMethodInstruction(ClientMethodDefinition method,
                                                      MethodVisitor mv, Type[] arguments, boolean unwrapOuterGenericLayer) {
        visitSessionMethodInvocation(method, mv, arguments, false, unwrapOuterGenericLayer);
    }

    /**
     * This method is used to generate the bytecode to call the invokeStreamable
     * method for a given client method.
     * <p>
     * In the end, this will place the result of the invoke method on the stack,
     * without consuming any previous values.
     * <p>
     * It is expected to only be called in method which have been validated to be
     * correct, as it requires the first argument of the enclosing method to be
     * a {@link SignalRSession}.
     *
     * @param method                  - The definition of the method
     * @param mv                      - The method visitor to use
     * @param arguments               - The arguments of the method (without the session)
     * @param unwrapOuterGenericLayer - Whether to unwrap the outer generic layer of the return type (such as a CompletableFuture)
     */
    protected void visitSessionStreamingMethodInvocation(ClientMethodDefinition method,
                                                        MethodVisitor mv, Type[] arguments, boolean unwrapOuterGenericLayer) {
        visitSessionMethodInvocation(method, mv, arguments, true, unwrapOuterGenericLayer);
    }

    /**
     * This method is used to generate the invoke method for a given client method.
     * <p>
     * It is expected to only be called in method which have been validated to be
     * correct, as it requires the first argument of the enclosing method to be
     * a {@link SignalRSession}.
     *
     * @param method                  - The definition of the method
     * @param mv                      - The method visitor to use
     * @param arguments               - The arguments of the method (without the session)
     * @param streamable              - Whether this should generate invoke or invokeStreamable
     * @param unwrapOuterGenericLayer - Whether to unwrap the outer generic layer of the return type (such as a CompletableFuture)
     */
    @SneakyThrows(ClassNotFoundException.class) // This should never happen, unless a class disappears
    private void visitSessionMethodInvocation(ClientMethodDefinition method, MethodVisitor mv,
                                             Type[] arguments, boolean streamable, boolean unwrapOuterGenericLayer) {
        // Add an ALOAD with index 1 (since the first param is always a session)
        // and since the session is an object, we use ALOAD for references.
        // This is the object that we will call the invoke method on.
        // After this the stack looks like this: [session]
        mv.visitVarInsn(Opcodes.ALOAD, 1);

        // Add a LDC with the method name, as the first parameter to the invoke method.
        // After this the stack looks like this: [session, "methodName"]
        mv.visitLdcInsn(method.getName());

        // Check if the return type is a generic
        Type returnType = Type.getReturnType(method.getDescriptor());
        if (returnType.getSort() == Type.OBJECT && Class.forName(returnType.getClassName()).getTypeParameters().length != 0) {
            // Extract the inner generic types from the signature. This will be used to
            // build the JavaType object.
            SignatureGenericTypesExtractor extractor = new SignatureGenericTypesExtractor(method.getSignature());

            List<String> innerGeneric = unwrapOuterGenericLayer ? extractor.getInnerArguments() :
                    extractor.getArguments();

            // This will generate the bytecode to create a String[] with the inner generic
            // types. After this the stack looks like this:
            // [session, "methodName", String[java.util.Map, java.lang.String,
            // java.lang.Integer]]
            visitStringArray(mv, innerGeneric);
        } else {
            // If the return type is not a generic, we can simply use the return type
            // directly.
            // This will generate the bytecode to create a String[] with the return type.
            // After this the stack looks like this: [session, "methodName", String[java.lang.String]]
            visitStringArray(mv, List.of(returnType.getClassName()));
        }

        // Wrap in NestedTypeReferenceBuilder.buildFromNames
        // After the method call, the stack will look like this
        visitMethodInvocation(mv, NestedTypeReferenceBuilder.class, "buildFromNames", true, String[].class);
        // [session, "methodName", result of: #buildFromNames]

        // Add the arguments to the stack
        visitArgumentArray(mv, arguments);
        // After this the stack looks like this: [session, "methodName", JavaType,
        // arguments...]

        // Once the arguments are added, we can call the invoke method.
        String name = streamable ? "invokeStreamable" : "invoke";
        visitMethodInvocation(mv, SignalRSession.class, name, false, String.class, JavaType.class, Object[].class);

        // After this the stack looks like this: [CompletableFuture] or [Multi]
        // depending on whether streamable is true or false
    }
}
