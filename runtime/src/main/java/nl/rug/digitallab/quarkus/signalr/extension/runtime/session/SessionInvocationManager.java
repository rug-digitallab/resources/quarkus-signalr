package nl.rug.digitallab.quarkus.signalr.extension.runtime.session;

import com.fasterxml.jackson.databind.JavaType;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.subscription.Cancellable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub.InvocationAlreadyExistsException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub.InvocationNotFoundException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation.PlainPendingInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation.StreamablePendingInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.stream.SignalRIncomingStream;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.CancelInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class is responsible for all the invocations which are made on the session.
 * It will keep track of every currently ongoing invocation and streamable invocation.
 */
@RequiredArgsConstructor
public class SessionInvocationManager {
    /**
     * The parent session for this invocation manager.
     */
    @NonNull
    private SignalRSession parentSession;

    /**
     * This is a map of pending outgoing invocations. (Invoker: Server, Receiver: Client)
     * <p>
     * The key is the invocation ID, and the value is the {@link PlainPendingInvocation}
     * which will be completed when the invocation is completed.
     * <p>
     * The only packet which is able to trigger a creation of a pending invocation
     * is the {@link Invocation} packet. (So this does not include stream invocations)
     * <p>
     * Once the receiver replies with a {@link Completion} packet, the pending
     * invocation will be completed.
     */
    private Map<String, PlainPendingInvocation<?>> outgoingInvocations = new ConcurrentHashMap<>();

    /**
     * This is a map of pending incoming invocations. (Invoker: Client, Receiver: Server)
     * <p>
     * The key is the invocation ID, and the value is the result of the invocation.
     * <p>
     * The only packet which is able to trigger a creation of a pending invocation
     * is the {@link Invocation} packet. (So this does not include stream invocations)
     * <p>
     * Once the server is done with the invocation, it will send a {@link Completion} packet,
     * which will complete the pending invocation.
     * <p>
     * This is used only for monitoring purposes, as the actual invocation is handled by the
     * SignalRHub, and should not be used in any way for attempting to cancel or complete the invocation.
     * <p>
     * Plain invocations cannot be cancelled, and as such, there is no need to keep track of them.
     */
    private Map<String, PlainPendingInvocation<?>> incomingInvocations = new ConcurrentHashMap<>();

    /**
     * This is a map of pending outgoing streamable invocations. (Invoker: Server, Receiver: Client)
     * <p>
     * The key is the invocation ID, and the value is the result of the Multi subscription
     * (aka Cancellable) which will be completed when the invocation is completed.
     * <p>
     * The only packet which is able to trigger a creation of a pending streamable invocation
     * is the {@link StreamInvocation} packet, which gets sent when a streamable method is invoked.
     * <p>
     * Once the receiver is done with the invocation, it will send a {@link Completion} packet, which
     * will complete the subscription.
     * <p>
     * This map exists here purely for monitoring purposes as for all intents and purposes outgoing streamable
     * invocations represent themselves as incoming streams (as it's just a pipeline for the client to send items),
     * and as such are registered in the {@link SessionStreamManager} instead.
     */
    private Map<String, StreamablePendingInvocation<?>> outgoingStreamInvocations = new ConcurrentHashMap<>();

    /**
     * This is a map of pending streamable invocations. (Invoker: Client, Receiver: Server)
     * <p>
     * The key is the invocation ID, and the value is the result of the Multi subscription (aka Cancellable)
     * which will be completed when the invocation is completed.
     * <p>
     * The only packet which is able to trigger a creation of a pending streamable invocation is the
     * {@link StreamInvocation} packet, which gets sent when the client invokes a streamable method on the server.
     * <p>
     * Once the server is done with the invocation, it will send a {@link Completion} packet, which will complete the
     * subscription.
     * <p>
     * Should the client cancel the invocation, it will send a {@link CancelInvocation} packet, which will cancel the
     * subscription, and if possible, cancel the computation.
     */
    private Map<String, Cancellable> incomingStreamInvocation = new ConcurrentHashMap<>();

    /**
     * This will place a new outgoing pending invocation.
     *
     * @param invocationId           - The ID of the invocation
     * @param plainPendingInvocation - The pending invocation
     * @throws InvocationAlreadyExistsException - If there is already an invocation with the given ID
     */
    public void addOutgoingInvocation(String invocationId, PlainPendingInvocation<?> plainPendingInvocation)
            throws InvocationAlreadyExistsException {
        if (outgoingInvocations.containsKey(invocationId))
            throw new InvocationAlreadyExistsException(invocationId);

        outgoingInvocations.put(invocationId, plainPendingInvocation);
    }

    /**
     * This will remove an outgoing pending invocation.
     *
     * @param invocationId - The ID of the invocation
     * @return - The pending invocation which was removed
     * @throws InvocationNotFoundException - If there is no pending invocation with the given ID
     */
    public PlainPendingInvocation<?> removeOutgoingInvocation(String invocationId)
            throws InvocationNotFoundException {
        if (!outgoingInvocations.containsKey(invocationId))
            throw new InvocationNotFoundException(invocationId);

        return outgoingInvocations.remove(invocationId);
    }

    /**
     * This will check if there is a pending outgoing invocation with the given ID.
     *
     * @param invocationId - The ID of the invocation
     * @return - True if there is a pending invocation with the given ID
     */
    public boolean hasOutgoingInvocation(String invocationId) {
        return outgoingInvocations.containsKey(invocationId);
    }

    /**
     * This will place a new incoming pending invocation.
     *
     * @param invocationId           - The ID of the invocation
     * @param plainPendingInvocation - The pending invocation
     * @throws InvocationAlreadyExistsException - If there is already an invocation with the given ID
     */
    public void addIncomingInvocation(String invocationId, PlainPendingInvocation<?> plainPendingInvocation)
            throws InvocationAlreadyExistsException {
        if (incomingInvocations.containsKey(invocationId))
            throw new InvocationAlreadyExistsException(invocationId);

        incomingInvocations.put(invocationId, plainPendingInvocation);
    }

    /**
     * This will remove an incoming pending invocation.
     *
     * @param invocationId - The ID of the invocation
     * @return - The pending invocation which was removed
     * @throws InvocationNotFoundException - If there is no pending invocation with the given ID
     */
    public PlainPendingInvocation<?> removeIncomingInvocation(String invocationId)
            throws InvocationNotFoundException {
        if (!incomingInvocations.containsKey(invocationId))
            throw new InvocationNotFoundException(invocationId);

        return incomingInvocations.remove(invocationId);
    }

    /**
     * This will check if there is a pending incoming invocation with the given ID.
     *
     * @param invocationId - The ID of the invocation
     * @return - True if there is a pending incoming invocation with the given ID
     */
    public boolean hasIncomingInvocation(String invocationId) {
        return incomingInvocations.containsKey(invocationId);
    }

    /**
     * This will place a new pending streamable invocation.
     *
     * @param invocationId                - The ID of the invocation
     * @param streamablePendingInvocation - The pending streamable invocation
     * @throws InvocationAlreadyExistsException - If there is already an invocation with the given ID
     */
    public void addOutgoingStreamInvocation(String invocationId, StreamablePendingInvocation<?> streamablePendingInvocation)
            throws InvocationAlreadyExistsException {
        if (outgoingStreamInvocations.containsKey(invocationId))
            throw new InvocationAlreadyExistsException(invocationId);

        outgoingStreamInvocations.put(invocationId, streamablePendingInvocation);
    }

    /**
     * This will remove a pending streamable invocation.
     *
     * @param invocationId - The ID of the invocation
     * @return - The pending streamable invocation which was removed
     * @throws InvocationNotFoundException - If there is no pending streamable invocation with the given ID
     */
    public StreamablePendingInvocation<?> removeOutgoingStreamInvocation(String invocationId)
            throws InvocationNotFoundException {
        if (!outgoingStreamInvocations.containsKey(invocationId))
            throw new InvocationNotFoundException(invocationId);

        return outgoingStreamInvocations.remove(invocationId);
    }

    /**
     * This will check if there is a pending streamable invocation with the given ID.
     *
     * @param invocationId - The ID of the invocation
     * @return - True if there is a pending streamable invocation with the given ID
     */
    public boolean hasOutgoingStreamInvocation(String invocationId) {
        return outgoingStreamInvocations.containsKey(invocationId);
    }

    /**
     * This will place a new pending incoming streamable invocation.
     *
     * @param invocationId - The ID of the invocation
     * @param subscription - The subscription to the stream
     * @throws InvocationAlreadyExistsException - If there is already an invocation with the given ID
     */
    public void addIncomingStreamInvocation(String invocationId, Cancellable subscription)
            throws InvocationAlreadyExistsException {
        if (incomingStreamInvocation.containsKey(invocationId))
            throw new InvocationAlreadyExistsException(invocationId);

        incomingStreamInvocation.put(invocationId, subscription);
    }

    /**
     * This will remove a pending incoming streamable invocation.
     *
     * @param invocationId - The ID of the invocation
     * @return - The subscription which was removed
     * @throws InvocationNotFoundException - If there is no pending incoming streamable invocation with the given ID
     */
    public Cancellable removeIncomingStreamInvocation(String invocationId)
            throws InvocationNotFoundException {
        if (!incomingStreamInvocation.containsKey(invocationId))
            throw new InvocationNotFoundException(invocationId);

        return incomingStreamInvocation.remove(invocationId);
    }

    /**
     * This will check if there is a pending incoming streamable invocation with the given ID.
     *
     * @param invocationId - The ID of the invocation
     * @return - True if there is a pending incoming streamable invocation with the given ID
     */
    public boolean hasIncomingStreamInvocation(String invocationId) {
        return incomingStreamInvocation.containsKey(invocationId);
    }

    /**
     * This class represents the result once the arguments have been separated. Arguments need to be separated
     * in two categories: streamable and non-streamable, this is due to the fact that streams need to be handled
     * differently than regular arguments.
     * <p>
     * It will also assign IDs to the streams.
     */
    @Data
    @AllArgsConstructor
    private static class ArgumentSeparationResult {
        private List<Object> nonStreamableArguments;
        private List<Multi<?>> streamableArguments;
        private List<String> streamableArgumentIds;

        /**
         * This method is used to send out streams to the client
         *
         * @param parentSession - The parent session to send the streams to
         */
        private void sendStreams(SignalRSession parentSession) {
            for (int i = 0; i < streamableArguments.size(); i++) {
                parentSession.getStreamManager().sendOutStream(streamableArgumentIds.get(i), streamableArguments.get(i));
            }
        }
    }

    /**
     * This will take the parameters and separate them into arguments and streams.
     * <p>
     * Anything which is a {@link Multi} will be considered a stream, and anything
     * else will be considered an argument.
     *
     * @param params - The parameters to separate
     * @return - The arguments and streams separated (with IDs assigned to the streams)
     */
    private ArgumentSeparationResult separateArgumentsFromStreams(Object... params) {
        // Separate streamables from non-streamables
        List<Object> arguments = new ArrayList<>();
        List<Multi<?>> streamableArguments = new ArrayList<>();
        for (Object param : params) {
            if (Multi.class.isAssignableFrom(param.getClass())) {
                streamableArguments.add((Multi<?>) param);
            } else {
                arguments.add(param);
            }
        }

        // Assign ids to the streams
        List<String> streamIds = streamableArguments.stream()
                .map(multi -> parentSession.nextInvocationId() + "")
                .toList();

        return new ArgumentSeparationResult(
                arguments,
                streamableArguments,
                streamIds
        );
    }


    /**
     * This will invoke a method on the client with the given parameters.
     *
     * @param methodName - The name of the method to invoke
     * @param returnType - The return type of the method (used for deserialization)
     * @param params     - The parameters to pass to the method
     * @param <T>        - The type of the return value
     * @return - A future which will be completed when the invocation is completed
     */
    public <T> CompletableFuture<T> invoke(String methodName, JavaType returnType, Object... params) {
        // Build the packet for the invocation
        ArgumentSeparationResult separated = separateArgumentsFromStreams(params);
        long currentInvocationId = parentSession.nextInvocationId();
        Invocation invocation = new Invocation(
                currentInvocationId + "",
                methodName,
                separated.getNonStreamableArguments(),
                separated.getStreamableArgumentIds()
        );

        // Modify internal state to wait for the completion
        CompletableFuture<T> future = new CompletableFuture<>();
        PlainPendingInvocation<T> plainPendingInvocation = new PlainPendingInvocation<>(returnType, future);
        addOutgoingInvocation(invocation.getInvocationId(), plainPendingInvocation);

        // Send out invocation and streams if needed.
        parentSession.sendMessage(invocation);
        separated.sendStreams(parentSession);

        return future;
    }


    /**
     * This will invoke a streaming method on the SignalR hub.
     *
     * @param methodName - The name of the method to invoke
     * @param itemType   - The type of the items in the stream
     * @param params     - The parameters to pass to the method
     * @param <T>        - The type of the items in the stream
     * @return - A Multi which will emit the items in the stream
     */
    public <T> Multi<T> invokeStreamable(String methodName, JavaType itemType, Object... params) {
        // Build the packet for the streamable invocation
        ArgumentSeparationResult separated = separateArgumentsFromStreams(params);
        long currentInvocationId = parentSession.nextInvocationId();

        StreamInvocation invocation = new StreamInvocation(
                currentInvocationId + "",
                methodName,
                separated.getNonStreamableArguments(),
                separated.getStreamableArgumentIds()
        );

        Multi<T> resultingMulti = Multi.createFrom().emitter(emitter -> {
            // Create the stream from the emitter
            SignalRIncomingStream<?> incomingStream = new SignalRIncomingStream<>(itemType, emitter);

            // Register stream with the session (so any StreamItems are redirected to the emitter)
            parentSession.getStreamManager().newIncomingStream(currentInvocationId + "", incomingStream);
        });

        // Register the cancellation of the stream
        resultingMulti = resultingMulti.onCancellation().invoke(() -> {
            CancelInvocation cancelInvocation = new CancelInvocation(currentInvocationId + "");
            parentSession.sendMessage(cancelInvocation);
        });

        // There is no need for us to handle the completion of the stream and remove it from the session
        // because the CompletionHandler will do that for us. (And will be removed when the client sends a completion)

        // For monitoring’s sake, keep track of this as a pending streamable invocation
        StreamablePendingInvocation<T> streamablePendingInvocation = new StreamablePendingInvocation<>(itemType, resultingMulti);
        String invocationId = invocation.getInvocationId();
        addOutgoingStreamInvocation(invocationId, streamablePendingInvocation);

        // Remove the streamable invocation from the session once the stream is done
        resultingMulti = resultingMulti.onTermination().invoke(() -> {
            if (hasOutgoingStreamInvocation(invocationId)) {
                removeOutgoingStreamInvocation(invocationId);
            }
        });

        // Send out the invocation request.
        parentSession.sendMessage(invocation);
        return resultingMulti;
    }
}
