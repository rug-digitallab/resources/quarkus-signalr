package nl.rug.digitallab.quarkus.signalr.extension.deployment.util;

import com.fasterxml.jackson.databind.JavaType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class JavaTypeDataFaker {
    public static final String KNOWN_FAKE_STRING = "FakeData";
    public static final int KNOWN_FAKE_INT = 42;

    public static Object generateFakeData(JavaType requiredType) {
        if (requiredType.getRawClass().equals(String.class)) {
            return KNOWN_FAKE_STRING;
        } else if (requiredType.getRawClass().equals(Integer.class) || requiredType.getRawClass().equals(int.class)) {
            return KNOWN_FAKE_INT;
        } else if (requiredType.getRawClass().equals(List.class)) {
            // Generate 3 fake objects
            List<Object> result = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                result.add(generateFakeData(requiredType.containedType(0)));
            }
            return result;
        } else if (requiredType.getRawClass().equals(Set.class)) {
            Set<Object> result = new HashSet<>();
            // Since the generated data is identical for keys, we just generate one
            result.add(generateFakeData(requiredType.containedType(0)));
            return result;
        } else if (requiredType.getRawClass().equals(Map.class)) {
            Map<Object, Object> result = new HashMap<>();
            // Since the generated data is identical for keys, we just generate one
            result.put(generateFakeData(requiredType.containedType(0)), generateFakeData(requiredType.containedType(1)));
            return result;
        } else if (requiredType.isArrayType()) {
            // Generate 3 fake objects
            Object result = Array.newInstance(requiredType.getContentType().getRawClass(), 3);
            for (int i = 0; i < 3; i++) {
                Array.set(result, i, generateFakeData(requiredType.getContentType()));
            }
            return result;
        } else if (requiredType.getRawClass().equals(CompositeTestObject.class)) {
            return new CompositeTestObject();
        } else if (requiredType.getRawClass().equals(CompletableFuture.class)) {
            return CompletableFuture.completedFuture(generateFakeData(requiredType.containedType(0)));
        } else {
            throw new IllegalArgumentException("Unknown type: " + requiredType);
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CompositeTestObject {
        public int primitiveField = KNOWN_FAKE_INT;
        public String stringField = KNOWN_FAKE_STRING;
        private List<String> stringList = Arrays.asList(KNOWN_FAKE_STRING, KNOWN_FAKE_STRING, KNOWN_FAKE_STRING);
    }
}
