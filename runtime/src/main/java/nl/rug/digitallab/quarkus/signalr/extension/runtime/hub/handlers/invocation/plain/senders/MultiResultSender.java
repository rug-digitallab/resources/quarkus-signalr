package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain.senders;

import io.smallrye.mutiny.Multi;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * This is an adapter for the Multi type. It will send the response to the
 * client once the Multi is completed, and will send it as a list of all the
 * items which were emitted.
 */
public class MultiResultSender extends PlainInvocationResultSender<Multi<?>> {

    @Override
    public void handleResult(SignalRSession session, Invocation packet, Multi<?> methodResult) {
        methodResult.collect().asList().subscribe().with(
                result -> sendCompletion(session, packet, methodResult),
                e -> sendErroredCompletion(session, packet, e.getMessage())
        );
    }

}
