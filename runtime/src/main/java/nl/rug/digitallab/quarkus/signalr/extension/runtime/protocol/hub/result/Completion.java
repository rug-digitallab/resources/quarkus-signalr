package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacketType;

/**
 * This is the Completion message, it is used to send the result of a method
 * invocation.
 * 
 * @see <a href=
 *      "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#completion-message-encoding"
 *      >Completion message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Completion extends SignalRPacket {
    /** The invocation id */
    private String invocationId;

    /** The result */
    private Object result;

    /** The error */
    private String error;

    /**
     * Instantiates a new completion packet.
     */
    public Completion() {
        super(SignalRPacketType.COMPLETION);
    }

    /**
     * Instantiates a new completion packet.
     * 
     * @param invocationId - The invocation id
     * @param result       - The result
     * @param error        - The error
     */
    public Completion(String invocationId, Object result, String error) {
        super(SignalRPacketType.COMPLETION);
        this.invocationId = invocationId;
        this.result = result;
        this.error = error;
    }
}
