package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.senders;

import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.subscription.Cancellable;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * An adapter for {@link StreamInvocation} that uses {@link Uni} as the result type.
 */
public class StreamUniResultSender extends StreamInvocationResultSender<Uni<?>> {

    @Override
    public void handleResult(SignalRSession session, StreamInvocation packet, Uni<?> methodResult) {
        Cancellable subscription = methodResult.subscribe().with(
                result -> this.sendItemAndCompletion(session, packet, result),
                e -> this.sendCompletion(session, packet, e.getMessage())
        );

        session.getInvocationManager().addIncomingStreamInvocation(packet.getInvocationId(), subscription);
    }
}
