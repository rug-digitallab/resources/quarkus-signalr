package nl.rug.digitallab.quarkus.signalr.extension.runtime.session;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.subscription.Cancellable;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub.InvocationAlreadyExistsException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub.InvocationNotFoundException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation.PlainPendingInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation.StreamablePendingInvocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;

class SessionInvocationManagerTests {
    private SessionInvocationManager invocationManager;

    @BeforeEach
    void setup() {
        SignalRSession session = new SignalRSession(UUID.randomUUID(), UUID.randomUUID(),
                new SignalRConnector(null, null, null), new ObjectMapper(), 1);
        this.invocationManager = new SessionInvocationManager(session);
    }

    @Test
    @DisplayName("Add outgoing invocation successfully")
    void addOutgoingInvocation() throws InvocationAlreadyExistsException {
        String invocationId = "0";
        PlainPendingInvocation<?> invocation = new PlainPendingInvocation<>(null, new CompletableFuture<>());

        invocationManager.addOutgoingInvocation(invocationId, invocation);

        assertTrue(invocationManager.hasOutgoingInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if outgoing invocation already exists")
    void addOutgoingInvocationThrows() {
        String invocationId = "1";
        PlainPendingInvocation<?> invocation = new PlainPendingInvocation<>(null, new CompletableFuture<>());

        invocationManager.addOutgoingInvocation(invocationId, invocation);

        assertThrows(InvocationAlreadyExistsException.class, () ->
                invocationManager.addOutgoingInvocation(invocationId, invocation));
    }

    @Test
    @DisplayName("Remove outgoing invocation successfully")
    void removeOutgoingInvocation() throws InvocationNotFoundException, InvocationAlreadyExistsException {
        String invocationId = "2";
        PlainPendingInvocation<?> invocation = new PlainPendingInvocation<>(null, new CompletableFuture<>());

        invocationManager.addOutgoingInvocation(invocationId, invocation);
        PlainPendingInvocation<?> removedInvocation = invocationManager.removeOutgoingInvocation(invocationId);

        assertEquals(invocation, removedInvocation);
        assertFalse(invocationManager.hasOutgoingInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if outgoing invocation not found")
    void removeOutgoingInvocationThrows() {
        String invocationId = "3";

        assertThrows(InvocationNotFoundException.class, () -> {
            invocationManager.removeOutgoingInvocation(invocationId);
        });
    }

    @Test
    @DisplayName("Add incoming invocation successfully")
    void addIncomingInvocation() throws InvocationAlreadyExistsException {
        String invocationId = "4";
        PlainPendingInvocation<?> invocation = new PlainPendingInvocation<>(null, new CompletableFuture<>());

        invocationManager.addIncomingInvocation(invocationId, invocation);

        assertTrue(invocationManager.hasIncomingInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if incoming invocation already exists")
    void addIncomingInvocationThrows() {
        String invocationId = "5";
        PlainPendingInvocation<?> invocation = new PlainPendingInvocation<>(null, new CompletableFuture<>());

        invocationManager.addIncomingInvocation(invocationId, invocation);

        assertThrows(InvocationAlreadyExistsException.class, () ->
                invocationManager.addIncomingInvocation(invocationId, invocation));
    }

    @Test
    @DisplayName("Remove incoming invocation successfully")
    void removeIncomingInvocation() throws InvocationNotFoundException, InvocationAlreadyExistsException {
        String invocationId = "6";
        PlainPendingInvocation<?> invocation = new PlainPendingInvocation<>(null, new CompletableFuture<>());

        invocationManager.addIncomingInvocation(invocationId, invocation);
        PlainPendingInvocation<?> removedInvocation = invocationManager.removeIncomingInvocation(invocationId);

        assertEquals(invocation, removedInvocation);
        assertFalse(invocationManager.hasIncomingInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if incoming invocation not found")
    void removeIncomingInvocationThrows() {
        String invocationId = "7";

        assertThrows(InvocationNotFoundException.class, () -> {
            invocationManager.removeIncomingInvocation(invocationId);
        });
    }

    @Test
    @DisplayName("Add outgoing stream invocation successfully")
    void addOutgoingStreamInvocation() throws InvocationAlreadyExistsException {
        String invocationId = "8";
        StreamablePendingInvocation<?> invocation = new StreamablePendingInvocation<>(null, Multi.createFrom().empty());

        invocationManager.addOutgoingStreamInvocation(invocationId, invocation);

        assertTrue(invocationManager.hasOutgoingStreamInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if outgoing stream invocation already exists")
    void addOutgoingStreamInvocationThrows() {
        String invocationId = "9";
        StreamablePendingInvocation<?> invocation = new StreamablePendingInvocation<>(null, Multi.createFrom().empty());

        invocationManager.addOutgoingStreamInvocation(invocationId, invocation);

        assertThrows(InvocationAlreadyExistsException.class, () ->
                invocationManager.addOutgoingStreamInvocation(invocationId, invocation));
    }

    @Test
    @DisplayName("Remove outgoing stream invocation successfully")
    void removeOutgoingStreamInvocation() throws InvocationNotFoundException, InvocationAlreadyExistsException {
        String invocationId = "10";
        StreamablePendingInvocation<?> invocation = new StreamablePendingInvocation<>(null, Multi.createFrom().empty());

        invocationManager.addOutgoingStreamInvocation(invocationId, invocation);
        StreamablePendingInvocation<?> removedInvocation = invocationManager.removeOutgoingStreamInvocation(invocationId);

        assertEquals(invocation, removedInvocation);
        assertFalse(invocationManager.hasOutgoingStreamInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if outgoing stream invocation not found")
    void removeOutgoingStreamInvocationThrows() {
        String invocationId = "11";

        assertThrows(InvocationNotFoundException.class, () -> {
            invocationManager.removeOutgoingStreamInvocation(invocationId);
        });
    }

    @Test
    @DisplayName("Add incoming stream invocation successfully")
    void addIncomingStreamInvocation() throws InvocationAlreadyExistsException {
        String invocationId = "12";
        Cancellable subscription = () -> {
        };

        invocationManager.addIncomingStreamInvocation(invocationId, subscription);

        assertTrue(invocationManager.hasIncomingStreamInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if incoming stream invocation already exists")
    void addIncomingStreamInvocationThrows() {
        String invocationId = "13";
        Cancellable subscription = () -> {
        };

        invocationManager.addIncomingStreamInvocation(invocationId, subscription);

        // The second invocation should throw an exception
        assertThrows(InvocationAlreadyExistsException.class, () ->
                invocationManager.addIncomingStreamInvocation(invocationId, subscription));
    }

    @Test
    @DisplayName("Remove incoming stream invocation successfully")
    void removeIncomingStreamInvocation() throws InvocationNotFoundException, InvocationAlreadyExistsException {
        String invocationId = "14";
        Cancellable subscription = () -> {
        };

        invocationManager.addIncomingStreamInvocation(invocationId, subscription);
        Cancellable removedSubscription = invocationManager.removeIncomingStreamInvocation(invocationId);

        assertEquals(subscription, removedSubscription);
        assertFalse(invocationManager.hasIncomingStreamInvocation(invocationId));
    }

    @Test
    @DisplayName("Throw exception if incoming stream invocation not found")
    void removeIncomingStreamInvocationThrows() {
        String invocationId = "15";

        assertThrows(InvocationNotFoundException.class, () -> {
            invocationManager.removeIncomingStreamInvocation(invocationId);
        });
    }
}