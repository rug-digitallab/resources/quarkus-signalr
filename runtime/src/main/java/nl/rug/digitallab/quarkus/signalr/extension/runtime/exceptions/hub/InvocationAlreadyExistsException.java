package nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub;

import lombok.Getter;

/**
 * This exception will be triggered whenever an invocation which has an id that already exists is attempted.
 * <p>
 * If the caller takes care to check the invocation id before sending, this exception should never be triggered.
 */
@Getter
public class InvocationAlreadyExistsException extends RuntimeException {
    private final String invocationId;

    public InvocationAlreadyExistsException(String invocationId) {
        super("Invocation with id %s already exists".formatted(invocationId));
        this.invocationId = invocationId;
    }
}
