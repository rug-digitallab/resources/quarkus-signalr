package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import io.quarkus.gizmo.Gizmo;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This class is used to stress test the method generators.
 */
class StressMethodGeneratorTests {
    /**
     * This method generates a definition for a method that returns a nested list.
     * The depth of the list is specified by the depthLevel parameter.
     * <p>
     * The inner argument of the nested list is always a string.
     * <p>
     *
     * @param depthLevel - The depth of the nested list
     * @return - The generated method definition
     */
    private ClientMethodDefinition generateNestedMethod(int depthLevel) {
        String descriptor = Type.getMethodDescriptor(
                Type.getType(List.class),
                Type.getType(SignalRSession.class)
        );

        // Sanity check
        assertEquals(';', descriptor.charAt(descriptor.length() - 1));

        // Start building the method signature
        // We start by removing the last character, which is a semicolon
        StringBuilder signature = new StringBuilder(descriptor.substring(0, descriptor.length() - 1) + "<");

        // Add the nested lists (-1 because one is already present from the descriptor)
        signature.append("Ljava/util/List<".repeat(depthLevel - 1))
                .append("Ljava/lang/String;") // Add the innermost argument
                .append(">;".repeat(depthLevel)); // Close all the lists

        return new ClientMethodDefinition(
                Opcodes.ACC_PUBLIC,
                "testMethod",
                descriptor,
                signature.toString(),
                new String[0],
                List.of()
        );
    }

    @Test
    @DisplayName("Ensures the method generator works with 6 nested return arguments (enough to trigger BIPUSH)")
    void ensureGenerationWorksWithBIPUSH() {
        ClientMethodGenerator generator = new BlockingMethodGenerator();

        // Generate a method with 6 nested lists
        ClientMethodDefinition method = generateNestedMethod(6);
        MethodVisitor dummyVisitor = new MethodVisitor(Gizmo.ASM_API_VERSION) {
        };

        generator.generateClientMethodBytecode(method, dummyVisitor, new Type[0]);
    }

    @Test
    @DisplayName("Ensures the method generator works with 128 nested return arguments (enough to trigger SIPUSH)")
    void ensureGenerationWorksWithSIPUSH() {
        ClientMethodGenerator generator = new BlockingMethodGenerator();

        // Generate a method with 128 nested lists
        ClientMethodDefinition method = generateNestedMethod(128);
        MethodVisitor dummyVisitor = new MethodVisitor(Gizmo.ASM_API_VERSION) {
        };

        generator.generateClientMethodBytecode(method, dummyVisitor, new Type[0]);
    }

    @Test
    @DisplayName("Ensure that the method generator fails for more than 32768 nested return arguments")
    void ensureGenerationFailsWithTooManyArguments() {
        ClientMethodGenerator generator = new BlockingMethodGenerator();

        // Generate a method with 32769 nested lists
        ClientMethodDefinition method = generateNestedMethod(32769);
        MethodVisitor dummyVisitor = new MethodVisitor(Gizmo.ASM_API_VERSION) {
        };

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            generator.generateClientMethodBytecode(method, dummyVisitor, new Type[0]);
        });

        assertEquals("Value out of range for BIPUSH or SIPUSH: 32770", exception.getMessage());
    }
}
