package nl.rug.digitallab.quarkus.signalr.extension.deployment.processor;

import io.quarkus.test.QuarkusUnitTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

class ValidSignalRHubExtensionProcessorTests {
    @RegisterExtension
    static final QuarkusUnitTest config = new QuarkusUnitTest()
            .withApplicationRoot(jar ->
                    jar.addClass(SomeValidHub.class)
            );

    @Test
    @DisplayName("Ensure build passes successfully when a valid SignalRHub is found")
    // Ignoring the warning about no assertions in the test
    @SuppressWarnings("java:S2699")
    void testSignalRHubs() {
        // Should never be called. The test is expected to pass without any exceptions
    }
}

