package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm;

import com.fasterxml.jackson.databind.JavaType;
import io.quarkus.gizmo.Gizmo;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator.*;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.jboss.jandex.AnnotationInstance;
import org.jboss.jandex.AnnotationTarget.Kind;
import org.jboss.jandex.AnnotationValue;
import org.jboss.jandex.DotName;
import org.objectweb.asm.*;

import java.util.List;
import java.util.Set;

/**
 * This class visitor is used to visit a class and generate the necessary
 * bytecode to invoke client methods.
 * <p>
 * The order of operations is as follows:
 * <ul>
 *
 * <li>Start visiting the class</li>
 *
 * <li>If the class is abstract, remove the abstract flag (so it can be
 * instantiated as a bean)</li>
 *
 * <li>Go through all the methods annotated with {@link ClientMethod}, record
 * their existence, and remove them from the class</li>
 *
 * <li>For each recorded method, generate an identical method that simply calls
 * the {@link SignalRSession#invoke(String, JavaType, Object[])} method with the
 * parameters of the method</li>
 *
 * </ul>
 * <p>
 * We do this because there's no easy way to add an implementation to a method
 * which is abstract, so we just remove them and re-create them.
 */
public class SignalRHubClassVisitor extends ClassVisitor {

    /**
     * The list of recorded Client methods
     */
    private Set<ClientMethodDefinition> clientMethods;

    private static List<ClientMethodGenerator> availableGenerators = List.of(
            new VoidMethodGenerator(),
            new CompletableFutureMethodGenerator(),
            new UniMethodGenerator(),
            new MultiMethodGenerator(),

            // Leaving this one last, since it's essentially a default.
            new BlockingMethodGenerator());

    /**
     * Creates a new SignalRHubClassVisitor
     *
     * @param writer        - The class writer to write to
     * @param clientMethods - The list of client methods to generate invoke calls
     *                      for
     */
    public SignalRHubClassVisitor(ClassWriter writer, Set<ClientMethodDefinition> clientMethods) {
        super(Gizmo.ASM_API_VERSION, writer);
        this.clientMethods = clientMethods;
    }

    /**
     * This will remove the abstract flag from the class, so it can be instantiated
     * as a bean.
     *
     * @param version    - The class version (see Opcodes.ASM)
     * @param access     - The access flags of the class
     * @param name       - The name of the class
     * @param signature  - The signature of the class
     * @param superName  - The super class of the class
     * @param interfaces - The interfaces implemented by the class
     */
    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        // If class is abstract, remove the abstract flag
        // We can do this safely, as the build steps ensure that only classes annotated
        // with @SignalRHub are visited by this visitor.
        super.visit(version, access & ~Opcodes.ACC_ABSTRACT, name, signature, superName, interfaces);
    }

    /**
     * This will remove all abstract methods from a class, and record all methods
     * annotated with {@link ClientMethod}. The validation step should make sure
     * that the class does not contain abstract method without the
     * {@link ClientMethod}.
     *
     * @param access     - The access flags of the method
     * @param name       - The name of the method
     * @param descriptor - The descriptor of the method
     * @param signature  - The signature of the method
     * @param exceptions - The exceptions thrown by the method
     */
    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
                                     String[] exceptions) {

        // Method is abstract, remove it.
        if ((access & Opcodes.ACC_ABSTRACT) != 0) {
            // Since the class visitor will only be triggered on classes which are annotated
            // with @SignalRHub, we can safely remove the abstract methods, as they will be
            // re-created later. If there is an abstract method which was not annotated with
            // @ClientMethod, the validation step should have caught it.
            return null;
        }

        // Method is not abstract, leave untouched.
        return super.visitMethod(access, name, descriptor, signature, exceptions);
    }

    /**
     * This will be called when visitation of the class is done. This is where we
     * generate the new methods that will invoke the client methods.
     */
    @Override
    public void visitEnd() {
        // Generate the invoke calls for each client method, this will generate
        // all the removed methods as well as inherited ones.
        for (ClientMethodDefinition method : this.clientMethods) {
            generateInvokeCalls(method);
        }

        super.visitEnd();
    }

    /**
     * This adds an annotation value to the given annotation visitor. If needed, this will
     * recursively call itself for arrays or call {@link #addAnnotationParameters(AnnotationVisitor, AnnotationInstance)}
     * for nested annotations.
     *
     * @param annotationVisitor - The annotation visitor to add the value to
     * @param annotationValue   - The value to add
     */
    private void addAnnotationValue(AnnotationVisitor annotationVisitor, AnnotationValue annotationValue) {
        switch (annotationValue.kind()) {
            case BYTE, SHORT, INTEGER, CHARACTER, FLOAT, DOUBLE, LONG, BOOLEAN, STRING ->
                    annotationVisitor.visit(annotationValue.name(), annotationValue.value());

            case CLASS -> {
                org.jboss.jandex.Type classType = annotationValue.asClass();
                Type asmType = Type.getObjectType(classType.name().toString().replace(".", "/"));
                annotationVisitor.visit(annotationValue.name(), asmType);
            }

            case ENUM -> {
                String enumValue = annotationValue.asEnum();
                Type asmType = Type.getObjectType(annotationValue.asEnumType().toString().replace(".", "/"));
                annotationVisitor.visitEnum(annotationValue.name(), asmType.getDescriptor(), enumValue);
            }

            case ARRAY -> {
                List<AnnotationValue> array = annotationValue.asArrayList();
                AnnotationVisitor arrayVisitor = annotationVisitor.visitArray(annotationValue.name());
                for (AnnotationValue arrayValue : array) {
                    addAnnotationValue(arrayVisitor, arrayValue);
                }
                arrayVisitor.visitEnd();
            }

            case NESTED -> {
                AnnotationInstance nested = annotationValue.asNested();
                Type asmType = Type.getObjectType(nested.name().toString().replace(".", "/"));
                AnnotationVisitor nestedAv = annotationVisitor.visitAnnotation(annotationValue.name(), asmType.getDescriptor());
                addAnnotationParameters(nestedAv, nested);
            }

            case UNKNOWN ->
                // Do nothing, this can never happen from .kind(), only from .componentKind()
                // So theoretically, this can never be reached.
                // Since we handle arrays higher up, and empty arrays are not an issue, this should
                // never be reached.
                    throw new IllegalArgumentException("Unknown annotation value kind: " + annotationValue.kind());

        }
    }

    /**
     * This will add the parameters of an annotation to the given annotation visitor.
     *
     * @param annotationVisitor  - The annotation visitor to add the parameters to
     * @param annotationInstance - The annotation to add the parameters of
     */
    private void addAnnotationParameters(AnnotationVisitor annotationVisitor, AnnotationInstance annotationInstance) {
        for (AnnotationValue value : annotationInstance.values()) {
            addAnnotationValue(annotationVisitor, value);
        }

        annotationVisitor.visitEnd();
    }

    /**
     * This will add an annotation for a method to the given method visitor.
     *
     * @param annotationInstance - The annotation instance to add
     * @param methodVisitor      - The method visitor to add the annotation to
     */
    private void addAnnotationsForMethod(AnnotationInstance annotationInstance, MethodVisitor methodVisitor) {
        DotName annotationName = annotationInstance.name();
        Type type = Type.getObjectType(annotationName.toString().replace(".", "/"));
        AnnotationVisitor av = methodVisitor.visitAnnotation(type.getDescriptor(), true);
        addAnnotationParameters(av, annotationInstance);
    }

    /**
     * This will add the annotations for a method parameter to the given method visitor.
     *
     * @param annotationInstance - The annotation to add
     * @param methodVisitor      - The method visitor to add the annotation to
     */
    private void addAnnotationsForParameter(AnnotationInstance annotationInstance, MethodVisitor methodVisitor) {
        int parameterIndex = annotationInstance.target().asMethodParameter().position();
        Type type = Type.getObjectType(annotationInstance.name().toString().replace(".", "/"));
        AnnotationVisitor visitor = methodVisitor.visitParameterAnnotation(parameterIndex,
                type.getDescriptor(), true);
        addAnnotationParameters(visitor, annotationInstance);
    }

    /**
     * This method is used to generate the invoke calls for a client method.
     *
     * @param method - The method to generate the invoke calls for
     */
    private void generateInvokeCalls(ClientMethodDefinition method) {
        int access = method.getAccess();

        // Remove the abstract flag from the method
        access &= ~Opcodes.ACC_ABSTRACT;

        String name = method.getName();
        String descriptor = method.getDescriptor();
        String signature = method.getSignature();
        String[] exceptions = method.getExceptions();

        // Start by triggering a visit to the method, which will bring the method into
        // existence.
        MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);

        // Re-generated methods should have the same annotations as the original method
        method.getMethodAnnotations().stream()
                .filter(annotation -> annotation.target().kind() == Kind.METHOD)
                .forEach(annotation -> addAnnotationsForMethod(annotation, mv));

        // Apply annotations on method parameters
        method.getMethodAnnotations().stream()
                .filter(annotation -> annotation.target().kind() == Kind.METHOD_PARAMETER)
                .forEach(annotation -> addAnnotationsForParameter(annotation, mv));

        // Parse the arguments of the method from the descriptor
        Type[] arguments = Type.getArgumentTypes(descriptor);
        Type[] argumentsWithoutSession = new Type[arguments.length - 1];
        System.arraycopy(arguments, 1, argumentsWithoutSession, 0, argumentsWithoutSession.length);

        Type returnType = Type.getReturnType(descriptor);

        for (ClientMethodGenerator methodGenerator : availableGenerators) {
            if (methodGenerator.canHandle(returnType)) {
                methodGenerator.generateClientMethodBytecode(method, mv, argumentsWithoutSession);
                break;
            }
        }
    }

}
