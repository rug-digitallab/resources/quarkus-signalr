package nl.rug.digitallab.quarkus.signalr.extension.deployment.exceptions.validation;

/**
 * This exception is thrown when a validation error occurs during the
 * validation process of a hub.
 * <p>
 * An exception is used in order to stop the build process and provide
 * a clear error message to the user.
 */
public class HubValidationException extends RuntimeException {
    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message - the detail message.
     */
    public HubValidationException(String message) {
        super(message);
    }
}
