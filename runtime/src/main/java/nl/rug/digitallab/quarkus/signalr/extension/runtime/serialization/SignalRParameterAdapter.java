package nl.rug.digitallab.quarkus.signalr.extension.runtime.serialization;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smallrye.mutiny.Multi;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub.HubMethodInvocationException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub.HubParameterConversionException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition.SignalRHubMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.stream.SignalRIncomingStream;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SessionStreamManager;

import java.util.ArrayList;
import java.util.List;

/**
 * This class handles the adaptation of parameters from the packet to the correct types for the method signature.
 *
 * @see SignalRParameterAdapter#adaptParameters(List, List) for more information.
 */
@JBossLog
@AllArgsConstructor
public class SignalRParameterAdapter {
    private final SignalRHubMethod method;
    private final SessionStreamManager streamManager;
    private final ObjectMapper objectMapper;

    /**
     * This method will adapt the parameters from the packet to the correct types for the method signature.
     * <p>
     * This will look at the method signature and the parameters from the packet, and adapt them to the method itself.
     * In this context, by unadapted parameters, we mean parameters that come straight out of Jackson, usually as JSON
     * objects.
     * <p>
     * The algorithm will iterate over each of the method parameters, and "consume" the parameters from the packet, either
     * picking a stream or a plain parameter, depending on the method signature.
     *
     * @param unadaptedParameters - The unadapted parameters from the packet
     * @param streamIds           - The stream IDs from the packet
     * @return - The adapted parameters
     * @throws HubMethodInvocationException - If the parameters cannot be adapted
     */
    public List<Object> adaptParameters(List<Object> unadaptedParameters, @NonNull List<String> streamIds) throws HubMethodInvocationException {
        List<Object> adaptedParameters = new ArrayList<>();

        // We need to keep track of the unadapted index and the stream index, as, from the packet,
        // they come separated, and we need to adapt them to the method signature, merging them
        // together.
        int unadaptedIndex = 0;
        int streamIndex = 0;

        // Start by adapting parameters
        List<String> parameterNames = method.getParameterNames();
        List<JavaType> parameterTypes = method.getParameterTypes();
        for (int actualMethodParamIndex = 1; actualMethodParamIndex < method.getParameterCount(); actualMethodParamIndex++) {
            JavaType parameterType = parameterTypes.get(actualMethodParamIndex);
            String parameterName = parameterNames.get(actualMethodParamIndex);

            Object adaptedParameter;
            boolean allowedToBeNull = false;

            if (parameterType.getRawClass().isAssignableFrom(Multi.class)) {
                if (streamIndex >= streamIds.size()) {
                    throw new HubMethodInvocationException(method,
                            new IllegalArgumentException("Not enough stream ids, received: " + streamIds.size()));
                }

                String streamId = streamIds.get(streamIndex);
                try {
                    adaptedParameter = adaptStreamingParameter(streamId, parameterType, parameterName);
                } catch (HubParameterConversionException e) {
                    throw new HubMethodInvocationException(method, e);
                }
                streamIndex++;
            } else {
                if (unadaptedIndex >= unadaptedParameters.size()) {
                    throw new HubMethodInvocationException(method,
                            new IllegalArgumentException("Not enough plain (non-streamable) parameters. Received: "
                                    + unadaptedParameters.size()));
                }

                Object unadaptedParameter = unadaptedParameters.get(unadaptedIndex);
                if (unadaptedParameter == null) {
                    // Simply set it to null
                    adaptedParameter = null;
                    allowedToBeNull = true;
                } else {
                    try {
                        adaptedParameter = adaptPlainParameter(unadaptedParameter, parameterType, parameterName);
                    } catch (HubParameterConversionException e) {
                        throw new HubMethodInvocationException(method, e);
                    }
                }
                unadaptedIndex++;
            }

            if (adaptedParameter == null && !allowedToBeNull) {
                throw new HubMethodInvocationException(method,
                        new IllegalArgumentException("Failed to adapt parameter " + parameterName));
            }

            adaptedParameters.add(adaptedParameter);
        }

        // This should technically never happen, due to the baking process indexing methods by their argument count
        // however, it is a good idea to check for it. We do +1 because the first parameter is the session.
        if (adaptedParameters.size() + 1 != method.getParameterCount()) {
            throw new HubMethodInvocationException(method,
                    new IllegalArgumentException("Invalid number of parameters for method: " + method.getMethodName()
                            + " Expected: " + method.getParameterCount() + " Received: " + adaptedParameters.size() + 1));
        }

        // Check if all stream ids were used. Technically, we can still continue with the invocation,
        // as the previous check passed, but it is a good idea to warn the user.
        if (streamIndex != streamIds.size()) {
            log.warn("Not all stream ids were used! Method: %s, Expected: %d, Used: %d"
                    .formatted(method.getMethodName(), streamIds.size(), streamIndex));
        }

        // Check if all parameters were used. Again, we can still continue with the invocation,
        // but it is a good idea to warn the user.
        if (unadaptedIndex != unadaptedParameters.size()) {
            log.warn("Not all parameters were used! Method: %s, Expected: %d, Used: %d"
                    .formatted(method.getMethodName(), unadaptedParameters.size(), unadaptedIndex));
        }

        return adaptedParameters;
    }

    /**
     * This method will adapt a streaming parameter to the correct type for the method signature. This will create the
     * correct Multi instance, bound to an emitter which is connected to the stream. The emitter will emit a completion
     * when the stream is completed.
     * <p>
     * This method also takes care of placing the stream in the session, essentially "subscribing" to the correct packets.
     *
     * @param streamId      - The ID of the stream
     * @param parameterType - The type of the parameter
     * @param parameterName - The name of the parameter (for error messages)
     * @return - The adapted parameter (a Multi)
     * @throws HubParameterConversionException - If the parameter cannot be converted
     */
    private Multi<?> adaptStreamingParameter(String streamId, JavaType parameterType, String parameterName)
            throws HubParameterConversionException {
        // If this is a multi, we must be able to get the stream item type from the
        // param type. Having a raw multi as a parameter is not supported!
        if (parameterType.containedTypeCount() == 0) {
            throw new HubParameterConversionException("Streamable type must be parameterized",
                    method, parameterName);
        }

        JavaType multiType = parameterType.containedType(0);
        return Multi.createFrom().emitter(emitter ->
                streamManager.newIncomingStream(streamId, new SignalRIncomingStream<>(multiType, emitter))
        );
    }

    /**
     * This method handles the adaptation of a so-called plain argument, which is an argument
     * that is not a stream. This method will convert the unadapted parameter (which is a JSON
     * object) to the correct type for the method signature.
     *
     * @param unadaptedParameter - The unadapted parameter
     * @param parameterType      - The type of the parameter
     * @param parameterName      - The name of the parameter (for error messages)
     * @return - The adapted parameter
     * @throws HubParameterConversionException - If the parameter cannot be converted
     */
    private Object adaptPlainParameter(Object unadaptedParameter, JavaType parameterType, String parameterName)
            throws HubParameterConversionException {
        try {
            // Construct JavaType from parametrized type to retain generic information
            return objectMapper.convertValue(unadaptedParameter, parameterType);
        } catch (IllegalArgumentException e) {
            log.error("Failed to convert parameter", e);
            throw new HubParameterConversionException("Failed to convert parameter to type %s from object %s"
                    .formatted(parameterType.getRawClass().getName(), unadaptedParameter),
                    method, parameterName);
        }
    }

}
