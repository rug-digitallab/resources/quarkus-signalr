package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * These are the two protocol formats which are supported by the SignalR protocol.
 * <p>
 * Currently only Json is supported.
 */
@AllArgsConstructor
@Getter(onMethod_ = @JsonValue)
public enum ProtocolFormat {
    /**
     * The JSON protocol format
     */
    JSON("json"),

    /**
     * The MessagePack protocol format
     */
    MESSAGE_PACK("messagepack");

    private final String value;
}
