package nl.rug.digitallab.quarkus.signalr.extension.runtime.config;

import io.quarkus.runtime.annotations.ConfigPhase;
import io.quarkus.runtime.annotations.ConfigRoot;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * The configuration for the SignalR extension.
 */
@ConfigRoot(phase = ConfigPhase.RUN_TIME)
@ConfigMapping(prefix = "quarkus.signalr")
public interface SignalRExtensionConfig {

    /**
     * @return The interval between pings to the clients (default "5s").
     */
    @WithDefault("5s")
    String pingInterval();
}
