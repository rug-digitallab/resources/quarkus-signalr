package nl.rug.digitallab.quarkus.signalr.extension.deployment.exceptions.validation;

import lombok.Getter;
import org.jboss.jandex.ClassInfo;
import org.jboss.jandex.DotName;

/**
 * This exception is thrown in the case that a class required during the
 * validation process is missing. This is technically impossible to happen,
 * however, if it *does* happen, this exception will be thrown.
 * <p>
 * The most likely reason you find yourself here is because of a bug in
 * Jandex, or because someone tampered with the JAR file or Jandex index.
 */
@Getter
public class MissingClassHubException extends HubValidationException {
    /**
     * Fully qualified class name of the missing class.
     */
    private final String qualifiedClassName;

    /**
     * Constructs a new exception with the given class.
     *
     * @param qualifiedClassName - the fully qualified class name of the missing class.
     */
    public MissingClassHubException(String qualifiedClassName) {
        super("Class " + qualifiedClassName + " could not be found!");
        this.qualifiedClassName = qualifiedClassName;
    }

    /**
     * Constructs a new exception with the given class.
     *
     * @param classInfo - the fully qualified class name of the missing class.
     */
    public MissingClassHubException(ClassInfo classInfo) {
        this(classInfo.name());
    }

    /**
     * Constructs a new exception with the given class.
     *
     * @param classDotName - the fully qualified class name of the missing class.
     */
    public MissingClassHubException(DotName classDotName) {
        this(classDotName.toString());
    }
}
