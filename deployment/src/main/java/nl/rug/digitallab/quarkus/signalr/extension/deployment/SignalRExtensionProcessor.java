package nl.rug.digitallab.quarkus.signalr.extension.deployment;

import io.quarkus.arc.deployment.AdditionalBeanBuildItem;
import io.quarkus.arc.deployment.BeanDefiningAnnotationBuildItem;
import io.quarkus.deployment.annotations.Record;
import io.quarkus.deployment.annotations.*;
import io.quarkus.deployment.builditem.BytecodeTransformerBuildItem;
import io.quarkus.deployment.builditem.CombinedIndexBuildItem;
import io.quarkus.deployment.builditem.FeatureBuildItem;
import io.quarkus.deployment.builditem.nativeimage.ReflectiveClassBuildItem;
import io.quarkus.vertx.http.deployment.RouteBuildItem;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import jakarta.enterprise.context.ApplicationScoped;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.SignalRHubClassVisitor;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.builditems.SignalRHubBuildItem;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.builditems.SignalRHubClassValidationBuildItem;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.helper.JandexReflectionHelper;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.SignalRRecorder;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ServerMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.SignalRHub;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnectorManager;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.websockets.SignalRWebsocket;
import org.jboss.jandex.AnnotationInstance;
import org.jboss.jandex.AnnotationValue;
import org.jboss.jandex.DotName;
import org.jboss.jandex.IndexView;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import java.util.*;
import java.util.stream.Collectors;

import static nl.rug.digitallab.quarkus.signalr.extension.deployment.helper.JandexReflectionHelper.fakeQuarkusIndex;
import static nl.rug.digitallab.quarkus.signalr.extension.deployment.helper.JandexReflectionHelper.getClassTree;

/**
 * The processor for the SignalR extension.
 * <p>
 * This handles the deployment of the SignalR extension.
 */
public class SignalRExtensionProcessor {

    private static final String FEATURE = "signalr-quarkus";

    /**
     * This build step adds the SignalR feature to the Quarkus application.
     * <p>
     * It's how Quarkus knows that this extension is present (and will show in the
     * logs in the list of installed features).
     */
    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }

    private static final DotName SIGNALR_ANNOTATION = DotName.createSimple(SignalRHub.class.getName());
    private static final DotName APPLICATION_SCOPED_ANNOTATION = DotName.createSimple(ApplicationScoped.class.getName());

    /**
     * This build step ensures that everything annotated with @SignalRHub is a bean.
     * It's needed in order to be able to query hubs as beans.
     * <p>
     * This makes sure that all classes annotated with @SignalRHub are beans, and
     * can be accessed through the CDI, in case they need to be injected anywhere
     * else.
     *
     * @return - A bean defining annotation build item
     */
    @BuildStep
    BeanDefiningAnnotationBuildItem signalRHubAnnotation() {
        // This build step ensures that everything annotated with @SignalRHub is a bean
        return new BeanDefiningAnnotationBuildItem(SIGNALR_ANNOTATION, APPLICATION_SCOPED_ANNOTATION);
    }

    /**
     * This build step scans for all classes annotated with @SignalRHub and produces
     * a SignalRHubBuildItem for each one.
     * <p>
     * This will produce multiple BuildItems which other parts of the extension
     * (or other extensions) can consume for things like registration and
     * deployment. This is the only supported way for build steps to
     * interact with each other, in order to maintain build reproducibility.
     *
     * @param combinedIndexBuildItem - The combined index build item
     * @param items                  - The producer for SignalRHubBuildItem
     */
    @BuildStep
    void scanForSignalRHubs(CombinedIndexBuildItem combinedIndexBuildItem,
                            BuildProducer<SignalRHubBuildItem> items) {
        IndexView view = combinedIndexBuildItem.getIndex();

        for (AnnotationInstance instance : view.getAnnotations(SIGNALR_ANNOTATION)) {
            String className = instance.target().asClass().name().toString();

            AnnotationValue pathAnnotationValue = instance.value("value");
            if (pathAnnotationValue == null) {
                throw new IllegalStateException("Path value is required for SignalRHub annotation");
            }

            String path = pathAnnotationValue.asString();

            AnnotationValue transportsAnnotationValue = instance.value("transports");
            String[] values = null;
            if (transportsAnnotationValue != null)
                values = transportsAnnotationValue.asEnumArray();

            Set<TransportType> transportTypes = new HashSet<>();
            if (values == null || values.length == 0)
                Collections.addAll(transportTypes, TransportType.values());
            else
                Arrays.stream(values).map(TransportType::valueOf).forEach(transportTypes::add);

            items.produce(new SignalRHubBuildItem(className, path, transportTypes));

            // We need to fake the Quarkus index to remove the abstract flag from the class,
            // so it can be instantiated as a bean. This is technically a "white lie"
            // because the class is not abstract by the time runtime comes around,
            // but Jandex doesn't know that, and it won't get picked up otherwise.

            fakeQuarkusIndex(view.getClassByName(className));
        }
    }

    /**
     * This build step registers all classes which run SignalR methods to be enabled
     * for reflection, specifically for methods. This is needed to ensure
     * compatibility with native (aka GraalVM or Mandrel) as when running in native
     * JVM reflection is disabled by default for performance reasons.
     *
     * @param signalHubs - The list of SignalRHubBuildItem
     * @return - A reflective class build item
     */
    @BuildStep
    ReflectiveClassBuildItem registerHubsForReflection(List<SignalRHubBuildItem> signalHubs) {
        String[] classNames = signalHubs.stream().map(SignalRHubBuildItem::getClassName).toArray(String[]::new);
        return ReflectiveClassBuildItem.builder(classNames)
                .methods()
                .build();
    }

    /**
     * This build step registers the SignalRWebsocket class for reflection.
     *
     * @return - A reflective class build item
     */
    @BuildStep
    ReflectiveClassBuildItem markSignalRWebsocketForReflection() {
        return ReflectiveClassBuildItem.builder(SignalRWebsocket.class).methods().build();
    }

    /**
     * This build step registers {@link SignalRConnectorManager} as an unremovable
     * bean. This is needed to ensure that the connector manager is always present
     * in the application.
     *
     * @return - An additional bean build item
     */
    @BuildStep
    AdditionalBeanBuildItem markConnectorBeanAsUnremovable() {
        return AdditionalBeanBuildItem.unremovableOf(SignalRConnectorManager.class);
    }

    /**
     * This build step validates all SignalR hub classes, and makes sure that no
     * abstract methods are present without the {@link ClientMethod} annotation,
     * and that all methods annotated with {@link ClientMethod} are valid.
     * <p>
     * It also ensures that all methods annotated with {@link ServerMethod} are
     * valid.
     *
     * @param combinedIndexBuildItem - The combined index build item
     * @param signalHubs             - The list of SignalRHubBuildItem
     */
    @BuildStep
    @Produce(SignalRHubClassValidationBuildItem.class)
    void validateSignalRHubClasses(CombinedIndexBuildItem combinedIndexBuildItem,
                                   List<SignalRHubBuildItem> signalHubs) {
        IndexView indexView = combinedIndexBuildItem.getIndex();

        for (SignalRHubBuildItem item : signalHubs) {
            String className = item.getClassName();

            Set<DotName> classTree = getClassTree(DotName.createSimple(className), indexView);
            SignalRHubClassValidator validator = new SignalRHubClassValidator(className, indexView, classTree,
                    getClientMethodDefinitions(className, indexView));
            validator.runValidation();
        }
    }

    /**
     * This build step mutates the hub class to remove abstract methods and mutate
     * all methods annotated with {@link ClientMethod}.
     *
     * @param combinedIndexBuildItem - The combined index build item
     * @param signalHubs             - The list of SignalRHubBuildItem
     * @param classProducer          - The class producer, used to tell Quarkus to
     *                               swap out the bytecode
     */
    @BuildStep
    @Consume(SignalRHubClassValidationBuildItem.class)
    void mutateHubClass(CombinedIndexBuildItem combinedIndexBuildItem, List<SignalRHubBuildItem> signalHubs,
                        BuildProducer<BytecodeTransformerBuildItem> classProducer) {

        IndexView view = combinedIndexBuildItem.getIndex();

        for (SignalRHubBuildItem item : signalHubs) {
            // Collect methods needed to be generated
            String className = item.getClassName();
            Set<ClientMethodDefinition> methods = getClientMethodDefinitions(className, view);

            BytecodeTransformerBuildItem transformItem = new BytecodeTransformerBuildItem.Builder()
                    .setClassToTransform(className)
                    .setCacheable(true)
                    .setContinueOnFailure(false)
                    .setInputTransformer((className1, originalBytecode) -> {
                        ClassReader reader = new ClassReader(originalBytecode);
                        ClassWriter writer = new ClassWriter(reader,
                                ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
                        reader.accept(new SignalRHubClassVisitor(writer, methods), ClassReader.EXPAND_FRAMES);
                        return writer.toByteArray();
                    })
                    .setPriority(1000)
                    .build();

            classProducer.produce(transformItem);
        }
    }

    /**
     * This method retrieves all client methods from a class and its superclasses.
     *
     * @param className - The name of the class
     * @param view      - The index view
     * @return - A set of client method definitions
     */
    private Set<ClientMethodDefinition> getClientMethodDefinitions(String className, IndexView view) {
        Set<DotName> classTree = getClassTree(DotName.createSimple(className), view);
        return classTree.stream()
                .map(view::getClassByName)
                .map(JandexReflectionHelper::extractClientMethodsFrom)
                .flatMap(Set::stream).collect(Collectors.toSet());
    }

    /**
     * This build step registers the bean deployment for all SignalR hubs.
     *
     * @param signalHubs - The list of SignalRHubBuildItem
     * @param recorder   - The SignalRRecorder instance
     */
    @BuildStep
    @Record(ExecutionTime.RUNTIME_INIT)
    void registerBeanDeployment(List<SignalRHubBuildItem> signalHubs, SignalRRecorder recorder) {
        for (SignalRHubBuildItem hub : signalHubs) {
            recorder.addHub(hub.getPath(), hub.getClassName(), hub.getTransports());
        }
    }

    /**
     * This build step registers all SignalR hubs for deployment, and sets up
     * all the endpoints required depending on the transports.
     * <p>
     * This step runs into static initialization, ensuring that discovery of
     * hubs happens during build time.
     *
     * @param signalHubs - The list of SignalRHubBuildItem
     * @param recorder   - The SignalRRecorder instance
     * @param routes     - The route build item producer
     */
    @BuildStep
    @Record(ExecutionTime.RUNTIME_INIT)
    void runHTTPEndpointDeployment(List<SignalRHubBuildItem> signalHubs,
                                   SignalRRecorder recorder, BuildProducer<RouteBuildItem> routes) {
        for (SignalRHubBuildItem hub : signalHubs) {
            boolean requiresPost = hub.getTransports().stream().anyMatch(TransportType::isRequiresPostEndpoint);
            String path = hub.getPath();

            // Register the negotiation endpoint
            Handler<RoutingContext> negotiationHandler = recorder.createNegotiationHandler(path);
            RouteBuildItem negotiateRouteItem = RouteBuildItem.builder()
                    .orderedRoute(path + "/negotiate", Integer.MAX_VALUE)
                    .handler(negotiationHandler)
                    .displayOnNotFoundPage()
                    .build();

            routes.produce(negotiateRouteItem);

            if (requiresPost) {
            }

            if (hub.getTransports().contains(TransportType.WEB_SOCKETS)) {
                Handler<RoutingContext> handler = recorder.createWebsocketHandler(hub.getPath());
                RouteBuildItem websocketRouteItem = RouteBuildItem.builder()
                        .orderedRoute(hub.getPath(), Integer.MAX_VALUE)
                        .handler(handler)
                        .build();

                routes.produce(websocketRouteItem);
            }

            // Check if SSE is in there
            if (hub.getTransports().contains(TransportType.SERVER_SENT_EVENTS)) {
            }

            // // Check if long polling is in there
            if (hub.getTransports().contains(TransportType.LONG_POLLING)) {
                // Handle long polling deployment
                // TODO: Implement long polling
            }
        }
    }
}
