package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition.SignalRHubMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRPacketListener;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.container.BaseResultSenderContainer;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.senders.*;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This class is responsible for handling {@link StreamInvocation} packets, and
 * will invoke the method on the hub instance, while then streaming the result
 * to the client.
 */
@JBossLog
public class StreamInvocationHandler
        extends BaseResultSenderContainer<StreamInvocation, StreamInvocationResultSender<?>>
        implements SignalRPacketListener<StreamInvocation> {

    /**
     * Creates a new StreamInvocationHandler.
     * <p>
     * This will initialize the default result senders for the different result
     * types.
     */
    public StreamInvocationHandler() {
        this.registerResultSender(CompletableFuture.class, new StreamCompletableFutureResultSender());
        this.registerResultSender(Uni.class, new StreamUniResultSender());
        this.registerResultSender(Multi.class, new StreamMultiResultSender());
        this.registerResultSender(Object.class, new DefaultStreamResultSender());
    }

    /**
     * This will handle the received {@link StreamInvocation} packet.
     * <p>
     * It will look up the correct method in the hub instance and invoke it with
     * the arguments from the packet. It will then invoke the correct
     * {@link StreamInvocationResultSender} to send the result back to the client.
     */
    @Override
    public void onPacketReceived(SignalRSession session, StreamInvocation packet) {
        SignalRConnector connector = session.getConnector();
        Object hubInstance = connector.getSignalRHubInstance();
        List<Object> arguments = packet.getArguments();

        String invocationId = packet.getInvocationId();

        if (invocationId == null) {
            throw new IllegalStateException(
                    "Invocation ID for Stream Invocation is null! Stream Invocations cannot be marked as non-blocking!");
        }

        String methodName = packet.getTarget();
        int argumentCount = arguments.size() + packet.getStreamCount();

        SignalRHubMethod method = connector.getHubDefinition().lookupMethod(methodName, argumentCount);

        log.debug("Invoking streaming method: " + methodName + " on " + hubInstance.getClass().getName() + " with "
                + arguments.size() + " arguments and " + packet.getStreamCount() + " streams");

        if (method == null) {
            Completion completion = new Completion(packet.getInvocationId(), null, "Method not found");
            session.sendMessage(completion);

            log.warn("Client requested non-existent method: " + methodName + " on " + hubInstance.getClass().getName()
                    + " with " + arguments.size() + " arguments and " + packet.getStreamCount() + " streams");
            return;
        }

        invokeStreamMethod(session, packet, connector, arguments, method);
    }

    /**
     * Invokes the streaming method on the hub instance and sends the result back to
     * the client.
     *
     * @param session   - The session that originated the invocation.
     * @param packet    - The invocation packet.
     * @param connector - The connector that originated the invocation.
     * @param arguments - The arguments for the method.
     * @param method    - The method to invoke.
     */
    private void invokeStreamMethod(SignalRSession session, StreamInvocation packet, SignalRConnector connector,
                                    List<Object> arguments, SignalRHubMethod method) {
        try {
            // Set up a preliminary Cancellable. This cancellable will exist until the method
            // is finished executing, at which point it will be replaced by the actual
            // Cancellable that will be returned by the method.

            // We simply use an empty function as a placeholder, since we don't need to do
            // anything with it. Also, it's a ConcurrentHashMap so we can't put null.
            session.getInvocationManager().addIncomingStreamInvocation(packet.getInvocationId(), () -> {
                // This is empty since there isn't much we can do to actually cancel the
                // computation. Things which can actually be cancelled will overwrite this
                // placeholder.
            });


            Object result = connector.invokeOnHubInstance(method, session, arguments, packet.getStreamIds());

            // Check if while invoking the method, the client cancelled the invocation.
            if (session.getInvocationManager().hasIncomingStreamInvocation(packet.getInvocationId())) {
                @SuppressWarnings("unchecked")
                StreamInvocationResultSender<Object> resultSender = (StreamInvocationResultSender<Object>)
                        this.lookupResultSender(result);
                resultSender.handleResult(session, packet, result);
            }
        } catch (Exception e) {
            handleStreamError(session, packet, e);
        }
    }

    private void handleStreamError(SignalRSession session, StreamInvocation packet, Exception e) {
        log.error("Error invoking method", e);

        try {
            Completion completion = new Completion(packet.getInvocationId(), null, e.getMessage());
            session.sendMessage(completion);
        } catch (RuntimeException ex) {
            log.error("Error sending completion", ex);
        } finally {
            // Wipe the ongoing invocation, since it errored out.
            if (session.getInvocationManager().hasIncomingStreamInvocation(packet.getInvocationId())) {
                session.getInvocationManager().removeIncomingStreamInvocation(packet.getInvocationId());
            }
        }
    }

}
