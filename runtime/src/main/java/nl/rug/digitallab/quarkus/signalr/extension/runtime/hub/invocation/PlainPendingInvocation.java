package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation;


import com.fasterxml.jackson.databind.JavaType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.CompletableFuture;

/**
 * This class represents a pending invocation that is waiting for a response.
 *
 * @param <T> - The type of the result of the invocation.
 */
@Getter
@AllArgsConstructor
public class PlainPendingInvocation<T> {
    private JavaType type;
    private CompletableFuture<T> future;
}
