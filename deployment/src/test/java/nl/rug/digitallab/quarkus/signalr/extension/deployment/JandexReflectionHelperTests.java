package nl.rug.digitallab.quarkus.signalr.extension.deployment;

import io.quarkus.gizmo.Gizmo;
import lombok.SneakyThrows;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.helper.JandexReflectionHelper;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.hamcrest.Matchers;
import org.jboss.jandex.ClassInfo;
import org.jboss.jandex.DotName;
import org.jboss.jandex.Index;
import org.jboss.jandex.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.objectweb.asm.signature.SignatureReader;
import org.objectweb.asm.signature.SignatureVisitor;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class JandexReflectionHelperTests {

    private Index index;

    @BeforeEach
    @SneakyThrows(IOException.class)
    void setupIndex() {
        index = Index.of(ClientMethodClass.class, SomeDetectableException.class, ClientMethodClassWithGenerics.class);
    }

    @Test
    @DisplayName("Ensure that the client methods are extracted from the class")
    void testClientMethodExtraction() {
        Set<ClientMethodDefinition> clientMethods = JandexReflectionHelper
                .extractClientMethodsFrom(index.getClassByName(ClientMethodClass.class));

        assertEquals(1, clientMethods.size());

        ClientMethodDefinition definition = clientMethods.iterator().next();

        assertEquals("clientMethod", definition.getName());
        assertEquals(Modifier.PUBLIC | Modifier.ABSTRACT, definition.getAccess());

        String exceptionName = org.objectweb.asm.Type.getInternalName(SomeDetectableException.class);
        assertThat(definition.getExceptions(), arrayContaining(exceptionName));
        assertEquals(1, definition.getExceptions().length);

        String expectedDescriptor = index.getClassByName(ClientMethodClass.class).method("clientMethod",
                Type.create(SignalRSession.class)).descriptor();

        assertEquals(expectedDescriptor, definition.getDescriptor());

        String expectedSignature = index.getClassByName(ClientMethodClass.class).method("clientMethod",
                Type.create(SignalRSession.class)).genericSignature();

        assertEquals(expectedSignature, definition.getSignature());
    }

    @Test
    @DisplayName("Ensure that generic information is kept when extracting client methods")
    void testClientMethodExtractionWithGenerics() {
        Set<ClientMethodDefinition> clientMethods = JandexReflectionHelper
                .extractClientMethodsFrom(index.getClassByName(ClientMethodClassWithGenerics.class));
        ClientMethodDefinition definition = clientMethods.iterator().next();

        AtomicBoolean foundString = new AtomicBoolean(false);
        AtomicBoolean foundInteger = new AtomicBoolean(false);

        SignatureReader reader = new SignatureReader(definition.getSignature());
        reader.accept(new SignatureVisitor(Gizmo.ASM_API_VERSION) {
            @Override
            public void visitClassType(String name) {
                if (name.equals("java/lang/String")) {
                    foundString.set(true);
                } else if (name.equals("java/lang/Integer")) {
                    foundInteger.set(true);
                }
            }
        });

        // Since the only place in the method that contains a string or an int are the generic types,
        // we can safely assume that the generic types are kept
        assertTrue(foundString.get());
        assertTrue(foundInteger.get());
    }

    @Test
    @DisplayName("Ensure that the Quarkus index is faked correctly")
    void testFakeQuarkusIndex() {
        ClassInfo info = index.getClassByName(ClientMethodClass.class);
        short flags = info.flags();
        assertTrue(Modifier.isAbstract(flags));

        JandexReflectionHelper.fakeQuarkusIndex(info);

        short newFlags = info.flags();
        assertFalse(Modifier.isAbstract(newFlags));
    }

    @Test
    @DisplayName("Ensure class tree is correctly detected")
    @SneakyThrows(IOException.class)
        // For the Index.of method
    void testClassTreeDetection() {
        Index treeIndex = Index.of(SomeClass.class, SomeInterface.class, SomeSubClass.class, SomeSubSubClass.class,
                SomeSubSubSubClass.class);

        // Leaf class
        DotName someSubClass = DotName.createSimple(SomeSubClass.class.getName());

        // Make sure everything above (and including) the leaf class is detected
        Set<DotName> classes = JandexReflectionHelper.getClassTree(someSubClass, treeIndex);

        assertThat(classes, containsInAnyOrder(
                DotName.createSimple(SomeClass.class.getName()),
                DotName.createSimple(SomeSubClass.class.getName()),
                DotName.createSimple(SomeInterface.class.getName())
        ));

        assertThat(classes, not(contains(DotName.createSimple(SomeSubSubClass.class.getName()))));


        DotName someSubSubSubClass = DotName.createSimple(SomeSubSubSubClass.class.getName());
        classes = JandexReflectionHelper.getClassTree(someSubSubSubClass, treeIndex);

        assertThat(classes, Matchers.containsInAnyOrder(
                DotName.createSimple(SomeSubSubSubClass.class.getName()),
                DotName.createSimple(SomeSubSubClass.class.getName()),
                DotName.createSimple(SomeSubClass.class.getName()),
                DotName.createSimple(SomeClass.class.getName()),
                DotName.createSimple(SomeInterface.class.getName())
        ));
    }

    @Test
    @DisplayName("Ensure object class is alone in the class tree")
    @SneakyThrows(IOException.class)
    void testObjectClassTreeDetection() {
        Index objectIndex = Index.of(Object.class);

        Set<DotName> classes = JandexReflectionHelper.getClassTree(DotName.createSimple(Object.class.getName()), objectIndex);

        assertThat(classes, contains(DotName.createSimple(Object.class.getName())));
        assertThat(classes, hasSize(1));
    }

    @Test
    @DisplayName("Ensure that the class can never be instantiated")
    @SneakyThrows(ReflectiveOperationException.class)
    void testCannotInstantiate() {
        Constructor<JandexReflectionHelper> constructor = JandexReflectionHelper.class.getDeclaredConstructor();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));

        constructor.setAccessible(true);

        assertThrows(InvocationTargetException.class, constructor::newInstance);
    }

    public interface SomeInterface {
    }

    static class SomeDetectableException extends Exception {
    }

    abstract static class ClientMethodClass {
        @ClientMethod
        public abstract void clientMethod(SignalRSession session) throws SomeDetectableException;

        public void nonClientMethod() {
            // Do nothing
        }
    }

    abstract static class ClientMethodClassWithGenerics {
        @ClientMethod
        public abstract Set<Integer> clientMethod(SignalRSession session, List<String> someArgument) throws SomeDetectableException;

        public void nonClientMethod() {
            // Do nothing
        }
    }

    public static class SomeClass {
    }

    public static class SomeSubClass extends SomeClass implements SomeInterface {
    }

    public static class SomeSubSubClass extends SomeSubClass {
    }

    public static class SomeSubSubSubClass extends SomeSubSubClass {
    }
}
