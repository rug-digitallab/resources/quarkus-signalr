package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.container;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * Classes implementing this interface are responsible for serving the response
 * to the client once the specific type of invocation has happened on the
 * server.
 * 
 * @param <P> - The type of the packet that initiated the invocation.
 * @param <T> - The type of the result of the invocation.
 */
public interface BaseInvocationResultSender<P extends SignalRPacket, T> {
    /**
     * This method will be called when the invocation has happened on the server.
     * <p>
     * It's expected that any implementations of this method will notify the client
     * about the completion of the invocation in some manner, as the server will
     * make no further attempts to do so.
     * 
     * @param session      - The session that originated the invocation.
     * @param packet       - The invocation packet.
     * @param methodResult - The result of the invocation.
     */
    void handleResult(SignalRSession session, P packet, T methodResult);
}