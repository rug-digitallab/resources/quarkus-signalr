package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.Ack;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.CloseMessage;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.Ping;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacketType;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake.Sequence;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.CancelInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;
import org.json.JSONException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PacketSerializationTests {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @DisplayName("Serialization of Invocation packet should yield correct JSON")
    void testInvocationSerialization() throws JSONException {
        Invocation invocation = new Invocation("123", "method", List.of("arg1", 2, true), List.of("1", "2"));
        String json = objectMapper.valueToTree(invocation).toString();

        String expected = """
                                {
                                  "type": 1,
                                  "invocationId": "123",
                                  "target": "method",
                                  "arguments": [
                                    "arg1",
                                    2,
                                    true
                                  ],
                                    "streamIds": ["1", "2"]
                                }
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of Invocation packet should yield correct object")
    void testInvocationDeserialization() throws JsonProcessingException {
        String json = """
                        {
                          "type": 1,
                          "invocationId": "123",
                          "target": "method",
                          "arguments": [
                            "arg1",
                            2,
                            true
                          ],
                           "streamIds": ["1", "2"]
                        }
                """;

        Invocation invocation = objectMapper.readValue(json, Invocation.class);

        assertEquals("123", invocation.getInvocationId());
        assertEquals("method", invocation.getTarget());
        assertEquals(List.of("arg1", 2, true), invocation.getArguments());
        assertEquals(List.of("1", "2"), invocation.getStreamIds());
    }

    @Test
    @DisplayName("Serialization of StreamItem packet should yield correct JSON")
    void testStreamItemSerialization() throws JSONException {
        StreamItem streamItem = new StreamItem("123", "testData");
        String json = objectMapper.valueToTree(streamItem).toString();

        String expected = """
                {"type":2, "invocationId":"123", "item":"testData"}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of StreamItem packet should yield correct object")
    void testStreamItemDeserialization() throws JsonProcessingException {
        String json = """
                {"type":2, "invocationId":"123", "item":"testData"}
                """;

        StreamItem streamItem = objectMapper.readValue(json, StreamItem.class);

        assertEquals("123", streamItem.getInvocationId());
        assertEquals("testData", streamItem.getItem());
    }

    @Test
    @DisplayName("Serialization of Completion packet should yield correct JSON")
    void testCompletionSerialization() throws JSONException {
        Completion completion = new Completion("789", "resultData", "errorMsg");
        String json = objectMapper.valueToTree(completion).toString();

        String expected = """
                {"type":3, "invocationId":"789", "result":"resultData", "error":"errorMsg"}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of Completion packet should yield correct object")
    void testCompletionDeserialization() throws JsonProcessingException {
        String json = """
                {"type":3, "invocationId":"789", "result":"resultData", "error":"errorMsg"}
                """;

        Completion completion = objectMapper.readValue(json, Completion.class);

        assertEquals("789", completion.getInvocationId());
        assertEquals("resultData", completion.getResult());
        assertEquals("errorMsg", completion.getError());
    }

    @Test
    @DisplayName("Serialization of StreamInvocation packet should yield correct JSON")
    void testStreamInvocationSerialization() throws JSONException {
        StreamInvocation streamInvocation = new StreamInvocation("123", "targetMethod", List.of("arg1", "arg2"), List.of("stream1"));
        String json = objectMapper.valueToTree(streamInvocation).toString();

        String expected = """
                {"type":4, "invocationId":"123", "target":"targetMethod", "arguments":["arg1", "arg2"], "streamIds":["stream1"]}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of StreamInvocation packet should yield correct object")
    void testStreamInvocationDeserialization() throws JsonProcessingException {
        String json = """
                {"type":4, "invocationId":"123", "target":"targetMethod", "arguments":["arg1", "arg2"], "streamIds":["stream1"]}
                """;

        StreamInvocation streamInvocation = objectMapper.readValue(json, StreamInvocation.class);

        assertEquals("123", streamInvocation.getInvocationId());
        assertEquals("targetMethod", streamInvocation.getTarget());
        assertEquals(List.of("arg1", "arg2"), streamInvocation.getArguments());
        assertEquals(List.of("stream1"), streamInvocation.getStreamIds());
    }


    @Test
    @DisplayName("Serialization of CancelInvocation packet should yield correct JSON")
    void testCancelInvocationSerialization() throws JSONException {
        CancelInvocation cancelInvocation = new CancelInvocation("456");
        String json = objectMapper.valueToTree(cancelInvocation).toString();

        String expected = """
                {"type":5, "invocationId":"456"}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of CancelInvocation packet should yield correct object")
    void testCancelInvocationDeserialization() throws JsonProcessingException {
        String json = """
                {"type":5, "invocationId":"456"}
                """;

        CancelInvocation cancelInvocation = objectMapper.readValue(json, CancelInvocation.class);

        assertEquals("456", cancelInvocation.getInvocationId());
    }

    @Test
    @DisplayName("Serialization of Ping packet should yield correct JSON")
    void testPingSerialization() throws JSONException {
        Ping ping = new Ping();
        String json = objectMapper.valueToTree(ping).toString();

        String expected = """
                {"type":6}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of Ping packet should yield correct object")
    void testPingDeserialization() throws JsonProcessingException {
        String json = """
                {"type":6}
                """;

        Ping ping = objectMapper.readValue(json, Ping.class);

        assertEquals(SignalRPacketType.PING, ping.getType());
    }

    @Test
    @DisplayName("Serialization of CloseMessage packet should yield correct JSON")
    void testCloseMessageSerialization() throws JSONException {
        CloseMessage closeMessage = new CloseMessage("Error occurred", true);
        String json = objectMapper.valueToTree(closeMessage).toString();

        String expected = """
                {"type":7, "error":"Error occurred", "allowReconnect":true}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of CloseMessage packet should yield correct object")
    void testCloseMessageDeserialization() throws JsonProcessingException {
        String json = """
                {"type":7, "error":"Error occurred", "allowReconnect":true}
                """;

        CloseMessage closeMessage = objectMapper.readValue(json, CloseMessage.class);

        assertEquals("Error occurred", closeMessage.getError());
        assertTrue(closeMessage.isAllowReconnect());
    }

    @Test
    @DisplayName("Serialization of Ack packet should yield correct JSON")
    void testAckSerialization() throws JSONException {
        Ack ack = new Ack(42);
        String json = objectMapper.valueToTree(ack).toString();

        String expected = """
                {"type":8, "sequenceId":42}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of Ack packet should yield correct object")
    void testAckDeserialization() throws JsonProcessingException {
        String json = """
                {"type":8, "sequenceId":42}
                """;

        Ack ack = objectMapper.readValue(json, Ack.class);

        assertEquals(42, ack.getSequenceId());
    }


    @Test
    @DisplayName("Serialization of Sequence packet should yield correct JSON")
    void testSequenceSerialization() throws JSONException {
        Sequence sequence = new Sequence(99);
        String json = objectMapper.valueToTree(sequence).toString();

        String expected = """
                {"type":9, "sequenceNumber":99}
                """;

        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    @DisplayName("Deserialization of Sequence packet should yield correct object")
    void testSequenceDeserialization() throws JsonProcessingException {
        String json = """
                {"type":9, "sequenceNumber":99}
                """;

        Sequence sequence = objectMapper.readValue(json, Sequence.class);

        assertEquals(99, sequence.getSequenceNumber());
    }


}
