package nl.rug.digitallab.quarkus.signalr.extension.deployment.processor;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ServerMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.SignalRHub;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

@SignalRHub("/chat")
public abstract class SomeValidHub {
    @ServerMethod
    public void someMethod(SignalRSession session, int a) {

    }

    @ServerMethod
    public void someOtherMethod(SignalRSession session) {

    }

    @ClientMethod
    public abstract void someClientMethod(SignalRSession session);
}