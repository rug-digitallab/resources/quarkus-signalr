package nl.rug.digitallab.quarkus.signalr.extension.deployment;

import lombok.AllArgsConstructor;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.exceptions.validation.HubValidationException;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.exceptions.validation.MissingClassHubException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ServerMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.SignalRHub;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.jboss.jandex.*;
import org.objectweb.asm.Opcodes;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class handles the validation of the SignalR hub classes.
 * <p>
 * The following rules are enforced:
 * <ul>
 *
 * <li>SignalR hub classes cannot be interfaces</li>
 *
 * <li>Server methods must have a {@link SignalRSession} as their first
 * argument</li>
 * <li>Server methods must not be abstract</li>
 * <li>Server methods must be public</li>
 *
 * <li>Client methods must have a {@link SignalRSession} as their first
 * argument</li>
 * <li>Client methods must be abstract</li>
 * <li>Client methods must be public</li>
 *
 * <li>Methods that are abstract in the hub class must be annotated with
 * {@link ClientMethod}</li>
 *
 * <li>Client methods and server methods cannot be overloaded</li>
 *
 * </ul>
 * <p>
 * Since the hub class is abstract, the compiler will not complain if an
 * inherited abstract method not annotated with {@link ClientMethod} is not
 * implemented, however, that will fail at runtime.
 * <p>
 * Any method lacking implementation (either from an abstract class or an
 * interface) annotated with {@link ClientMethod} is guaranteed to be
 * provided an implementation by the SignalR extension.
 */
@AllArgsConstructor
public class SignalRHubClassValidator {

    /**
     * The class being validated, used for error messages
     */
    private String ownerClass;

    /**
     * The index view used to collect metadata
     */
    private IndexView indexView;

    /**
     * The list of classes present in the hierarchy of the hub. Does not include
     * children of the hub.
     */
    private Set<DotName> classTree;

    /**
     * The list of client methods extracted from the hub class.
     */
    private Set<ClientMethodDefinition> extractedMethods;

    /**
     * This will run the all the validation steps on the given locations.
     */
    public void runValidation() {
        this.ensureHubIsNotInterface();
        this.ensureHubIsLeaf();
        this.ensureClientMethodsAreAbstract();
        this.ensureNoMethodHasBothAnnotations();
        this.ensureAbstractMethodsInHubAreClientMethods();
        this.validateArgumentOfClientAndServerMethods();
        this.ensureServerMethodsArePublic();
        this.ensureClientMethodsArePublic();

        this.ensureBothClientAndServerMethodsAreNotOverloaded();
    }

    /**
     * Ensures that the hub class is not an interface.
     */
    private void ensureHubIsNotInterface() {
        ClassInfo classInfo = indexView.getClassByName(ownerClass);
        if (classInfo == null) {
            // This should never happen, unless the class somehow disappeared
            // from compile to runtime. If that happens, we have a bigger problem,
            // so just fail early.
            throw new MissingClassHubException(ownerClass);
        }

        if ((classInfo.flags() & Opcodes.ACC_INTERFACE) != 0) {
            throw new HubValidationException("SignalR hub classes cannot be interfaces! Offending class: " + ownerClass);
        }
    }

    /**
     * Ensures that the hub class is a leaf in the class hierarchy.
     */
    private void ensureHubIsLeaf() {
        ClassInfo classInfo = indexView.getClassByName(ownerClass);

        if (classInfo.annotation(DotName.createSimple(SignalRHub.class)) == null) {
            // This should never happen, unless the annotation disappeared from compile to
            // runtime. If that happens, make sure the class being passed in is the actual
            // class, not the class obtained from Arc.
            throw new HubValidationException("Class: " + ownerClass + " is not annotated with @SignalRHub!");
        }

        // Make sure the class isn't extended by anyone
        Collection<ClassInfo> knownSubclasses = indexView.getAllKnownSubclasses(ownerClass);
        if (knownSubclasses.isEmpty()) {
            return;
        }

        throw new HubValidationException("SignalR hub classes should not be used as parent classes! Offending class: "
                + ownerClass + " is extended by known subclasses: " + knownSubclasses);
    }

    /**
     * Ensures that all client methods are abstract.
     */
    private void ensureClientMethodsAreAbstract() {
        for (ClientMethodDefinition method : extractedMethods) {
            if ((method.getAccess() & Opcodes.ACC_ABSTRACT) == 0) {
                throw new HubValidationException("Client methods must be abstract! Offending method: " + method.getName()
                        + " inside of " + ownerClass);
            }
        }
    }

    /**
     * Ensures that no method has both {@link ServerMethod} and {@link ClientMethod}
     * annotations.
     */
    private void ensureNoMethodHasBothAnnotations() {
        DotName serverMethod = DotName.createSimple(ServerMethod.class);
        DotName clientMethod = DotName.createSimple(ClientMethod.class);

        for (DotName className : classTree) {
            ClassInfo classInfo = indexView.getClassByName(className.toString());
            if (classInfo == null) {
                throw new MissingClassHubException(className);
            }

            // Go through all the methods
            for (MethodInfo method : classInfo.methods()) {
                // Check if the method has both annotations
                if (method.annotation(serverMethod) != null && method.annotation(clientMethod) != null) {
                    throw new HubValidationException(
                            "Methods cannot have both @ServerMethod and @ClientMethod! Offending method: "
                                    + method.name() + " of: " + className);
                }
            }

        }
    }

    /**
     * Ensures that all abstract methods in the hub class are annotated with
     * {@link ClientMethod}. Unannotated abstract methods can still be placed in
     * classes from which the hub class inherits, but they must be implemented in
     * the hub class.
     * <p>
     * This is needed due to the fact that the hub class is abstract, and the
     * compiler will not complain if an inherited abstract method is not
     * implemented, and abstract methods in the hub class must be fully eliminated
     * before we can safely remove the abstract modifier on the class.
     */
    private void ensureAbstractMethodsInHubAreClientMethods() {
        ClassInfo classInfo = indexView.getClassByName(ownerClass);

        // Go through all the methods
        for (MethodInfo method : classInfo.methods()) {
            // Check if the method is abstract
            if ((method.flags() & Opcodes.ACC_ABSTRACT) != 0 && method.annotation(DotName.createSimple(ClientMethod.class)) == null) {
                throw new HubValidationException(
                        "Abstract methods in SignalR hub classes must be annotated with @ClientMethod! Offending method: "
                                + method.name() + " in: " + ownerClass);
            }
        }
    }

    /**
     * Ensures that the first argument of all methods annotated with
     * {@link ServerMethod} or {@link ClientMethod} is {@link SignalRSession}.
     */
    private void validateArgumentOfClientAndServerMethods() {
        DotName serverMethod = DotName.createSimple(ServerMethod.class);
        DotName clientMethod = DotName.createSimple(ClientMethod.class);
        DotName signalRSession = DotName.createSimple(SignalRSession.class);

        for (DotName className : classTree) {
            ClassInfo classInfo = indexView.getClassByName(className.toString());
            if (classInfo == null) {
                throw new MissingClassHubException(className);
            }

            // Go through all the methods
            for (MethodInfo method : classInfo.methods()) {
                // Check if the method has both annotations
                if (method.annotation(serverMethod) == null && method.annotation(clientMethod) == null) {
                    continue;
                }

                // Check if the first argument is SignalRSession
                List<MethodParameterInfo> parameters = method.parameters();
                if (!parameters.isEmpty() && parameters.getFirst().type().name().equals(signalRSession)) {
                    continue;
                }

                throw new HubValidationException("Methods annotated with @ServerMethod or @ClientMethod must " +
                        "have SignalRSession as their first argument! Offending method: "
                        + method.name() + " of class: " + className);
            }

        }
    }

    /**
     * Ensures that all server methods are public.
     */
    private void ensureServerMethodsArePublic() {
        DotName serverMethod = DotName.createSimple(ServerMethod.class);

        for (DotName className : classTree) {
            ClassInfo classInfo = indexView.getClassByName(className.toString());
            if (classInfo == null) {
                throw new MissingClassHubException(className);
            }

            // Go through all the methods
            for (MethodInfo method : classInfo.methods()) {
                // Check if the method has both annotations
                if (method.annotation(serverMethod) != null && (method.flags() & Opcodes.ACC_PUBLIC) == 0) {
                    throw new HubValidationException("Server methods must be public! Offending method: "
                            + method.name() + " in: " + className);
                }
            }

        }
    }

    /**
     * Ensures that all client methods are public.
     */
    private void ensureClientMethodsArePublic() {
        for (ClientMethodDefinition method : extractedMethods) {
            if ((method.getAccess() & Opcodes.ACC_PUBLIC) == 0) {
                throw new HubValidationException("Client methods must be public! Offending method: " + method.getName()
                        + " in class: " + ownerClass);
            }
        }
    }

    /**
     * This will ensure that all client methods and server methods are unique.
     */
    private void ensureBothClientAndServerMethodsAreNotOverloaded() {
        DotName serverMethod = DotName.createSimple(ServerMethod.class);
        DotName clientMethod = DotName.createSimple(ClientMethod.class);

        for (DotName className : classTree) {
            ClassInfo classInfo = indexView.getClassByName(className.toString());
            if (classInfo == null) {
                throw new MissingClassHubException(className);
            }

            Set<String> uniqueMethodNames = new HashSet<>();
            for (MethodInfo method : classInfo.methods()) {
                // Check if the method has any of the annotations
                if (method.annotation(serverMethod) != null || method.annotation(clientMethod) != null) {
                    String methodName = method.name();
                    if (uniqueMethodNames.contains(methodName)) {
                        throw new HubValidationException(
                                "Client methods and server methods cannot be overloaded! Offending method: "
                                        + methodName + " in class: " + className);
                    }

                    uniqueMethodNames.add(methodName);
                }
            }
        }
    }

}
