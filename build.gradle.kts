plugins {
    id("maven-publish")
    id("nl.rug.digitallab.gradle.plugin.java.project")
}

configure<PublishingExtension> {
    publications {
        create<MavenPublication>("signalr") {
            artifactId = "signalr-parent"
            groupId = project.property("projectGroupId") as String
        }
    }

    repositories {
        maven {
            url = project.uri(project.findProperty("mavenRepositoryUrl").toString())
            name = project.findProperty("mavenRepositoryName").toString()

            credentials(HttpHeaderCredentials::class.java) {
                name = project.findProperty("mavenRepositoryHeaderName").toString()
                value = project.findProperty("mavenRepositoryHeaderValue").toString()
            }

            authentication.apply {
                create("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
