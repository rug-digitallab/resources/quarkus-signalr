package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.completion;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRPacketListener;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation.PlainPendingInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.concurrent.CompletableFuture;

/**
 * This will handle all instances of the Completion packet.
 */
@JBossLog
@AllArgsConstructor
public class CompletionHandler implements SignalRPacketListener<Completion> {

    /**
     * The ObjectMapper to use for deserialization
     */
    private ObjectMapper objectMapper;

    /**
     * This will handle the completion of an invocation, marking the stream as
     * completed.
     *
     * @param session - The session that received the completion
     * @param packet  - The completion packet
     */
    @Override
    public void onPacketReceived(SignalRSession session, Completion packet) {
        // Check if this is a stream completion we're dealing with
        if (session.getStreamManager().hasIncomingStream(packet.getInvocationId())) {
            handleStreamCompletion(session, packet);
            return;
        }

        // Handle pending invocation
        if (session.getInvocationManager().hasOutgoingInvocation(packet.getInvocationId())) {
            handleInvocationCompletion(session, packet);
        }

        throw new IllegalStateException("No stream" +
                " found for completion with id " + packet.getInvocationId());
    }

    /**
     * Handles the completion of a stream.
     * <p>
     * This will remove the stream from the session's incoming streams and complete
     * the emitter.
     *
     * @param session - The session that received the completion
     * @param packet  - The completion packet
     */
    private void handleStreamCompletion(SignalRSession session, Completion packet) {
        session.getStreamManager().removeIncomingStream(packet.getInvocationId()).getEmitter().complete();
        log.debug("Stream completion received for id " + packet.getInvocationId());
    }

    /**
     * Handles the completion of an invocation.
     * This will complete the future associated with the invocation with the result
     * or error. If there is an error, it will complete the future exceptionally
     * with a RuntimeException containing the error.
     *
     * @param session - The session that received the completion
     * @param packet  - The completion packet
     */
    private void handleInvocationCompletion(SignalRSession session, Completion packet) {
        PlainPendingInvocation<?> invocation = session.getInvocationManager().removeOutgoingInvocation(packet.getInvocationId());

        @SuppressWarnings("unchecked")
        CompletableFuture<Object> future = (CompletableFuture<Object>) invocation.getFuture();
        if (packet.getError() != null && !packet.getError().isEmpty()) {
            future.completeExceptionally(new RuntimeException(packet.getError()));
            log.error("Error received for invocation with id " +
                    packet.getInvocationId() + ": " + packet.getError());

            return;
        }

        if (packet.getResult() != null) {
            try {
                Object result = objectMapper.convertValue(packet.getResult(), invocation.getType());
                log.debug("Result received for invocation with id " + packet.getInvocationId() + ": " + result);
                future.complete(result);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        } else {
            future.complete(null);
            log.debug("Null result received for invocation with id " + packet.getInvocationId());
        }
    }


}
