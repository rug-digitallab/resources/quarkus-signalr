package nl.rug.digitallab.quarkus.signalr.extension.runtime.transport;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.negotiation.SupportedTransport;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.negotiation.TransferFormat;

import java.util.List;

/**
 * Enum representing the different types of transports that can be used to
 * connect to the SignalR server.
 */
@Getter
@AllArgsConstructor
public enum TransportType {
    /**
     * The transport type for WebSockets.
     */
    WEB_SOCKETS("WebSockets", false, List.of(TransferFormat.TEXT)),

    /**
     * The transport type for Long Polling. This one requires a POST endpoint to be
     * available on the server.
     */
    LONG_POLLING("LongPolling", true, List.of(TransferFormat.TEXT)),

    /**
     * The transport type for SSE. This one requires a POST endpoint to be
     * available on the server.
     */
    SERVER_SENT_EVENTS("ServerSentEvents", true, List.of(TransferFormat.TEXT));

    @Getter(onMethod_ = @JsonValue)
    private final String name;

    /**
     * Whether the transport requires a POST endpoint to be available on the server.
     */
    private final boolean requiresPostEndpoint;

    /**
     * The transfer formats supported by the transport.
     */
    private final List<TransferFormat> transferFormats;

    /**
     * Get the supported transport for this transport type.
     * 
     * @return - The supported transport.
     */
    public SupportedTransport getSupportedTransport() {
        return new SupportedTransport(this, transferFormats);
    }
}
