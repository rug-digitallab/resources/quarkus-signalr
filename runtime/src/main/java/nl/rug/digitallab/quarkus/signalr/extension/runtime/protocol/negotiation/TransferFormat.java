package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.negotiation;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * This is the format in which formats can exist.
 * <p>
 * For more context see
 * <a href="https://learn.microsoft.com/en-us/azure/azure-signalr/signalr-concept-client-negotiation">the docs</a>
 */
@AllArgsConstructor
public enum TransferFormat {
    TEXT("Text"),
    BINARY("Binary");

    @Getter(onMethod_ = @JsonValue)
    private final String format;
}
