package nl.rug.digitallab.quarkus.signalr.extension.deployment.builditems;

import io.quarkus.builder.item.EmptyBuildItem;

/**
 * This build item is used to validate the SignalR hub classes.
 * <p>
 * It is an empty build item, so should be used as a marker.
 */
public class SignalRHubClassValidationBuildItem extends EmptyBuildItem {
}
