package nl.rug.digitallab.quarkus.signalr.extension.deployment;

import io.quarkus.deployment.IsDevelopment;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.devui.spi.page.CardPageBuildItem;
import io.quarkus.devui.spi.page.Page;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.builditems.SignalRHubBuildItem;

import java.util.List;

/**
 * This class is responsible for building the UI for the SignalR extension.
 */
public class SignalRExtensionUIProcessor {

    /**
     * This method builds the UI for the SignalR extension.
     * This build step will only be executed in development mode.
     * 
     * @param hubsBuildItems - The list of SignalR hubs.
     * @return - The card page build item.
     */
    @BuildStep(onlyIf = IsDevelopment.class) 
    public CardPageBuildItem buildSignalRExtension(List<SignalRHubBuildItem> hubsBuildItems) {
        CardPageBuildItem pageBuildItem = new CardPageBuildItem();

        pageBuildItem.addPage(
                Page.webComponentPageBuilder()
                        .icon("font-awesome-solid:cloud-arrow-up")
                        .componentLink("signalr-hubs-list.js")
                        .title("SignalR Hubs Registered")
                        .staticLabel(hubsBuildItems.size() + ""));

        pageBuildItem.addBuildTimeData("hubs", hubsBuildItems);
        return pageBuildItem;
    }
}
