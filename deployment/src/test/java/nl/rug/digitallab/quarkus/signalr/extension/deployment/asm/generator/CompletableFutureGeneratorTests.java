package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import nl.rug.digitallab.quarkus.signalr.extension.deployment.util.JavaTypeDataFaker.CompositeTestObject;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Type;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class CompletableFutureGeneratorTests extends GenericGeneratorTest<CompletableFutureMethodGenerator> {

    @Override
    public Type getHandledType() {
        return Type.getType(CompletableFuture.class);
    }

    @Override
    public CompletableFutureMethodGenerator createClientMethodGenerator() {
        return new CompletableFutureMethodGenerator();
    }

    @Override
    @SuppressWarnings("java:S1172") // Unused parameters for SonarLint
    public Object getMethodContainer() {
        return new Object() {
            // @formatter:off
            CompletableFuture<Integer> primitiveWrapperNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<int[]> primitiveArrayNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<String> stringNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<String[]> stringArrayNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<List<String>> listNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<Map<String, String>> setNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<Map<String, String>> mapNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<CompositeTestObject> compositeNoParamMethod(SignalRSession session) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayNoParamMethod(SignalRSession session) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<String> stringMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<List<String>> listMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<String> stringMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<List<String>> listMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<String> stringMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<List<String>> listMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<String> stringMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<List<String>> listMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<String> stringMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<List<String>> listMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<String> stringMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<List<String>> listMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<String> stringMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<List<String>> listMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<String> stringMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<List<String>> listMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<String> stringMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<List<String>> listMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<String> stringMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<List<String>> listMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }

            CompletableFuture<Integer> primitiveWrapperMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<int[]> primitiveArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<Integer[]> primitiveWrapperArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<String> stringMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<String[]> stringArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<List<String>> listMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<Map<String, String>> setMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<Map<String, String>> mapMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<CompositeTestObject> compositeMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompletableFuture<CompositeTestObject[]> compositeArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            // @formatter:on
        };
    }

    @Override
    public Object unwrapMethodInvocationResult(Object result) {
        CompletableFuture<?> future = (CompletableFuture<?>) result;
        return future.join();
    }

    @Override
    public boolean expectsStreamable() {
        return false;
    }
}
