// This will not conflict with Quarkus, as Quarkus only looks for the publication artifact
rootProject.name = "quarkus-signalr"


pluginManagement {
    plugins {
        val quarkusVersion: String by settings
        val digitalLabGradlePluginVersion: String by settings

        id("io.quarkus.extension") version quarkusVersion
        id("nl.rug.digitallab.gradle.plugin.java.project") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
        gradlePluginPortal()
        mavenCentral()
    }
}

include("runtime")
include("deployment")
