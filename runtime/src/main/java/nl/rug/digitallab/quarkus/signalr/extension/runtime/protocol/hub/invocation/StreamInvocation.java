package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation;

import lombok.Data;
import lombok.EqualsAndHashCode;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacketType;

import java.util.List;

/**
 * This is the StreamInvocation message, it is used to invoke a streaming method
 * on the server.
 * 
 * @see <a href=
 *      "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#streaminvocation-message-encoding">StreamInvocation
 *      message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StreamInvocation extends SignalRPacket {
    /** The invocation id */
    private String invocationId;

    /** The target method */
    private String target;

    /** The arguments */
    private List<Object> arguments;

    /** The stream ids */
    private List<String> streamIds;

    /**
     * Creates a new empty instance for the StreamInvocation message.
     */
    public StreamInvocation() {
        super(SignalRPacketType.STREAM_INVOCATION);
    }

    /**
     * Creates a new instance for the StreamInvocation message.
     * 
     * @param invocationId - The invocation id
     * @param target       - The target method
     * @param arguments    - The arguments
     * @param streamIds    - The stream ids
     */
    public StreamInvocation(String invocationId, String target, List<Object> arguments, List<String> streamIds) {
        super(SignalRPacketType.STREAM_INVOCATION);

        this.invocationId = invocationId;
        this.target = target;
        this.arguments = arguments;
        this.streamIds = streamIds;
    }

    /**
     * This will count the amount of stream ids present in the invocation.
     *
     * @return The amount of stream ids present in the invocation.
     */
    public int getStreamCount() {
        if (streamIds == null) {
            return 0;
        }
        return streamIds.size();
    }
}
