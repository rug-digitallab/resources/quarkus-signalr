package nl.rug.digitallab.quarkus.signalr.extension.deployment.processor;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.SignalRHub;

@SignalRHub("/chat")
public abstract class InvalidSignalRHub {
    @ClientMethod
    public void clientMethod() {
    }
}
