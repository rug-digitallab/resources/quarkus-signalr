package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm;

import io.quarkus.gizmo.Gizmo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.objectweb.asm.Type;
import org.objectweb.asm.signature.SignatureReader;
import org.objectweb.asm.signature.SignatureVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to extract the type reference from a method signature's
 * return type.
 * <p>
 * This is used to find the inner generic of the return type in a class.
 * <p>
 * As an example, if the signature of the following method:
 * 
 * <pre>
 * public CompletableFuture&lt;Map&lt;String, Integer&gt;&gt; method();
 * </pre>
 * 
 * Is passed to this class, it will return a list of ["CompletableFuture",
 * "Map", "String", "Integer"]
 * <p>
 * Due to the fact that the amount of arguments is known, the list can safely be
 * flattened, with the first element being the outermost type.
 * <p>
 * It should be noted that this class will not perform any type of boxing
 * operations, and will return the raw types of the arguments.
 * <p>
 * For example:
 * 
 * <pre>
 * public int method();
 * </pre>
 * 
 * Will return a list of ["int"]
 */
@AllArgsConstructor
public class SignatureGenericTypesExtractor {

    /** The signature of the method being analyzed */
    @NonNull
    private String methodSignature;

    /**
     * This will return the inner arguments of a type reference.
     * <p>
     * As an example, for Map<String, List<Integer>>, this will return a list of
     * ["Map", "String", "List", "Integer"].
     * 
     * @return - The inner arguments of the type reference
     */
    public List<String> getArguments() {
        ExtractorSignatureVisitor visitor = new ExtractorSignatureVisitor();
        new SignatureReader(methodSignature).accept(visitor);

        List<Type> typeArguments = visitor.getTypeArguments();
        List<String> arguments = new ArrayList<>();
        for (Type type : typeArguments) {
            arguments.add(type.getClassName());
        }

        return arguments;
    }

    /**
     * This will return the inner arguments of a type reference.
     * <p>
     * As an example, for Map<String, List<Integer>>, this will return a list of
     * ["String", "List", "Integer"].
     * 
     * 
     * @return - The inner arguments of the type reference
     * @see #getArguments()
     * @throws UnsupportedOperationException - If the type reference is not a
     *                                       generic type
     */
    public List<String> getInnerArguments() {
        List<String> arguments = getArguments();
        if (arguments.size() < 2) {
            throw new UnsupportedOperationException("The type reference is not a generic type!");
        }

        // In this case, getting the inner arguments simply means removing the outer
        // type
        return arguments.subList(1, arguments.size());
    }

    /**
     * This is the visitor that will be used to extract the type reference.
     */
    private static class ExtractorSignatureVisitor extends SignatureVisitor {
        public ExtractorSignatureVisitor() {
            super(Gizmo.ASM_API_VERSION);
        }

        /**
         * This will enable or disable type visit functions, to make sure we only
         * process the return type.
         */
        private boolean foundReturn = false;

        /**
         * This will store the type arguments of the type reference.
         */
        @Getter
        private List<Type> typeArguments = new ArrayList<>();

        /**
         * This counts in how many levels of arrays the given type is wrapped.
         */
        private int arrayCount = 0;

        /**
         * This method will get called once the return type starts being visited.
         */
        @Override
        public SignatureVisitor visitReturnType() {
            this.foundReturn = true;
            return super.visitReturnType();
        }

        /**
         * This will increment the array count once a new array type is found.
         * <p>
         * This works due to the fact that in the JVM, arrays are represented as
         * [Ltype; for reference types and [type for primitive types, and the array
         * count is the amount of '[' characters in the descriptor, which is called
         * before the actual type is visited.
         */
        @Override
        public SignatureVisitor visitArrayType() {
            if (!foundReturn) {
                return super.visitArrayType();
            }

            arrayCount++;
            return super.visitArrayType();
        }

        /**
         * This gets called whenever a primitive is found.
         * It will add the primitive type to the type arguments.
         */
        @Override
        public void visitBaseType(char descriptor) {
            super.visitBaseType(descriptor);
            if (!foundReturn) {
                return;
            }


            Type type = switch (descriptor) {
                case 'B' -> Type.BYTE_TYPE;
                case 'C' -> Type.CHAR_TYPE;
                case 'D' -> Type.DOUBLE_TYPE;
                case 'F' -> Type.FLOAT_TYPE;
                case 'I' -> Type.INT_TYPE;
                case 'J' -> Type.LONG_TYPE;
                case 'S' -> Type.SHORT_TYPE;
                case 'Z' -> Type.BOOLEAN_TYPE;

                // This case actually gets caught by ASM itself, but it's here for completeness
                default -> throw new IllegalArgumentException("Unknown base type: " + descriptor);
            };

            typeArguments.add(wrapInArrays(type));
        }

        /**
         * This will visit a class type, which is a reference type.
         */
        @Override
        public void visitClassType(String name) {
            super.visitClassType(name);

            if (!foundReturn) {
                return;
            }

            typeArguments.add(wrapInArrays(Type.getObjectType(name)));
        }

        /**
         * This will wrap the given type in the amount of arrays that have been found.
         * <p>
         * As an example, if the array count is 2 and the type is Integer, this will
         * return [[Ljava/lang/Integer; which would be the descriptor for Integer[][]
         * 
         * @param type - The type to wrap in arrays
         * @return - The type wrapped in the amount of arrays that have been found
         */
        private Type wrapInArrays(Type type) {
            if (arrayCount == 0) {
                return type;
            }

            Type result = Type.getType("[".repeat(arrayCount) + type.getDescriptor());
            arrayCount = 0;
            return result;
        }
    }
}
