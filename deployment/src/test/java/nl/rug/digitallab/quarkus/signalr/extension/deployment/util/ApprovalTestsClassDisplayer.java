package nl.rug.digitallab.quarkus.signalr.extension.deployment.util;

public class ApprovalTestsClassDisplayer {
    private ApprovalTestsClassDisplayer() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * This will convert the given byte array into its hexadecimal representation.
     * It will use columns of 16 bytes, with each byte being in the format 0xXX.
     * <p>
     * This is done so that a quick visual comparison can be made between two byte arrays,
     * as ApprovalTests has no built-in support for byte arrays.
     *
     * @param bytecode - The byte array to convert
     * @return - The hexadecimal representation of the byte array
     */
    public static String displayClass(byte[] bytecode) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < bytecode.length; i++) {
            result.append("0x").append(String.format("%02X", bytecode[i])).append(" ");
            if ((i + 1) % 16 == 0) {
                result.append("\n");
            }
        }

        return result.toString();
    }
}
