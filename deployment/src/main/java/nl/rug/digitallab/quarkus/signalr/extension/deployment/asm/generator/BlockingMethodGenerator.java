package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import com.fasterxml.jackson.databind.JavaType;
import io.quarkus.deployment.util.AsmUtil;
import lombok.NonNull;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.concurrent.CompletableFuture;

/**
 * This class is used to generate the necessary bytecode to invoke a client
 * method when the return type is a regular serializable type.
 */
public class BlockingMethodGenerator extends ClientMethodGenerator {
    /**
     * This will generate the required bytecode for a client method with a
     * general type. It will use the usual {@link CompletableFuture} and then await
     * it for an indefinite amount of time. The transformation will look like this:
     *
     * <pre>
     * &#64;ClientMethod
     * public abstract Map&lt;String, Integer&gt; method(SignalRSession session, String param);
     * </pre>
     * <p>
     * Will be transformed into this
     *
     * <pre>
     * public Map&lt;String, Integer&gt; method(SignalRSession session, String param) {
     *     return session.invoke("method", NestedTypeReferenceBuilder.buildFromNames(
     *             "java.util.Map", "java.lang.String", "java.lang.Integer"), param).join();
     * }
     * </pre>
     * <p>
     * For more information check out
     * {@link SignalRSession#invoke(String, JavaType, Object...)}
     *
     * @param method    - The definition of the method
     * @param mv        - The method visitor to use
     * @param arguments - The arguments of the method (without the session)
     */
    public void generateClientMethodBytecode(ClientMethodDefinition method, MethodVisitor mv, Type[] arguments) {
        // This will call and place the result of the SignalRSession.invoke method on
        // the stack.
        visitSessionInvokeMethodInstruction(method, mv, arguments, false);
        // After this the stack looks like this: [CompletableFuture]

        // Call the join method on the CompletableFuture
        visitMethodInvocation(mv, CompletableFuture.class, "join", false);

        // Trigger a CheckCast, since the return type is known and from a Generic type.
        Type methodReturnType = Type.getReturnType(method.getDescriptor());

        if (methodReturnType.getSort() == Type.OBJECT || methodReturnType.getSort() == Type.ARRAY) {
            mv.visitTypeInsn(Opcodes.CHECKCAST, methodReturnType.getInternalName());
        } else {
            // If the return type is a primitive, we need to unbox it.
            // Do a checkcast to the boxed type
            // Unbox the value
            AsmUtil.unboxIfRequired(mv, methodReturnType);
        }

        mv.visitLabel(new Label());

        // Search for the return type of the method and return it.
        mv.visitInsn(methodReturnType.getOpcode(Opcodes.IRETURN));

        /*
         * This produces the equivalent of:
         * return session.invoke("method",
         * NestedTypeReferenceBuilder.buildFromNames("java.util.Map",
         * "java.lang.String", "java.lang.Integer"), param).join();
         */

        // Since COMPUTE_MAXS is enabled, 0,0 is fine
        mv.visitMaxs(0, 0);
        mv.visitEnd();
    }

    /**
     * Since this is the default generator, it can handle any type, except for void.
     */
    @Override
    public boolean canHandle(@NonNull Type type) {
        return type.getSort() != Type.VOID;
    }

}
