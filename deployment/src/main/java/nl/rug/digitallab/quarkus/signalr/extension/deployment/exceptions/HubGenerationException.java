package nl.rug.digitallab.quarkus.signalr.extension.deployment.exceptions;

/**
 * This exception is thrown when a generation error occurs during the
 * generation process of a hub.
 */
public class HubGenerationException extends RuntimeException {
    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message - the detail message.
     */
    public HubGenerationException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message - the detail message.
     * @param cause - the cause of the exception.
     */
    public HubGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}
