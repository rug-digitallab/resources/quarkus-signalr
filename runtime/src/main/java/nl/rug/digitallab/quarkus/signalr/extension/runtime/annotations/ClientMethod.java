package nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations;

import java.lang.annotation.*;

/**
 * Annotation to mark a method as a client method.
 * <p>
 * Any method which is annotated with this <b>must be abstract</b> and will be
 * invoked by the server.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ClientMethod {
    
}
