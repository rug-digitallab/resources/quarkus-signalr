package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.negotiation;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;

import java.util.List;

/**
 * Represents a type of transport that is supported by the SignalR server.
 */
@Data
@AllArgsConstructor
public class SupportedTransport {
    /** The transport name */
    private final TransportType transport;

    /** The transfer formats supported by the transport */
    private final List<TransferFormat> transferFormats;
}