package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.senders;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * An adapter for {@link StreamInvocation} that uses {@link Object} as the
 * result type.
 * <p>
 * It will send the result as a {@link StreamItem} and a {@link Completion}
 * packet.
 */
public class DefaultStreamResultSender extends StreamInvocationResultSender<Object> {

    @Override
    public void handleResult(SignalRSession session, StreamInvocation packet, Object methodResult) {
        this.sendItemAndCompletion(session, packet, methodResult);
    }
}
