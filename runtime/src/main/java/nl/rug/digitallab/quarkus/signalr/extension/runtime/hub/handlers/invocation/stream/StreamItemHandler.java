package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream;


import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRPacketListener;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.stream.SignalRIncomingStream;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * This will handle all instances of StreamItem packets that are received from
 * the SignalR client and send them to the correct open stream.
 *
 * @see StreamItem
 */
@AllArgsConstructor
public class StreamItemHandler implements SignalRPacketListener<StreamItem> {
    /**
     * The object mapper which will be used for conversion
     */
    private ObjectMapper objectMapper;

    /**
     * This will handle all instances of StreamItem packets that are received from
     * the SignalR client and send them to the correct open stream.
     */
    @Override
    public void onPacketReceived(SignalRSession session, StreamItem packet) {
        // Figure out the stream to send the item to
        String streamId = packet.getInvocationId();
        @SuppressWarnings("unchecked")
        SignalRIncomingStream<Object> stream = (SignalRIncomingStream<Object>)
                session.getStreamManager().getIncomingStream(streamId);
        if (stream == null) {
            throw new IllegalStateException("Stream with id " + streamId + " not found.");
        }

        // Convert the item to the correct type and emit it
        JavaType itemType = stream.getItemType();

        try {
            Object item = objectMapper.convertValue(packet.getItem(), itemType);

            // Emit the item
            stream.getEmitter().emit(item);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
