package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.container;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;

import java.util.*;

/**
 * This class is responsible for keeping track of registered result senders, as
 * well as handling lookup for the correct result sender for a given invocation.
 * 
 * @param <P> - The type of the packet that initiated the invocation.
 * @param <S> - The type of the result sender.
 */
public abstract class BaseResultSenderContainer<P extends SignalRPacket, S extends BaseInvocationResultSender<P, ?>> {
    private Map<Class<?>, S> resultSenders = new HashMap<>();

    /**
     * This method will look up the result sender for the given object type.
     * <p> <p>
     * It will check iterate over all supertypes and interfaces implemented by the
     * result type to find the correct result sender.
     * <p> <p>
     * Should the result be null, the result sender for Object will be returned.
     * <p> <p>
     * Ideally, a result sender should be registered for each type of result, as well
     * as a default result sender for Object.
     * 
     * @param resultClass - The result to look up the result sender for.
     * @return - The result sender for the result.
     */
    protected S lookupResultSender(Class<?> resultClass) {
        // This is a simple breadth-first search across the class hierarchy.
        Queue<Class<?>> classes = new LinkedList<>();
        classes.add(resultClass);

        while (!classes.isEmpty()) {
            Class<?> clazz = classes.poll();
            S resultSender = this.resultSenders.get(clazz);
            if (resultSender != null) {
                return resultSender;
            }

            Class<?> superclass = clazz.getSuperclass();
            if (superclass != null && !superclass.equals(Object.class)) {
                classes.add(superclass);
            }

            Collections.addAll(classes, clazz.getInterfaces());
        }

        return this.resultSenders.get(Object.class);
    }

    /**
     * This method will look up the correct result sender for the given result.
     * <p> <p>
     * It will call the lookupResultSender method with the result's class.
     * 
     * @param result - The result to look up the result sender for.
     * @return - The result sender for the result.
     * @see #lookupResultSender(Class)
     */
    protected S lookupResultSender(Object result) {
        if (result == null) {
            return this.resultSenders.get(Object.class);
        }

        return this.lookupResultSender(result.getClass());
    }

    /**
     * This method will register a new result sender for the given result type.
     * 
     * @param resultType - The type of the result to register the result sender for.
     * @param resultSender - The result sender to register.
     */
    public void registerResultSender(Class<?> resultType, S resultSender) {
        this.resultSenders.put(resultType, resultSender);
    }

}