package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.generic;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is just a simple error message that can be sent or received from the
 * SignalR server.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignalRErrorMessage {
    /**
     * The error message.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String error;
}
