package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * This is the Close message, it is used to close the connection.
 * 
 * @see <a href=
 *      "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#close-message-encoding">
 *      Close message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CloseMessage extends SignalRPacket {
    /** The error */
    private String error;

    /** Whether a reconnect is allowed. */
    @Getter(AccessLevel.NONE)
    private Boolean allowReconnect;

    /**
     * Instantiates a new close packet.
     */
    public CloseMessage() {
        super(SignalRPacketType.CLOSE);
    }

    /**
     * Instantiates a new close packet.
     * 
     * @param error          - The error
     * @param allowReconnect - Whether a reconnect is allowed. (null to omit the
     *                       field)
     */
    public CloseMessage(String error, Boolean allowReconnect) {
        super(SignalRPacketType.CLOSE);
        this.error = error;
        this.allowReconnect = allowReconnect;
    }

    /**
     * Checks if reconnection is allowed.
     *
     * @return true, if reconnection is allowed
     */
    public boolean isAllowReconnect() {
        return allowReconnect != null && allowReconnect;
    }
}
