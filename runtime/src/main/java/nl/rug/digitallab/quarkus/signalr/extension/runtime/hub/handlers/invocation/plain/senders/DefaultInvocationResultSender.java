package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain.senders;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * This class is an invocation adapter that will be used when no other adapter
 * is found for the invocation. It simply serializes the result and sends it
 * back to the client.
 */
public class DefaultInvocationResultSender extends PlainInvocationResultSender<Object> {

    @Override
    public void handleResult(SignalRSession session, Invocation packet, Object methodResult) {
        // In this case, we don't actually need to handle errors, since if an exception is
        // in fact thrown by the method, it will be caught long before it reaches here.
        // Error handling by the ResultSender is only used when errors are passed
        // through some other means (like in CompletableFutures)
        sendCompletion(session, packet, methodResult);
    }

}
