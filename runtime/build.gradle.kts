plugins {
    id("io.quarkus.extension")

    id("nl.rug.digitallab.gradle.plugin.java.project")
    id("maven-publish")
}

dependencies {
    val quarkusVersion: String by project
    val lombokVersion: String by project

    // Lombok
    compileOnly("org.projectlombok:lombok:${lombokVersion}")
    annotationProcessor("org.projectlombok:lombok:${lombokVersion}")

    testCompileOnly("org.projectlombok:lombok:${lombokVersion}")
    testAnnotationProcessor("org.projectlombok:lombok:${lombokVersion}")

    // Everything related to quarkus
    implementation(platform("io.quarkus:quarkus-bom:${quarkusVersion}"))
    implementation("io.quarkus:quarkus-arc")
    implementation("io.quarkus:quarkus-vertx-http")
    implementation("io.quarkus:quarkus-jackson")
    implementation("io.quarkus:quarkus-quartz")
}

configure<PublishingExtension> {
    publications {
        create<MavenPublication>("signalrRuntime") {
            from(components["java"])
            artifactId = "signalr"
            groupId = project.property("projectGroupId") as String
        }
    }

    repositories {
        maven {
            url = project.uri(project.findProperty("mavenRepositoryUrl").toString())
            name = project.findProperty("mavenRepositoryName").toString()

            credentials(HttpHeaderCredentials::class.java) {
                name = project.findProperty("mavenRepositoryHeaderName").toString()
                value = project.findProperty("mavenRepositoryHeaderValue").toString()
            }

            authentication.apply {
                create("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
