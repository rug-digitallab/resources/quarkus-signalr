package nl.rug.digitallab.quarkus.signalr.extension.runtime.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.arc.Arc;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnectorManager;

/**
 * This class represents a handler for a SignalR endpoint.
 */
@RequiredArgsConstructor
public class SignalRHandler {
    @Getter
    @NonNull
    private String path;

    private SignalRConnector connector;

    private ObjectMapper objectMapper;

    /**
     * Get the connector for this handler.
     *
     * @return - The connector for this handler.
     */
    public SignalRConnector getConnector() {
        if (connector == null) {
            SignalRConnectorManager connectorManager = Arc.container().instance(SignalRConnectorManager.class).get();
            this.connector = connectorManager.getConnector(path);
        }
        return connector;
    }

    /**
     * Get the object mapper for this handler.
     *
     * @return - The object mapper for this handler.
     */
    public ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            SignalRConnectorManager connectorManager = Arc.container().instance(SignalRConnectorManager.class).get();
            this.objectMapper = connectorManager.getObjectMapper();
        }
        return objectMapper;
    }
}
