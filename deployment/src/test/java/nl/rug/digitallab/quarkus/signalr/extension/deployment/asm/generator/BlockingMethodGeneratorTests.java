package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import nl.rug.digitallab.quarkus.signalr.extension.deployment.util.JavaTypeDataFaker.CompositeTestObject;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Type;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class BlockingMethodGeneratorTests extends GenericGeneratorTest<BlockingMethodGenerator> {
    @Override
    public Type getHandledType() {
        return Type.getType(Object.class);
    }

    @Override
    public BlockingMethodGenerator createClientMethodGenerator() {
        return new BlockingMethodGenerator();
    }

    @Override
    @SuppressWarnings("java:S1172") // Unused parameters for SonarLint
    public Object getMethodContainer() {
        return new Object() {
            // @formatter:off
            int primitiveNoParamMethod(SignalRSession session) { return 0; }
            Integer primitiveWrapperNoParamMethod(SignalRSession session) { return null; }
            int[] primitiveArrayNoParamMethod(SignalRSession session) { return null; }
            Integer[] primitiveWrapperArrayNoParamMethod(SignalRSession session) { return null; }
            String stringNoParamMethod(SignalRSession session) { return null; }
            String[] stringArrayNoParamMethod(SignalRSession session) { return null; }
            List<String> listNoParamMethod(SignalRSession session) { return null; }
            Set<String> setNoParamMethod(SignalRSession session) { return null; }
            Map<String, String> mapNoParamMethod(SignalRSession session) { return null; }
            CompositeTestObject compositeNoParamMethod(SignalRSession session) { return null; }
            CompositeTestObject[] compositeArrayNoParamMethod(SignalRSession session) { return null; }

            int primitiveMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return 0; }
            Integer primitiveWrapperMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            int[] primitiveArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Integer[] primitiveWrapperArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            String stringMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            String[] stringArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            List<String> listMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Set<String> setMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Map<String, String> mapMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompositeTestObject compositeMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            CompositeTestObject[] compositeArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }

            int primitiveMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return 0; }
            Integer primitiveWrapperMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            int[] primitiveArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Integer[] primitiveWrapperArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            String stringMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            String[] stringArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            List<String> listMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Set<String> setMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Map<String, String> mapMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompositeTestObject compositeMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            CompositeTestObject[] compositeArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }

            int primitiveMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return 0; }
            Integer primitiveWrapperMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            int[] primitiveArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Integer[] primitiveWrapperArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            String stringMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            String[] stringArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            List<String> listMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Set<String> setMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Map<String, String> mapMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompositeTestObject compositeMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            CompositeTestObject[] compositeArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }

            int primitiveMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return 0; }
            Integer primitiveWrapperMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            int[] primitiveArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Integer[] primitiveWrapperArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            String stringMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            String[] stringArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            List<String> listMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Set<String> setMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Map<String, String> mapMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompositeTestObject compositeMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            CompositeTestObject[] compositeArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }

            int primitiveMethodStringParam(SignalRSession session, String stringParam) { return 0; }
            Integer primitiveWrapperMethodStringParam(SignalRSession session, String stringParam) { return null; }
            int[] primitiveArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Integer[] primitiveWrapperArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            String stringMethodStringParam(SignalRSession session, String stringParam) { return null; }
            String[] stringArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            List<String> listMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Set<String> setMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Map<String, String> mapMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompositeTestObject compositeMethodStringParam(SignalRSession session, String stringParam) { return null; }
            CompositeTestObject[] compositeArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }

            int primitiveMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return 0; }
            Integer primitiveWrapperMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            int[] primitiveArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Integer[] primitiveWrapperArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            String stringMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            String[] stringArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            List<String> listMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Set<String> setMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Map<String, String> mapMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompositeTestObject compositeMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            CompositeTestObject[] compositeArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }

            int primitiveMethodListParam(SignalRSession session, List<String> listParam) { return 0; }
            Integer primitiveWrapperMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            int[] primitiveArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Integer[] primitiveWrapperArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            String stringMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            String[] stringArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            List<String> listMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Set<String> setMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Map<String, String> mapMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompositeTestObject compositeMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            CompositeTestObject[] compositeArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }

            int primitiveMethodSetParam(SignalRSession session, Set<String> setParam) { return 0; }
            Integer primitiveWrapperMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            int[] primitiveArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Integer[] primitiveWrapperArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            String stringMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            String[] stringArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            List<String> listMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Set<String> setMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Map<String, String> mapMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompositeTestObject compositeMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            CompositeTestObject[] compositeArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }

            int primitiveMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return 0; }
            Integer primitiveWrapperMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            int[] primitiveArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Integer[] primitiveWrapperArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            String stringMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            String[] stringArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            List<String> listMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Set<String> setMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Map<String, String> mapMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompositeTestObject compositeMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            CompositeTestObject[] compositeArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }

            int primitiveMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return 0; }
            Integer primitiveWrapperMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            int[] primitiveArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Integer[] primitiveWrapperArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            String stringMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            String[] stringArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            List<String> listMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Set<String> setMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Map<String, String> mapMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompositeTestObject compositeMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            CompositeTestObject[] compositeArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }

            int primitiveMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return 0; }
            Integer primitiveWrapperMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            int[] primitiveArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Integer[] primitiveWrapperArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            String stringMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            String[] stringArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            List<String> listMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Set<String> setMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Map<String, String> mapMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompositeTestObject compositeMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            CompositeTestObject[] compositeArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            // @formatter:on
        };
    }

    @Override
    public Object unwrapMethodInvocationResult(Object result) {
        return result;
    }

    @Override
    public boolean expectsStreamable() {
        return false;
    }
}
