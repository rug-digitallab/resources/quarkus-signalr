package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.negotiation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.List;
import java.util.UUID;

/**
 * This is the negotiation response that is sent to the SignalR client.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NegotiationResponse {
    /** The connection token */
    private UUID connectionToken;

    /** The connection id */
    private UUID connectionId;

    /** The negotiated version */
    private int negotiateVersion;

    /** The available transports */
    private List<SupportedTransport> availableTransports;

    /**
     * Constructor for the NegotiationResponse, it takes a session and a list of available transports.
     * 
     * @param session - The session
     * @param availableTransports - The available transports
     */
    public NegotiationResponse(SignalRSession session, List<SupportedTransport> availableTransports) {
        this.connectionToken = session.getConnectionToken();
        this.connectionId = session.getConnectionId();
        this.negotiateVersion = session.getNegotiateVersion();
        this.availableTransports = availableTransports;
    }
}
