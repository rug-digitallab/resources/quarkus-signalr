package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import io.quarkus.arc.ClientProxy;
import lombok.*;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ServerMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

/**
 * This represents a SignalR hub definition.
 * <p>
 * These objects are generated during the deployment of the SignalR extension,
 * with some information about the hub, such as the path, the class name and the
 * transports it supports.
 * <p>
 * Further information is calculated during runtime, such as the callable
 * methods of the hub.
 */
@JBossLog
@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class SignalRHubDefinition {
    @NonNull
    private String className;
    @NonNull
    private String path;

    @NonNull
    private Set<TransportType> transports;

    @Setter(AccessLevel.NONE)
    private SignalRConnector connector;

    private Map<String, Map<Integer, SignalRHubMethod>> callableMethods = new HashMap<>();

    /**
     * This represents a method on a SignalR hub.
     * <p>
     * This contains information about the parameters (retaining generic information)
     * and a MethodHandle to the method, which can be used to invoke it.
     */
    @Getter
    @AllArgsConstructor
    public static class SignalRHubMethod {
        private final String methodName;

        private final JavaType returnType;

        private final List<JavaType> parameterTypes;
        private final List<String> parameterNames;

        private final MethodHandle methodHandle;

        public SignalRHubMethod(Method method) throws IllegalAccessException {
            TypeFactory typeFactory = TypeFactory.defaultInstance();

            this.methodName = method.getName();
            this.methodHandle = MethodHandles.publicLookup().unreflect(method);

            if (void.class.equals(method.getReturnType()) || Void.class.equals(method.getReturnType()))
                this.returnType = null;
            else
                this.returnType = typeFactory.constructType(method.getGenericReturnType());

            List<JavaType> paramTypes = new ArrayList<>();
            List<String> paramNames = new ArrayList<>();
            for (Parameter parameter : method.getParameters()) {
                JavaType javaType = typeFactory.constructType(parameter.getParameterizedType());
                paramTypes.add(javaType);
                paramNames.add(parameter.getName());
            }

            // Create an immutable list
            this.parameterTypes = List.copyOf(paramTypes);
            this.parameterNames = List.copyOf(paramNames);
        }

        public int getParameterCount() {
            return parameterTypes.size();
        }
    }

    /**
     * This will set the connector for the hub.
     * <p>
     * It should be noted, that this will also bake
     * the callable methods for the hub.
     *
     * @param connector - The connector
     */
    public void setConnector(SignalRConnector connector) {
        this.connector = connector;

        try {
            bakeCallableMethods();
        } catch (IllegalAccessException e) {
            // This should never happen, as the bytecode validation step ensures
            // that the methods are accessible
            log.error("Could not bake callable methods", e);
        }
    }

    /**
     * This will bake the callable methods for the hub.
     * <p>
     * In this context, "baking" refers to calculating all the methods which
     * can be called on the hub, grabbing Method objects for them and keeping
     * track of what can be called based on argument count.
     * <p>
     * This is done for performance reasons, as we do not want to look up
     * methods every time a message is received.
     * <p>
     * This will look for all methods annotated with {@link ServerMethod} and store
     * them in a map, based on their name and parameter count. This means that the
     * only way to differentiate between methods is by their name and parameter
     * count, so overloading can be done by changing the parameter count.
     *
     * @throws IllegalAccessException - If the method is not accessible
     * @throws IllegalStateException  - If the connector is not set by the time baking happens
     */
    private void bakeCallableMethods() throws IllegalAccessException {
        if (connector == null) {
            throw new IllegalStateException("Connector is not set");
        }
        this.callableMethods.clear();

        Object signalRHubInstance = connector.getSignalRHubInstance();
        Class<?> hubClass = signalRHubInstance.getClass();
        if (signalRHubInstance instanceof ClientProxy) {
            hubClass = signalRHubInstance.getClass().getSuperclass();
        }

        for (Method method : hubClass.getMethods()) {
            if (!method.isAnnotationPresent(ServerMethod.class)) {
                continue;
            }

            // We do not count the first parameter, which is the SignalRSession
            int parameterCount = method.getParameterCount() - 1;
            String methodName = method.getName();

            callableMethods.computeIfAbsent(methodName, key -> new HashMap<>());

            if (callableMethods.get(methodName).containsKey(parameterCount)) {
                throw new IllegalStateException("Method %s with %d parameters already exists"
                        .formatted(methodName, parameterCount));
            }

            callableMethods.get(methodName).put(parameterCount, new SignalRHubDefinition.SignalRHubMethod(method));
        }
    }

    /**
     * This will look up a MethodHandle by name and parameter count.
     *
     * @param methodName     - The name of the method
     * @param parameterCount - The number of parameters
     * @return - The MethodHandle for the method, or null if it does not exist
     */
    public SignalRHubMethod lookupMethod(String methodName, int parameterCount) {
        if (!callableMethods.containsKey(methodName)) {
            return null;
        }

        return callableMethods.get(methodName).get(parameterCount);
    }
}
