package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.senders;

import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.container.BaseInvocationResultSender;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * Classes implementing this interface are responsible for serving the response
 * to the client once the
 * stream invocation has happened on the server.
 * <p>
 * Ideally, a method which is called as a stream invocation should return a
 * Multi, as that is the ideal type for streaming data.
 * <p>
 * That being said, should the method return something which is not necessarily
 * a Multi or a streamable type, the implementation of this interface should do
 * its best to still adapt this into a stream. As an example, take a concrete
 * implementation of this {@link StreamCompletableFutureResultSender}, which
 * takes the completable future, takes its result, and sends it as an item
 * followed, by a completion, hence adapting the completable future into a
 * stream.
 *
 * @param <T> - The type of the result of the invocation.
 */
@JBossLog
public abstract class StreamInvocationResultSender<T> implements BaseInvocationResultSender<StreamInvocation, T> {

    /**
     * This is a convenience method that will send an item to the client.
     * It will wrap the given object in a {@link StreamItem} and send it to the
     * client.
     *
     * @param session - The session that originated the invocation.
     * @param packet  - The invocation packet.
     * @param item    - The item to send to the client.
     */
    protected void sendItem(SignalRSession session, StreamInvocation packet, Object item) {
        // If it had been previously cancelled, we don't want to send a completion.
        if (!session.getInvocationManager().hasIncomingStreamInvocation(packet.getInvocationId()))

            return;

        session.sendMessage(new StreamItem(packet.getInvocationId(), item));
    }

    /**
     * This is a convenience method that will send a completion to the client.
     * <p>
     * Since this is for a streamable invocation, there is no result expected in the
     * completion.
     *
     * @param session - The session that originated the invocation.
     * @param packet  - The stream invocation packet.
     * @param error   - The error message, if any.
     */
    protected void sendCompletion(SignalRSession session, StreamInvocation packet, String error) {
        // If it had been previously cancelled, we don't want to send a completion.
        if (!session.getInvocationManager().hasIncomingStreamInvocation(packet.getInvocationId()))
            return;

        // Since the invocation is now complete, we can remove it from the ongoing
        // streamable invocations.
        session.getInvocationManager().removeIncomingStreamInvocation(packet.getInvocationId());

        // Send the completion message
        session.sendMessage(new Completion(packet.getInvocationId(), null, error));
    }

    /**
     * This is a convenience method that will send the item and the completion. This
     * is useful when adapting things like a Uni or a CompletableFuture to a stream
     * invocation.
     */
    protected void sendItemAndCompletion(SignalRSession session, StreamInvocation packet, Object item) {
        // If it had been previously cancelled, we don't want to send a completion.
        if (!session.getInvocationManager().hasIncomingStreamInvocation(packet.getInvocationId()))
            return;

        try {
            sendItem(session, packet, item);
        } catch (RuntimeException e) {
            log.error("Error sending item", e);
        } finally {
            sendCompletion(session, packet, null);
        }
    }
}
