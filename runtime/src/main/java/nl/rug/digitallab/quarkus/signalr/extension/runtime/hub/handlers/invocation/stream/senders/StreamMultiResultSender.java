package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.senders;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.subscription.Cancellable;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * An adapter for {@link StreamInvocation} that uses {@link Multi} as the result type.
 */
public class StreamMultiResultSender extends StreamInvocationResultSender<Multi<?>> {

    @Override
    public void handleResult(SignalRSession session, StreamInvocation packet, Multi<?> methodResult) {
        String invocationId = packet.getInvocationId();

        Cancellable result = methodResult.subscribe().with(
                item -> this.sendItem(session, packet, item),
                e -> this.sendCompletion(session, packet, e.getMessage()),
                () -> this.sendCompletion(session, packet, null)
        );

        session.getInvocationManager().addIncomingStreamInvocation(invocationId, result);
    }
}
