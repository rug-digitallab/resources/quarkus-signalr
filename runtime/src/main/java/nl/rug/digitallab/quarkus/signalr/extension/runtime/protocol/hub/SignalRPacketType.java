package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake.Sequence;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.CancelInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;

/**
 * This is the type of packet that a SignalR packet can be.
 * <p>
 * For information where the numbers come from, see the SignalR protocol spec
 * <a href=
 * "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md">here</a>.
 */
@AllArgsConstructor
public enum SignalRPacketType {
    INVOCATION(1, Invocation.class),
    STREAM_ITEM(2, StreamItem.class),
    COMPLETION(3, Completion.class),
    STREAM_INVOCATION(4, StreamInvocation.class),
    CANCEL_INVOCATION(5, CancelInvocation.class),
    PING(6, Ping.class),
    CLOSE(7, CloseMessage.class),
    ACK(8, Ack.class),
    SEQUENCE(9, Sequence.class);

    @Getter(onMethod_ = @JsonValue)
    private final int type;

    @Getter
    private final Class<? extends SignalRPacket> packetClass;

    /**
     * Gets the SignalRPacketType from the integer type.
     * 
     * @param type - The type of the packet
     * @return - The SignalRPacketType
     */
    public static SignalRPacketType fromTypeInt(int type) {
        for (SignalRPacketType packetType : SignalRPacketType.values()) {
            if (packetType.type == type) {
                return packetType;
            }
        }
        return null;
    }
}
