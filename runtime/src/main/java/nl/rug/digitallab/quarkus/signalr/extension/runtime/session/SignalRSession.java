package nl.rug.digitallab.quarkus.signalr.extension.runtime.session;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smallrye.mutiny.Multi;
import lombok.*;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

/**
 * This represents a SignalR session.
 */
@Data
@JBossLog
@RequiredArgsConstructor
@AllArgsConstructor
public class SignalRSession {
    /**
     * This is the connection ID for the SignalR session.
     *
     * @see <a href=
     * "https://learn.microsoft.com/en-us/azure/azure-signalr/signalr-concept-client-negotiation">SignalR
     * Negociation</a>
     */
    @NonNull
    private UUID connectionId;

    /**
     * This is the connection token for the SignalR session.
     *
     * @see <a href=
     * "https://learn.microsoft.com/en-us/azure/azure-signalr/signalr-concept-client-negotiation">SignalR
     * Negociation</a>
     */
    @NonNull
    private UUID connectionToken;

    /**
     * The SignalR connector backing this session.
     */
    @NonNull
    private SignalRConnector connector;

    /**
     * The object mapper for this session.
     */
    @NonNull
    private ObjectMapper objectMapper;

    /**
     * This is used for invocations and stream ids. It is incremented for each
     * invocation.
     * <p>
     * According to the spec, the ID is a string, however all available implementations
     * simply use an incrementing number which is what we will do as well.
     */
    private AtomicLong invocationId = new AtomicLong(0);

    /**
     * The invocation manager for this session
     */
    @Setter(AccessLevel.NONE)
    private SessionInvocationManager invocationManager = new SessionInvocationManager(this);

    /**
     * The stream manager for this session
     */
    @Setter(AccessLevel.NONE)
    private SessionStreamManager streamManager = new SessionStreamManager(this);

    /**
     * This is the version of the protocol which was agreed upon by the server
     * and the client.
     */
    private final int negotiateVersion;

    /**
     * This keeps track of whether the handshake has been completed or not. This is
     * needed, because
     * the first message sent by the client is a handshake message, and the server
     * needs to know
     * to handle the handshake and not forward it to the hub.
     */
    private boolean handshakeComplete = false;

    /**
     * This allows the SignalR session to have messages sent back down. This is
     * lazily initialized based on what the client opens. For example, if the
     * client opens a WebSocket connection, this will send messages over the WebSocket.
     */
    @Setter
    private Consumer<String> messageSender;

    /**
     * This sends a message to the client, over whichever transport is open.
     *
     * @param message - The message to send
     */
    public void sendRawMessage(String message) {
        if (messageSender != null) {
            messageSender.accept(message);
        } else {
            throw new IllegalStateException("Message sender not set");
        }
    }

    public void sendMessage(Object object) {
        try {
            String message = objectMapper.writeValueAsString(object);
            log.debug("S->C:" + object + " mapped as " + message);
            sendRawMessage(message);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to serialize object", e);
        }
    }

    /**
     * This will get the next invocation ID for this session.
     * It can be used for either a stream id or an invocation id.
     * <p>
     * This function guarantees that the invocation ID will be unique for this session. (as long as it is not overflowed)
     *
     * @return - The next available invocation ID
     */
    protected long nextInvocationId() {
        return invocationId.getAndIncrement();
    }

    /**
     * This will invoke a method on the SignalR hub.
     *
     * @param methodName - The name of the method to invoke
     * @param returnType - The return type of the method (used for deserialization)
     * @param params     - The parameters to pass to the method
     * @param <T>        - The type of the return value
     * @return - A future which will be completed when the invocation is completed
     */
    public <T> CompletableFuture<T> invoke(String methodName, JavaType returnType, Object... params) {
        return invocationManager.invoke(methodName, returnType, params);
    }

    /**
     * This will invoke a streaming method on the SignalR hub.
     *
     * @param methodName - The name of the method to invoke
     * @param itemType   - The type of the items in the stream
     * @param params     - The parameters to pass to the method
     * @param <T>        - The type of the items in the stream
     * @return - A Multi which will emit the items in the stream
     */
    public <T> Multi<T> invokeStreamable(String methodName, JavaType itemType, Object... params) {
        return invocationManager.invokeStreamable(methodName, itemType, params);
    }
}
