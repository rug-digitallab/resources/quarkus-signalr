package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import com.fasterxml.jackson.databind.JavaType;
import lombok.NonNull;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.concurrent.CompletableFuture;

/**
 * This class is used to generate the necessary bytecode to invoke a client
 * method when the return type is a {@link CompletableFuture}.
 */
public class CompletableFutureMethodGenerator extends ClientMethodGenerator {
    /**
     * This will generate the required bytecode for a client method with a
     * {@link CompletableFuture} return type. This is about the simplest
     * transformation possible, as it just returns the usual result of the invoke
     * method. The transformation will look like this
     * 
     * <pre>
     * &#64;ClientMethod
     * public abstract CompletableFuture&lt;Map&lt;String, Integer&gt;&gt; method(SignalRSession session, String param);
     * </pre>
     * 
     * Will be transformed into this
     * 
     * <pre>
     * &#64;ClientMethod
     * public CompletableFuture&lt;Map&lt;String, Integer&gt;&gt; method(SignalRSession session, String param) {
     *     return session.invoke("method", NestedTypeReferenceBuilder.buildFromNames(
     *             "java.util.Map", "java.lang.String", "java.lang.Integer"), param);
     * }
     * </pre>
     * 
     * For more information check out
     * {@link SignalRSession#invoke(String, JavaType, Object...)}
     * 
     * @param method    - The definition of the method
     * @param mv        - The method visitor to use
     * @param arguments - The arguments of the method (without the session)
     */
    public void generateClientMethodBytecode(ClientMethodDefinition method, MethodVisitor mv, Type[] arguments) {
        // This will call and place the result of the SignalRSession.invoke method on
        // the stack.
        visitSessionInvokeMethodInstruction(method, mv, arguments, true);
        // After this the stack looks like this: [CompletableFuture]

        mv.visitLabel(new Label());

        // Simply return the CompletableFuture
        mv.visitInsn(Opcodes.ARETURN);

        // Since COMPUTE_MAXS is enabled, 0,0 is fine
        mv.visitMaxs(0, 0);
        mv.visitEnd();
    }

    /**
     * This will check if the given type is a {@link CompletableFuture}.
     * 
     * @param type - The type to check
     * @return - True if the type is a {@link CompletableFuture}, false otherwise
     */
    @Override
    public boolean canHandle(@NonNull Type type) {
        return type.equals(Type.getType(CompletableFuture.class));
    }
}
