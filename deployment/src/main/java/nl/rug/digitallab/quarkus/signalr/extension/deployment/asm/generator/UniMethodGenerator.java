package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import com.fasterxml.jackson.databind.JavaType;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.groups.UniCreate;
import lombok.NonNull;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.concurrent.CompletionStage;

/**
 * This class is used to generate the necessary bytecode to invoke a client
 * method when the return type is a {@link Uni}.
 */
public class UniMethodGenerator extends ClientMethodGenerator {
    /**
     * This will generate the required bytecode for a client method with a
     * {@link Uni} return type. The transformation will look like this
     * 
     * <pre>
     * &#64;ClientMethod
     * public abstract CompletableFuture&lt;Map&lt;String, Integer&gt;&gt; method(SignalRSession session, String param);
     * </pre>
     * 
     * Will be transformed into this
     * 
     * <pre>
     * public CompletableFuture&lt;Map&lt;String, Integer&gt;&gt; method(SignalRSession session, String param) {
     *     return Uni.createFrom().completionStage(
     *             session.invoke("method", NestedTypeReferenceBuilder.buildFromNames(
     *                     "java.util.Map", "java.lang.String", "java.lang.Integer"), param));
     * }
     * </pre>
     * 
     * For more information check out
     * {@link SignalRSession#invoke(String, JavaType, Object...)}
     * 
     * @param method    - The definition of the method
     * @param mv        - The method visitor to use
     * @param arguments - The arguments of the method (without the session)
     */
    public void generateClientMethodBytecode(ClientMethodDefinition method, MethodVisitor mv, Type[] arguments) {

        // This starts by calling the method Uni.createFrom()
        // After this the stack will look like this: [UniCreate]
        visitMethodInvocation(mv, Uni.class, "createFrom", true);

        // This will call and place the result of the SignalRSession.invoke method on
        // the stack.
        visitSessionInvokeMethodInstruction(method, mv, arguments, true);
        // After this the stack looks like this: [UniCreate, CompletableFuture]

        // This will call the completionStage method on the UniCreate object,
        // essentially wrapping the CompletableFuture
        visitMethodInvocation(mv, UniCreate.class, "completionStage", false, CompletionStage.class);

        // After this the stack looks like this: [Uni]

        mv.visitLabel(new Label());
        mv.visitInsn(Opcodes.ARETURN);

        // Since COMPUTE_MAXS is enabled, 0,0 is fine
        mv.visitMaxs(0, 0);
        mv.visitEnd();
    }

    /**
     * This will check if the given type is a {@link Uni}.
     * 
     * @param type - The type to check
     * @return - True if the type is a {@link Uni}, false otherwise
     */
    @Override
    public boolean canHandle(@NonNull Type type) {
        return type.equals(Type.getType(Uni.class));
    }

}
