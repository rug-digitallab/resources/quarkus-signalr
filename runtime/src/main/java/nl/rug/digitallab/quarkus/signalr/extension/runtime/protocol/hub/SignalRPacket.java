package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake.Sequence;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.CancelInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;

import java.util.Map;

/**
 * This is the base class for all SignalR packets.
 * <p>
 * It is used to serialize and deserialize packets, and contains the type and
 * all the headers that can come with a packet.
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @Type(value = Invocation.class, name = "1"),
        @Type(value = StreamItem.class, name = "2"),
        @Type(value = Completion.class, name = "3"),
        @Type(value = StreamInvocation.class, name = "4"),
        @Type(value = CancelInvocation.class, name = "5"),
        @Type(value = Ping.class, name = "6"),
        @Type(value = CloseMessage.class, name = "7"),
        @Type(value = Ack.class, name = "8"),
        @Type(value = Sequence.class, name = "9")
})
public class SignalRPacket {
    /**
     * The type of the packet.
     */
    @NonNull
    private SignalRPacketType type;

    /**
     * List of headers in the packet. According to the Spec, neither the client nor
     * the server should respond to any headers in particular, and a server fully
     * unaware of headers is fully compliant.
     * <p>
     * That being said, it should be able to handle them, and the client should be
     * able to send them.
     */
    private Map<String, String> headers;
}
