package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition.SignalRHubMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRPacketListener;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.container.BaseResultSenderContainer;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain.senders.*;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation.PlainPendingInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This class is responsible for handling {@link Invocation} packets, and will
 * invoke the correct method on the hub instance.
 * <p>
 * In this case by "plain" we mean that the invocation is not a streaming
 * invocation.
 */
@JBossLog
public class PlainInvocationHandler
        extends BaseResultSenderContainer<Invocation, PlainInvocationResultSender<?>>
        implements SignalRPacketListener<Invocation> {

    /**
     * This will create a new InvocationHandler.
     * <p>
     * It will also register the default result senders for the different types of
     * responses.
     */
    public PlainInvocationHandler() {
        this.registerResultSender(CompletableFuture.class, new CompletableFutureResultSender());
        this.registerResultSender(Uni.class, new UniResultSender());
        this.registerResultSender(Multi.class, new MultiResultSender());
        this.registerResultSender(Object.class, new DefaultInvocationResultSender());
    }

    /**
     * This will handle the received {@link Invocation} packet.
     * <p>
     * It will look up the correct method in the hub instance and invoke it with
     * the arguments from the packet. It will also send a response back to the
     * client with the result of the invocation.
     *
     * @param session - The session from which the invocation came from.
     * @param packet  - The invocation packet.
     */
    @Override
    public void onPacketReceived(SignalRSession session, Invocation packet) {
        // Figure out what connector information
        SignalRConnector connector = session.getConnector();

        // In our case, the hub instance is useful for logging
        Object hubInstance = connector.getSignalRHubInstance();

        // Method name and arguments
        String methodName = packet.getTarget();
        List<Object> arguments = packet.getArguments();

        // This argument count refers to the actual argument count of the method
        // The SignalR protocol sends the arguments and streams separately, so we need
        // to add the stream count to the argument count.
        int argumentCount = arguments.size() + packet.getStreamCount();

        // Find the actual method on the hub instance
        SignalRHubMethod method = connector.getHubDefinition().lookupMethod(methodName, argumentCount);

        log.debug("Invoking method: " + methodName + " on " + hubInstance.getClass().getName() + " with "
                + arguments.size() + " arguments and " + packet.getStreamCount() + " streams");

        // In the SignalR protocol a non-blocking invocation has no invocation id
        // There is no way to send a completion back to the client, as there is no
        // invocation id to send it to, and the client will not be expecting one.
        boolean blocking = packet.getInvocationId() != null;

        // If no method is found, we should send a completion with an error message
        if (method == null) {
            log.warn("Client requested non-existent method: " + methodName + " on " + hubInstance.getClass().getName()
                    + " with " + arguments.size() + " arguments and " + packet.getStreamCount() + " streams");
            // According to the protocol we should never send a completion for a
            // non-blocking invocation (even if an error occurs)
            if (blocking) {
                Completion completion = new Completion(packet.getInvocationId(), null, "Method not found");
                session.sendMessage(completion);
            }

            return;
        }

        if (blocking) {
            invokeMethodAndRespond(session, packet, connector, method);
        } else {
            invokeNonBlockingMethod(session, packet, connector, method);
        }
    }

    /**
     * This will invoke the method on the hub instance and send the response back to
     * the client, using the correct {@link PlainInvocationResultSender}.
     *
     * @param session      - The session from which the invocation came from.
     * @param packet       - The invocation packet.
     * @param connector    - The connector of the hub
     * @param methodHandle - The method handle to invoke
     */
    private void invokeMethodAndRespond(SignalRSession session, Invocation packet, SignalRConnector connector, SignalRHubMethod methodHandle) {
        CompletableFuture<Object> resultingFuture = new CompletableFuture<>();
        PlainPendingInvocation<?> pendingInvocation = new PlainPendingInvocation<>(methodHandle.getReturnType(), resultingFuture);

        // Add the pending invocation to the session
        session.getInvocationManager().addIncomingInvocation(packet.getInvocationId(), pendingInvocation);

        try {
            // Figure out the result of the invocation
            Object result = connector.invokeOnHubInstance(methodHandle, session, packet.getArguments(), packet.getStreamIds());

            // Complete the future with the result
            resultingFuture.complete(result);

            // Find the correct result sender and send the response.
            @SuppressWarnings("unchecked")
            PlainInvocationResultSender<Object> resultSender = (PlainInvocationResultSender<Object>)
                    lookupResultSender(result);
            resultSender.handleResult(session, packet, result);
        } catch (Exception e) {
            log.error("Error invoking method", e);

            // Let the client know that an error occurred
            Completion completion = new Completion(packet.getInvocationId(), null, e.getMessage());
            session.sendMessage(completion);

            // Forward the exception to the future
            if (!resultingFuture.isDone()) {
                resultingFuture.completeExceptionally(e);
            }
        }

        // Remove the pending invocation from the session
        if (session.getInvocationManager().hasIncomingInvocation(packet.getInvocationId())) {
            session.getInvocationManager().removeIncomingInvocation(packet.getInvocationId());
        }
    }

    /**
     * This will invoke the method on the hub instance, and will avoid sending
     * any sort of completion or error message back to the client.
     *
     * @param session      - The session from which the invocation came from.
     * @param packet       - The invocation packet.
     * @param connector    - The connector of the hub
     * @param methodHandle - The method handle to invoke
     */
    private void invokeNonBlockingMethod(SignalRSession session, Invocation packet, SignalRConnector connector, SignalRHubMethod methodHandle) {
        CompletableFuture<Void> resultingFuture = new CompletableFuture<>();
        PlainPendingInvocation<?> pendingInvocation = new PlainPendingInvocation<>(null, resultingFuture);

        // Add the pending invocation to the session
        session.getInvocationManager().addIncomingInvocation(packet.getInvocationId(), pendingInvocation);

        try {
            // Figure out the result of the invocation
            // Since the method is non-blocking, we should not send a completion, even if an error does occur
            connector.invokeOnHubInstance(methodHandle, session, packet.getArguments(), packet.getStreamIds());

            // Complete the future
            resultingFuture.complete(null);
        } catch (Exception e) {
            log.error("Error invoking method", e);

            // Forward the exception to the future
            if (!resultingFuture.isDone()) {
                resultingFuture.completeExceptionally(e);
            }
        }

        // Remove the pending invocation from the session
        if (session.getInvocationManager().hasIncomingInvocation(packet.getInvocationId())) {
            session.getInvocationManager().removeIncomingInvocation(packet.getInvocationId());
        }
    }
}