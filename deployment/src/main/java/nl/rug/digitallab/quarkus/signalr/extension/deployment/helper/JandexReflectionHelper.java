package nl.rug.digitallab.quarkus.signalr.extension.deployment.helper;

import lombok.SneakyThrows;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import org.jboss.jandex.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * This class provides helper methods to extract information from Jandex.
 */
public final class JandexReflectionHelper {
    private JandexReflectionHelper() {
        throw new UnsupportedOperationException("This class should not be instantiated!");
    }

    /**
     * This method gets the class tree for a given class name.
     * It will look up the class hierarchy and interfaces.
     *
     * @param className - The class name
     * @param view      - The index view
     * @return - A set of class names
     */
    public static Set<DotName> getClassTree(DotName className, IndexView view) {
        Set<DotName> classes = new HashSet<>();
        ClassInfo info = view.getClassByName(className);
        if (info != null) {
            classes.add(className);
            if (info.superName() != null) {
                classes.addAll(getClassTree(info.superName(), view));
            }

            // Add interfaces
            info.interfaceNames().stream()
                    .map(interfaceName -> getClassTree(interfaceName, view))
                    .forEach(classes::addAll);
        }

        return classes;
    }

    /**
     * This method extracts all client methods from a class.
     * <p>
     * In this context, a client method is a method annotated with
     * {@link ClientMethod}.
     *
     * @param info - The class info to extract the methods from
     * @return - A set of client method definitions
     */
    public static Set<ClientMethodDefinition> extractClientMethodsFrom(ClassInfo info) {
        Set<ClientMethodDefinition> methods = new HashSet<>();
        for (MethodInfo method : info.methods()) {
            AnnotationInstance clientMethod = method.annotation(ClientMethod.class.getName());
            if (clientMethod == null) {
                continue;
            }

            String[] exceptions = method.exceptions().stream()
                    .map(c -> c.name().toString().replace(".", "/"))
                    .toArray(String[]::new);

            ClientMethodDefinition definition = new ClientMethodDefinition(
                    method.flags(),
                    method.name(),
                    method.descriptor(),
                    method.genericSignature(),
                    exceptions,
                    method.annotations()
            );

            methods.add(definition);
        }

        return methods;
    }

    /**
     * This method fakes the Quarkus index to remove the abstract flag from the
     * class
     *
     * @param info - The class info to alter
     */
    @SneakyThrows(ReflectiveOperationException.class)
    @SuppressWarnings("java:S3011") // We're bypassing the Jandex API here, this is needed.
    public static void fakeQuarkusIndex(ClassInfo info) {
        short flags = info.flags();
        short newFlags = (short) (flags & ~Modifier.ABSTRACT);

        Method method = ClassInfo.class.getDeclaredMethod("setFlags", short.class);
        method.setAccessible(true);
        method.invoke(info, newFlags);
    }
}
