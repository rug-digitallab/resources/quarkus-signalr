package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.senders;

import io.smallrye.mutiny.subscription.Cancellable;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.concurrent.CompletableFuture;

/**
 * An adapter for {@link StreamInvocation} that uses {@link CompletableFuture}
 * as the result type.
 */
public class StreamCompletableFutureResultSender extends StreamInvocationResultSender<CompletableFuture<?>> {

    @Override
    public void handleResult(SignalRSession session, StreamInvocation packet, CompletableFuture<?> methodResult) {
        methodResult.whenComplete((result, e) -> {
            if (e != null) {
                this.sendCompletion(session, packet, e.getMessage());
                return;
            }

            this.sendItemAndCompletion(session, packet, result);
        });

        // Override the preliminary completion with a cancellable subscription
        Cancellable subscription = () -> methodResult.cancel(true);
        session.getInvocationManager().addIncomingStreamInvocation(packet.getInvocationId(), subscription);
    }
}
