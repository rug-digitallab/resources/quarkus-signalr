package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.visitor;

/**
 * This class is used to specify the settings for the ApprovalTests.
 */
public class PackageSettings {
    /**
     * This is where the base directory for the approvals is located.
     */
    @SuppressWarnings("java:S3008") // ApprovalTests needs this exact variable name
    public static String ApprovalBaseDirectory = "../resources/approvals/";
}
