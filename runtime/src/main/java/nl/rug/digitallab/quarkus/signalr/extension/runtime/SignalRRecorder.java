package nl.rug.digitallab.quarkus.signalr.extension.runtime;

import io.quarkus.arc.Arc;
import io.quarkus.runtime.annotations.Recorder;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnectorManager;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.http.SignalRNegotiationHandler;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.websockets.SignalRWebsocket;

import java.util.Set;

/**
 * The recorder for the SignalR extension.
 * <p>
 * This handles deployment of the SignalR extension hubs and handlers.
 */
@Recorder
public class SignalRRecorder {
    /**
     * Adds a hub to the SignalR connector manager.
     * 
     * @param hubPath - The path of the hub
     * @param hubClass - The class of the hub
     */
    public void addHub(String hubPath, String hubClass, Set<TransportType> transports) {
        Arc.container().instance(SignalRConnectorManager.class).get().addHub(hubPath, hubClass, transports);
    }

    /**
     * Creates a handler for the SignalR Websocket.
     * 
     * @param path - The path of the handler
     * @return - The handler
     */
    public Handler<RoutingContext> createWebsocketHandler(String path) {
        return new SignalRWebsocket(path);
    }

    /**
     * Creates a handler for the SignalR negotiation protocol.
     * This method expects a path, however, it is only used to
     * detect the {@link SignalRConnector}, therefore, the
     * /negotiate path should be omitted.
     * 
     * @param path - The path of the handler
     * @return - The handler
     */
    public Handler<RoutingContext> createNegotiationHandler(String path) {
        return new SignalRNegotiationHandler(path);
    }
}
