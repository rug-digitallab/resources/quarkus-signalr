package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This is the Ack message, it is used to acknowledge all messages up to a
 * certain point.
 * 
 * @see <a href=
 *      https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#ack-message-encoding">
 *      Ack message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Ack extends SignalRPacket {

    /** The sequence number of the last received packet. */
    private int sequenceId;

    /**
     * Instantiates a new ack packet.
     */
    public Ack() {
        super(SignalRPacketType.ACK);
    }

    /**
     * Instantiates a new ack packet.
     * 
     * @param sequenceId - The sequence number of the last received packet.
     */
    public Ack(int sequenceId) {
        super(SignalRPacketType.ACK);
        this.sequenceId = sequenceId;
    }

}
