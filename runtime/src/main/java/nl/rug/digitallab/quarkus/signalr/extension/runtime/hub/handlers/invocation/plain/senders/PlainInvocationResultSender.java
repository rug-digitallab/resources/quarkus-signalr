package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain.senders;

import lombok.NonNull;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.container.BaseInvocationResultSender;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * Classes implementing this interface are responsible for serving the response
 * to the client once the invocation has happened on the server.
 * <p>
 * As an example, if the client invokes a method on the server which returns a
 * CompletableFuture, this adapter will be responsible for registering a
 * listener on the CompletableFuture and sending the response to the client once
 * the future is completed.
 * <p>
 * In this case, by Simple, we mean that the method was triggered by a
 * {@link Invocation} packet, and is not a stream invocation.
 * <p>
 * The naming is required in order to not collide with the base type
 * {@link BaseInvocationResultSender}.
 * 
 * @param <T> - The type of the result of the invocation.
 */
public abstract class PlainInvocationResultSender<T> implements BaseInvocationResultSender<Invocation, T> {
    /**
     * This is a convenience method that will send a completion to the client.
     * 
     * @param session - The session that originated the invocation.
     * @param packet  - The invocation packet.
     * @param error   - The error message.
     */
    protected void sendErroredCompletion(SignalRSession session, Invocation packet, @NonNull String error) {
        session.sendMessage(new Completion(packet.getInvocationId(), null, error));
    }

    /**
     * This is a convenience method that will send a completion with a given result
     * to the client.
     * 
     * @param session - The session that originated the invocation.
     * @param packet  - The invocation packet.
     * @param result  - The result of the invocation.
     */
    protected void sendCompletion(SignalRSession session, Invocation packet, Object result) {
        session.sendMessage(new Completion(packet.getInvocationId(), result, null));
    }

}