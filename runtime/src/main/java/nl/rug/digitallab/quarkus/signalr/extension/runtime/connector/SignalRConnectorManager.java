package nl.rug.digitallab.quarkus.signalr.extension.runtime.connector;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import io.quarkus.arc.All;
import io.quarkus.arc.Arc;
import io.quarkus.jackson.ObjectMapperCustomizer;
import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.Getter;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class manages the SignalR connectors.
 */
@JBossLog
@ApplicationScoped
public class SignalRConnectorManager {
    /**
     * The definitions for the SignalR hubs. Mapped by path.
     */
    private Map<String, SignalRHubDefinition> hubDefinitions = new HashMap<>();

    /**
     * The object mapper for the SignalR protocol.
     */
    @Getter
    private ObjectMapper objectMapper;

    /**
     * This will set up the object mapper for use within the SignalR protocol.
     * 
     * @param customizers - The customizers for the object mapper already present
     */
    @Inject
    void createObjectMapper(@All List<ObjectMapperCustomizer> customizers) {
        ObjectMapper mapper = JsonMapper.builder(new JsonFactory())
                .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
                .build();

        for (ObjectMapperCustomizer customizer : customizers) {
            customizer.customize(mapper);
        }

        this.objectMapper = mapper;
    }

    /**
     * Get a connector for a given path.
     * This will lazily create a connector if one does not exist.
     * 
     * @param path - The path to the connector
     * @return - The connector
     */
    public SignalRConnector getConnector(String path) {
        SignalRHubDefinition definition = hubDefinitions.get(path);
        if (definition.getConnector() != null) {
            return definition.getConnector();
        }

        String hubClassName = definition.getClassName();
        log.info("Attempting to create connector for path: " + path + " with hub class: " + hubClassName);
        try {
            Class<?> hubClazz = loadClass(hubClassName);

            // Query the Arc container for the endpoint
            Object beanInstance = Arc.container().instance(hubClazz).get();
            SignalRConnector connector = new SignalRConnector(beanInstance, objectMapper, definition);
            definition.setConnector(connector);

            return connector;
        } catch (ClassNotFoundException e) {
            // This should technically never happen unless the class was removed between
            // build and runtime, which, if it happened, we have bigger problems
            log.error("Hub class not found: " + hubClassName, e);
            throw new RuntimeException("Hub class not found: " + hubClassName, e);
        }
    }

    /**
     * Add a hub to the manager.
     * 
     * @param path       - The path to the hub
     * @param hubClass   - The class of the hub
     * @param transports - The transports supported by the hub
     */
    public void addHub(String path, String hubClass, Set<TransportType> transports) {
        SignalRHubDefinition definition = new SignalRHubDefinition(hubClass, path, transports);
        hubDefinitions.put(path, definition);
    }

    /**
     * Load a class by name. This will first try to use the system class loader and
     * then fall back to the thread context class loader.
     * 
     * @param className - The name of the class
     * @return - The class object
     * @throws ClassNotFoundException - If the class could not be found
     */
    private static Class<?> loadClass(String className) throws ClassNotFoundException {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            return Thread.currentThread().getContextClassLoader().loadClass(className);
        }
    }

    /**
     * This will broadcast pings to all connectors, to ensure they are still alive.
     */
    @Scheduled(every = "${quarkus.signalr.ping-interval}")
    void broadcastPings() {
        for (SignalRHubDefinition definition : hubDefinitions.values()) {
            SignalRConnector connector = getConnector(definition.getPath());
            if (connector != null) {
                connector.broadcastPing();
            }
        }
    }
}
