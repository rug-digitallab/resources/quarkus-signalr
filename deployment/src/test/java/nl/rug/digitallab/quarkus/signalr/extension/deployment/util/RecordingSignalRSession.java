package nl.rug.digitallab.quarkus.signalr.extension.deployment.util;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smallrye.mutiny.Multi;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.connector.SignalRConnector;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Getter
public class RecordingSignalRSession extends SignalRSession {
    private final List<RecordedInvocation> invocations = new ArrayList<>();

    public RecordingSignalRSession(@NonNull UUID connectionId, @NonNull UUID connectionToken, @NonNull SignalRConnector connector,
                                   @NonNull ObjectMapper objectMapper, int negotiateVersion) {
        super(connectionId, connectionToken, connector, objectMapper, negotiateVersion);
    }

    public boolean isMethodInvoked(String name) {
        return invocations.stream().anyMatch(i -> i.getMethodName().equals(name));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> CompletableFuture<T> invoke(String methodName, JavaType returnType, Object... params) {
        RecordedInvocation invocation = new RecordedInvocation(false, methodName, returnType, params);
        invocations.add(invocation);

        if (returnType == null) {
            return CompletableFuture.completedFuture(null);
        }

        return CompletableFuture.completedFuture((T) JavaTypeDataFaker.generateFakeData(returnType));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Multi<T> invokeStreamable(String methodName, JavaType itemType, Object... params) {
        RecordedInvocation invocation = new RecordedInvocation(true, methodName, itemType, params);
        invocations.add(invocation);

        return Multi.createFrom().items((T) JavaTypeDataFaker.generateFakeData(itemType));
    }

    @Data
    @AllArgsConstructor
    public static class RecordedInvocation {
        private final boolean isStreamable;
        private final String methodName;
        private final JavaType returnType;
        private final Object[] params;
    }
}
