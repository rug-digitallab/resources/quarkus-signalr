package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is a simple handshake request that is sent to the SignalR server.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HandshakeRequest {
    /** This is the protocol requested by the client. json or messagepack */
    private ProtocolFormat protocol;

    /** This is the version of the protocol. Currently, always 1 */
    private int version;
}
