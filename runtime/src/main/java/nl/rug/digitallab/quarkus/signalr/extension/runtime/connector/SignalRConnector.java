package nl.rug.digitallab.quarkus.signalr.extension.runtime.connector;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smallrye.mutiny.Multi;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub.HubMethodInvocationException;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition.SignalRHubMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRPacketListener;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.cancellation.CancelInvocationHandler;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.completion.CompletionHandler;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain.PlainInvocationHandler;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.StreamInvocationHandler;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.stream.StreamItemHandler;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.CloseMessage;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.Ping;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake.HandshakeRequest;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake.HandshakeResponse;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake.ProtocolFormat;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.CancelInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.StreamInvocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.serialization.SignalRParameterAdapter;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class backs each SignalR hub instance, and is responsible for managing
 * the sessions and handling the messages.
 * <p>
 * It is the class responsible for invoking the hub methods.
 */
@JBossLog
public class SignalRConnector {
    /**
     * The instance of the SignalR hub it is backing.
     */
    @Getter
    private Object signalRHubInstance;

    @Getter
    private ObjectMapper objectMapper;

    @Getter
    private SignalRHubDefinition hubDefinition;

    /**
     * List of active sessions, mapped by connectionToken
     */
    private Map<UUID, SignalRSession> sessions = new ConcurrentHashMap<>();

    /**
     * List of listeners for each packet type
     */
    private Map<Class<? extends SignalRPacket>, List<SignalRPacketListener<? extends SignalRPacket>>> listeners = new ConcurrentHashMap<>();

    /**
     * This will create a new SignalR connector.
     *
     * @param signalRHubInstance - The instance of the SignalR hub it is backing.
     * @param objectMapper       - The object mapper for the SignalR protocol.
     * @param hubDefinition      - The definition of the hub.
     */
    public SignalRConnector(Object signalRHubInstance, ObjectMapper objectMapper, SignalRHubDefinition hubDefinition) {
        this.signalRHubInstance = signalRHubInstance;
        this.objectMapper = objectMapper;
        this.hubDefinition = hubDefinition;

        this.registerDefaultListeners();
    }

    private void registerDefaultListeners() {
        this.addListener(Invocation.class, new PlainInvocationHandler());
        this.addListener(StreamInvocation.class, new StreamInvocationHandler());
        this.addListener(StreamItem.class, new StreamItemHandler(this.objectMapper));
        this.addListener(Completion.class, new CompletionHandler(this.objectMapper));
        this.addListener(CancelInvocation.class, new CancelInvocationHandler());
    }

    /**
     * This will handle a message. It is expected to be in JSON
     * and will be parsed by the SignalR protocol.
     *
     * @param session - The session that received the message.
     * @param message - The message to handle.
     */
    public void handleMessage(SignalRSession session, String message) {
        if (session.isHandshakeComplete()) {
            // Handle the message
            try {
                SignalRPacket packet = objectMapper.readValue(message, SignalRPacket.class);
                Class<?> packetType = packet.getType().getPacketClass();
                if (!listeners.containsKey(packetType)) {
                    // No listeners for this packet type, ignore
                    log.trace("No listeners for packet type: " + packetType);
                    return;
                }

                log.debug("C->S: " + packet);
                dispatchToListeners(session, message, packet);
            } catch (JsonProcessingException e) {
                log.error("Failed to parse message", e);
            }

            return;
        }

        // Handle the handshake
        try {
            HandshakeRequest handshakeRequest = objectMapper.readValue(message, HandshakeRequest.class);

            // Ensure the requested protocol is json and the version is 1
            if (
                    !ProtocolFormat.JSON.equals(handshakeRequest.getProtocol())
                            || handshakeRequest.getVersion() != 1
            ) {
                String errorMessage = "Invalid handshake request";

                log.error(errorMessage);

                session.sendRawMessage(getSerializedHandshakeResponse(errorMessage));
                return;
            }

            session.sendRawMessage(objectMapper.writeValueAsString(new HandshakeResponse()));
            session.setHandshakeComplete(true);
        } catch (JsonProcessingException e) {
            String errorMessage = "Failed to parse handshake request";
            log.error(errorMessage, e);

            session.sendRawMessage(getSerializedHandshakeResponse(errorMessage));
        }
    }

    /**
     * This will dispatch the message to all listeners for the packet type.
     *
     * @param session - The session that received the message.
     * @param message - The message that was received.
     * @param packet  - The packet that was received.
     */
    @SuppressWarnings({"unchecked", "rawtypes"}) // Needed for listeners!
    private void dispatchToListeners(SignalRSession session, String message, SignalRPacket packet) {
        Class<?> packetType = packet.getType().getPacketClass();
        for (SignalRPacketListener listener : listeners.get(packetType)) {
            try {
                listener.onPacketReceived(session, packet);
            } catch (Exception e) {
                // Should any other exception happen, log it, and close the connection
                log.error("Failed to handle message of type %s with listener %s for session %s with message %s"
                        .formatted(packetType, listener, session, message), e);
                session.sendMessage(new CloseMessage(e.getMessage(), true));

                // TODO (once connection lifecycle is properly done): Close the connection.
            }
        }
    }

    /**
     * This will get the serialized handshake response.
     *
     * @param error - The error to send back. (null if no error)
     * @return - The serialized handshake response.
     */
    @SneakyThrows(JsonProcessingException.class)
    public String getSerializedHandshakeResponse(String error) {
        HandshakeResponse response = new HandshakeResponse(error);

        return objectMapper.writeValueAsString(response);
    }

    /**
     * This will handle a close event.
     *
     * @param session - The session that was closed.
     */
    public void handleClose(SignalRSession session) {
        sessions.remove(session.getConnectionToken());
    }

    /**
     * This will create a new session for the SignalR connector.
     *
     * @param negotiateVersion - The version of the protocol to negotiate.
     * @return - The new session.
     */
    public SignalRSession newSession(int negotiateVersion) {
        UUID connectionId = UUID.randomUUID();
        UUID connectionToken = UUID.randomUUID();

        SignalRSession session = new SignalRSession(connectionId, connectionToken, this, objectMapper,
                negotiateVersion);

        sessions.put(connectionToken, session);

        return session;
    }

    /**
     * This will get a session by the connection token.
     *
     * @param connectionToken - The connection token to get the session for.
     * @return - The session.
     */
    public SignalRSession getSession(UUID connectionToken) {
        return sessions.get(connectionToken);
    }

    /**
     * Broadcasts a ping to all sessions.
     */
    public void broadcastPing() {
        Ping ping = new Ping();
        try {
            String pingMessage = objectMapper.writeValueAsString(ping);
            sessions.values().stream()
                    .filter(SignalRSession::isHandshakeComplete)
                    .forEach(session -> session.sendRawMessage(pingMessage));
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize ping", e);
        }
    }

    /**
     * This will add a listener for a packet type.
     *
     * @param packetType - The packet type to listen for.
     * @param listener   - The listener to add.
     * @param <T>        - The type of the packet.
     */
    public <T extends SignalRPacket> void addListener(Class<T> packetType, SignalRPacketListener<T> listener) {
        listeners.computeIfAbsent(packetType, key -> new ArrayList<>());

        List<SignalRPacketListener<? extends SignalRPacket>> packetListeners = listeners.get(packetType);
        if (!packetListeners.contains(listener)) {
            packetListeners.add(listener);
        } else {
            throw new IllegalArgumentException("Listener already added");
        }
    }

    /**
     * This will invoke a method on the hub instance.
     * <p>
     * By unadapted parameters, we mean the parameters that are not adapted to the
     * method signature, meaning straight out of Jackson.
     * <p>
     * It will also map stream ids to the correct streams. In this case, the only
     * stream which is supported is the {@link Multi} stream.
     * <p>
     * Arguments will be placed in order, being mixed with the stream ids, depending
     * on the parameter type.
     *
     * @param method              - The hub method handle to invoke.
     * @param session             - The session that invoked the method.
     * @param unadaptedParameters - The list of unadapted parameters.
     * @param streamIds           - The list of stream ids.
     * @return - The result of the method invocation.
     * @throws HubMethodInvocationException - If the method invocation fails.
     */
    public Object invokeOnHubInstance(SignalRHubMethod method, SignalRSession session,
                                      List<Object> unadaptedParameters, List<String> streamIds) throws HubMethodInvocationException {
        // Create a parameter adapter bound to the method and session
        SignalRParameterAdapter parameterAdapter = new SignalRParameterAdapter(method, session.getStreamManager(), objectMapper);

        // Ensure stream ids are not null
        // This can happen as the spec does not mention whether stream ids can be null, or an empty list
        // and clients do both.
        if (streamIds == null) {
            streamIds = List.of();
        }

        List<Object> adaptedParameters = parameterAdapter.adaptParameters(unadaptedParameters, streamIds);

        // Add the session as the first parameter for the method signature
        adaptedParameters.addFirst(session);

        // Add the hub instance as the underlying object for the method
        adaptedParameters.addFirst(signalRHubInstance);

        // This way the first two arguments are the hub instance and the session, which are the
        // underlying object and first parameter of the method signature, respectively.

        try {
            return method.getMethodHandle().invoke(adaptedParameters.toArray());
        } catch (Throwable t) {
            throw new HubMethodInvocationException(method, t);
        }
    }
}
