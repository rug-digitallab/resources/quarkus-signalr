package nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;

import java.lang.annotation.*;

/**
 * Annotate a class with this to expose it as a SignalR hub.
 * <p>
 * This can only be applied on a class and will not take into account
 * surrounding paths (such as those defined by @Path), regardless of nesting.
 * <p>
 * Once annotated, methods in the class will be exposed as SignalR hub methods.
 * It should be noted, an additional path will be registered for the hub, which
 * will be "your-path/negotiate". This is used by the client to negotiate the
 * connection.
 * <p>
 * Classes annotated with this method can expose methods to the client through
 * the {@link ServerMethod} annotation, and can invoke client methods through the
 * {@link ClientMethod} annotation.
 * 
 * @see ServerMethod
 * @see ClientMethod
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SignalRHub {
    /**
     * The path to expose the hub on.
     * 
     * @return - The path to expose the hub on.
     */
    String value();

    /**
     * The transports the hub will support. If none are provided, all transports
     * will be supported. See {@link TransportType} for available transports.
     * 
     * @return - The transports the hub will support.
     */
    TransportType[] transports() default {};
}
