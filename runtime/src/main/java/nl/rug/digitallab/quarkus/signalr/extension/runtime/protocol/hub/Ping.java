package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This is the Ping message, it is used to keep the connection alive.
 * 
 * @see <a href=
 *      "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#streaminvocation-message-encoding">
 *      Ping message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Ping extends SignalRPacket {
    /**
     * Instantiates a new ping packet.
     */
    public Ping() {
        super(SignalRPacketType.PING);
    }
}
