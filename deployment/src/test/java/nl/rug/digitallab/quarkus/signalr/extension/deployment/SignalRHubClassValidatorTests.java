package nl.rug.digitallab.quarkus.signalr.extension.deployment;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ServerMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.SignalRHub;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.jboss.jandex.DotName;
import org.jboss.jandex.Index;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.rug.digitallab.quarkus.signalr.extension.deployment.helper.JandexReflectionHelper.extractClientMethodsFrom;
import static nl.rug.digitallab.quarkus.signalr.extension.deployment.helper.JandexReflectionHelper.getClassTree;
import static org.junit.jupiter.api.Assertions.*;

class SignalRHubClassValidatorTests {
    static Map<Class<?>, SignalRHubClassValidator> validatorMap = new HashMap<>();

    @BeforeAll
    static void setupValidators() throws IOException {
        List<Class<?>> classes = List.of(
                InvalidHubInterface.class,

                TestClassWhichWillBeExtended.class,
                TestClassWhichExtends.class,

                TestClassWithNonAbstractClientMethod.class,

                TestClassWithAMethodWithBothAnnotations.class,

                TestClassWithAbstractServerMethods.class,

                TestClassWithWrongArgumentClientMethod.class,
                TestClassWithWrongArgumentServerMethod.class,

                TestHubWithPrivateServerMethod.class,
                TestHubWithProtectedClientMethod.class,

                TestClassWithOverloadedMethods.class,

                TestValidHub.class
        );

        Index index = Index.of(classes);

        for (Class<?> clazz : classes) {
            SignalRHubClassValidator validator = new SignalRHubClassValidator(
                    clazz.getName(),
                    index,
                    getClassTree(DotName.createSimple(clazz), index),
                    extractClientMethodsFrom(index.getClassByName(clazz))
            );

            validatorMap.put(clazz, validator);
        }
    }

    @Test
    @DisplayName("Ensure interfaces are not valid hubs")
    void testInvalidHubInterface() {
        SignalRHubClassValidator validator = validatorMap.get(InvalidHubInterface.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);

        assertEquals("SignalR hub classes cannot be interfaces! Offending class: "
                + InvalidHubInterface.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that hubs have to be leaf classes")
    void testHubInheritance() {
        SignalRHubClassValidator validator = validatorMap.get(TestClassWhichWillBeExtended.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("SignalR hub classes should not be used as parent classes! Offending class: "
                + TestClassWhichWillBeExtended.class.getName() + " is extended by known subclasses: [" +
                TestClassWhichExtends.class.getName() + "]", exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that client methods are abstract")
    void testNonAbstractClientMethod() {
        SignalRHubClassValidator validator = validatorMap.get(TestClassWithNonAbstractClientMethod.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Client methods must be abstract! Offending method: testMethod inside of "
                + TestClassWithNonAbstractClientMethod.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that methods cannot have both client and server annotations")
    void testMethodWithBothAnnotations() {
        SignalRHubClassValidator validator = validatorMap.get(TestClassWithAMethodWithBothAnnotations.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Methods cannot have both @ServerMethod and @ClientMethod! Offending method: testMethod of: "
                + TestClassWithAMethodWithBothAnnotations.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that server methods are never abstract")
    void testAbstractServerMethod() {
        SignalRHubClassValidator validator = validatorMap.get(TestClassWithAbstractServerMethods.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Abstract methods in SignalR hub classes must be annotated with @ClientMethod! Offending method: testMethod in: "
                + TestClassWithAbstractServerMethods.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that hubs always have the session as their first argument")
    void testWrongArgumentClientMethod() {
        SignalRHubClassValidator validator = validatorMap.get(TestClassWithWrongArgumentClientMethod.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Methods annotated with @ServerMethod or @ClientMethod must " +
                "have SignalRSession as their first argument! Offending method: testMethod of class: "
                + TestClassWithWrongArgumentClientMethod.class.getName(), exception.getMessage());

        validator = validatorMap.get(TestClassWithWrongArgumentServerMethod.class);
        exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Methods annotated with @ServerMethod or @ClientMethod must " +
                "have SignalRSession as their first argument! Offending method: testMethod of class: "
                + TestClassWithWrongArgumentServerMethod.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that server methods are public")
    void testPrivateServerMethod() {
        SignalRHubClassValidator validator = validatorMap.get(TestHubWithPrivateServerMethod.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Server methods must be public! Offending method: testMethod in: " +
                TestHubWithPrivateServerMethod.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that client methods are public")
    void testPrivateClientMethod() {
        SignalRHubClassValidator validator = validatorMap.get(TestHubWithProtectedClientMethod.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Client methods must be public! Offending method: testMethod in class: " +
                TestHubWithProtectedClientMethod.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that methods cannot be overloaded")
    void testOverloadedMethods() {
        SignalRHubClassValidator validator = validatorMap.get(TestClassWithOverloadedMethods.class);

        RuntimeException exception = assertThrows(RuntimeException.class, validator::runValidation);
        assertEquals("Client methods and server methods cannot be overloaded! Offending method: testMethod in class: " +
                TestClassWithOverloadedMethods.class.getName(), exception.getMessage());
    }

    @Test
    @DisplayName("Ensure that valid hubs pass validation")
    void testValidHub() {
        SignalRHubClassValidator validator = validatorMap.get(TestValidHub.class);

        assertDoesNotThrow(validator::runValidation);
    }

    @SignalRHub("/chat")
    public interface InvalidHubInterface {
    }

    @SignalRHub("/chat")
    public class TestClassWhichWillBeExtended {
    }

    @SignalRHub("/chat")
    public class TestClassWhichExtends extends TestClassWhichWillBeExtended {
    }

    @SignalRHub("/chat")
    public class TestClassWithNonAbstractClientMethod {
        @ClientMethod
        public void testMethod() {
            /* Just a placeholder */
        }
    }

    @SignalRHub("/chat")
    public abstract class TestClassWithAMethodWithBothAnnotations {
        @ClientMethod
        @ServerMethod
        public abstract void testMethod();
    }

    @SignalRHub("/chat")
    public abstract class TestClassWithAbstractServerMethods {
        @ServerMethod
        public abstract void testMethod();
    }

    @SignalRHub("/chat")
    public abstract class TestClassWithWrongArgumentClientMethod {
        @ClientMethod
        public abstract void testMethod(String arg);
    }

    @SignalRHub("/chat")
    public class TestClassWithWrongArgumentServerMethod {
        @ServerMethod
        public void testMethod() {
            /* Just a placeholder */
        }
    }

    @SignalRHub("/chat")
    public class TestHubWithPrivateServerMethod {
        @ServerMethod
        private void testMethod(SignalRSession hub) {
            /* Just a placeholder */
        }
    }

    @SignalRHub("/chat")
    public abstract class TestHubWithProtectedClientMethod {
        @ClientMethod
        protected abstract void testMethod(SignalRSession hub);
    }

    @SignalRHub("/chat")
    public abstract class TestClassWithOverloadedMethods {
        @ClientMethod
        public abstract void testMethod(SignalRSession hub);

        @ClientMethod
        public abstract void testMethod(SignalRSession hub, String arg);
    }

    @SignalRHub("/chat")
    public abstract class TestValidHub {
        @ServerMethod
        public void testMethod(SignalRSession session, int a) {

        }

        @ServerMethod
        public void testOtherMethod(SignalRSession session) {

        }

        @ClientMethod
        public abstract void testClientMethod(SignalRSession session);
    }
}
