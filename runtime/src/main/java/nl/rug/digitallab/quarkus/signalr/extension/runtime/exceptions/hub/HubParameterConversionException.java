package nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub;

import lombok.Getter;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition.SignalRHubMethod;

@Getter
public class HubParameterConversionException extends Exception {
    private final String methodName;
    private final String parameterName;

    public HubParameterConversionException(String issue, SignalRHubMethod method, String parameterName, Throwable cause) {
        super(issue + " in method " + method.getMethodName() + " for parameter " + parameterName, cause);
        this.methodName = method.getMethodName();
        this.parameterName = parameterName;
    }

    public HubParameterConversionException(String issue, SignalRHubMethod method, String parameterName) {
        this(issue, method, parameterName, null);
    }
}
