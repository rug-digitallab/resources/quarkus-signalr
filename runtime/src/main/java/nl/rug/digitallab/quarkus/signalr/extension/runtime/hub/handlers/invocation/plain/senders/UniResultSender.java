package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain.senders;

import io.smallrye.mutiny.Uni;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * This adapter will hook into the Uni and send the response to the client once
 * the Uni is completed.
 * <p>
 * Also, it will catch any exceptions that might happen during the execution of
 * the Uni and send the exception message back to the client.
 */
public class UniResultSender extends PlainInvocationResultSender<Uni<?>> {

    @Override
    public void handleResult(SignalRSession session, Invocation packet, Uni<?> methodResult) {
        methodResult.subscribe().with(
                result -> sendCompletion(session, packet, result),
                e -> sendErroredCompletion(session, packet, e.getMessage())
        );
    }
}
