package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.handlers.invocation.plain.senders;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation.Invocation;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.util.concurrent.CompletableFuture;

/**
 * This adapter will hook into the CompletableFuture and send the response to
 * the client once the future is completed.
 * <p>
 * It will also catch any exceptions that might happen during the execution of
 * the future and send the exception message back to the client.
 */
public class CompletableFutureResultSender extends PlainInvocationResultSender<CompletableFuture<?>> {

    @Override
    public void handleResult(SignalRSession session, Invocation packet, CompletableFuture<?> methodResult) {
        methodResult.whenComplete((result, e) -> {
            if (e != null) {
                sendErroredCompletion(session, packet, e.getMessage());
                return;
            }

            sendCompletion(session, packet, result);
        });
    }
}
