package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result;

import lombok.Data;
import lombok.EqualsAndHashCode;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacketType;

/**
 * This is the StreamItem message, it is used to send a single item from a
 * stream.
 * <p>
 * <i>From the SignalR docs:</i>
 * Indicates individual items of streamed response data from a previous
 * StreamInvocation message or streamed uploads from an invocation with
 * streamIds.
 *
 * @see <a href=
 * "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#streamitem-message-encoding">StreamItem
 * message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StreamItem extends SignalRPacket {
    private String invocationId;
    private Object item;

    /**
     * Instantiates a new empty stream item packet.
     */
    public StreamItem() {
        super(SignalRPacketType.STREAM_ITEM);
    }

    /**
     * Instantiates a new stream item packet.
     *
     * @param invocationId - The invocation id (stream id)
     * @param item         - The item
     */
    public StreamItem(String invocationId, Object item) {
        super(SignalRPacketType.STREAM_ITEM);

        this.invocationId = invocationId;
        this.item = item;
    }
}
