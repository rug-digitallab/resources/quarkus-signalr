package nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub;

import lombok.Getter;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.SignalRHubDefinition.SignalRHubMethod;

/**
 * This exception is thrown when a method on a hub instance triggers an exception.
 * <p>
 * This exception is analogous to the {@link java.lang.reflect.InvocationTargetException},
 * but is more specific to the SignalR extension.
 * <p>
 * This method will also trigger in the case of an invocation packet being malformed.
 * <p>
 * An example of this would be a packet which matches the argument count based on the
 * argument count (plain + stream), but the amount of arguments in the packet does not
 * match the amount of arguments in the method. In such cases, the underlying exception
 * will always be an {@link java.lang.IllegalArgumentException}.
 * <p>
 * Finally, this exception can be triggered when the parameter conversion process fails,
 * usually triggered by a non-parametrized type used for a stream or an object which Jackson
 * fails to convert. In such cases the underlying exception will always be a {@link HubParameterConversionException}
 */
@Getter
public class HubMethodInvocationException extends Exception {
    private final String methodName;

    public HubMethodInvocationException(SignalRHubMethod method, Throwable cause) {
        super("Failed to invoke hub method: " + method.getMethodName(), cause);
        this.methodName = method.getMethodName();
    }
}
