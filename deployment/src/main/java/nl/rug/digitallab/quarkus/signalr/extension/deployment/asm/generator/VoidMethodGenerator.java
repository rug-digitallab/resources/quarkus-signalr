package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import com.fasterxml.jackson.databind.JavaType;
import lombok.NonNull;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.ClientMethodDefinition;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

/**
 * This class is used to generate the necessary bytecode to invoke a client
 * method when the return type is void.
 */
public class VoidMethodGenerator extends ClientMethodGenerator {
    /**
     * This will generate the required bytecode for a client method with a void
     * return type. The transformation will look like this
     * 
     * <pre>
     * &#64;ClientMethod
     * public abstract void method(SignalRSession session, String param);
     * </pre>
     * 
     * Will be transformed into this
     * 
     * <pre>
     * &#64;ClientMethod
     * public void method(SignalRSession session, String param) {
     *     session.invoke("method", null, param);
     * }
     * </pre>
     * 
     * For more information check out
     * {@link SignalRSession#invoke(String, JavaType, Object...)}
     * 
     * @param method    - The definition of the method
     * @param mv        - The method visitor to use
     * @param arguments - The arguments of the method (without the session)
     */
    public void generateClientMethodBytecode(ClientMethodDefinition method, MethodVisitor mv, Type[] arguments) {
        // Add an ALOAD with index 1 (since the first param is always a session)
        // and since the session is an object, we use ALOAD for references.
        // This is the object that we will call the invoke method on.
        // After this the stack looks like this: [session]
        mv.visitVarInsn(Opcodes.ALOAD, 1);

        // Add a LDC with the method name, as the first parameter to the invoke method.
        // After this the stack looks like this: [session, "methodName"]
        mv.visitLdcInsn(method.getName());

        // Add an ACONST_NULL, since the return type is void, we don't need to do
        // anything with it. This is to spare us from having to do any work with the
        // return value.
        // This replaces the usual NestedTypeReferenceBuilder.buildFromNames call.
        mv.visitInsn(Opcodes.ACONST_NULL);

        // Add the arguments to the stack
        visitArgumentArray(mv, arguments);
        // After this the stack looks like this: [session, "methodName", null,
        // arguments...]

        // Once we have all the arguments on the stack, we can call the invoke method
        // It should also be noted that void methods cannot be streamed (since we don't
        // care about the result anyway), so we don't need to worry about that.
        visitMethodInvocation(mv, SignalRSession.class, "invoke", false, String.class, JavaType.class, Object[].class);

        // Since the return type is void, we don't need to do anything with the result
        // As such, since the result of invoke is still on the stack after the
        // invocation
        // we can simply pop it off.
        mv.visitInsn(Opcodes.POP);

        mv.visitLabel(new Label());

        // Append the return instruction, (notice how it's a RETURN and not an ARETURN)
        mv.visitInsn(Opcodes.RETURN);

        // Since COMPUTE_MAXS is enabled, 0,0 is fine
        mv.visitMaxs(0 ,0);
        mv.visitEnd();
    }

    /**
     * This will check if the given type is void.
     * 
     * @param type - The type to check
     * @return - True if the type is void, false otherwise
     */
    @Override
    public boolean canHandle(@NonNull Type type) {
        return type.equals(Type.VOID_TYPE);
    }

}
