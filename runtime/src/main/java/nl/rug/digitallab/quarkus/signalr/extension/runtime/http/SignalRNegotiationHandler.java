package nl.rug.digitallab.quarkus.signalr.extension.runtime.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import lombok.NonNull;
import lombok.extern.jbosslog.JBossLog;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.generic.SignalRErrorMessage;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.negotiation.NegotiationResponse;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.negotiation.SupportedTransport;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;

import java.util.List;

/**
 * This class is responsible for handling the SignalR negotiation request.
 */
@JBossLog
public class SignalRNegotiationHandler extends SignalRHandler implements Handler<RoutingContext> {
    /** The minimum supported version */
    private static final int MAX_NEGOTIATION_VERSION = 1;
    /** The maximum supported version */
    private static final int MIN_SUPPORTED_VERSION = 1;

    /**
     * Create a new instance of the negotiation handler.
     * 
     * @param path - The path to handle
     */
    public SignalRNegotiationHandler(@NonNull String path) {
        super(path);
    }

    @Override
    public void handle(RoutingContext event) {
        if (!event.request().method().toString().equalsIgnoreCase("POST")) {
            event.next();
            return ;
        }

        // Should no negotiation version be provided, default to 0
        int requestedNegotiationVersion = Integer.parseInt(event.request().getParam("negotiateVersion", "0"));

        // As per spec:
        // https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/TransportProtocols.md
        if (
            requestedNegotiationVersion < MIN_SUPPORTED_VERSION 
            || requestedNegotiationVersion > MAX_NEGOTIATION_VERSION
        ) {
            try {
                String errorMessage = "Negotiation version not supported. Expected version between "
                        + MIN_SUPPORTED_VERSION + " and " + MAX_NEGOTIATION_VERSION;

                event.response().setStatusCode(400)
                        .end(getObjectMapper().writeValueAsString(new SignalRErrorMessage(errorMessage)));
            } catch (JsonProcessingException e) {
                log.error("Failed to serialize negotiation response", e);
                event.response().setStatusCode(500).end();
            }

            return;
        }

        // Create a new session
        SignalRSession session = this.getConnector().newSession(requestedNegotiationVersion);

        List<SupportedTransport> supportedTransports = this.getConnector().getHubDefinition()
                .getTransports().stream()
                .map(TransportType::getSupportedTransport)
                .toList();

        // Handle the request
        NegotiationResponse response = new NegotiationResponse(session, supportedTransports);
        try {
            event.response().end(getObjectMapper().writeValueAsString(response));
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize negotiation response", e);
            event.response().setStatusCode(500).end();
        }
    }
}
