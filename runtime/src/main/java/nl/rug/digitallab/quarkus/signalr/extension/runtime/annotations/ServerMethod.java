package nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.lang.annotation.*;

/**
 * Annotation to mark a method as a server method.
 * <p>
 * Any method which is annotated with this annotation will be exposed to the
 * client, and can be invoked by the client.
 * <p>
 * Methods annotated with this annotation can only be present in classes
 * annotated with {@link SignalRHub}, and must have the first argument as a
 * {@link SignalRSession}.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServerMethod {

}
