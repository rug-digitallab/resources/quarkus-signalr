package nl.rug.digitallab.quarkus.signalr.extension.runtime.session;

import io.smallrye.mutiny.Multi;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.stream.SignalRIncomingStream;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.Completion;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.result.StreamItem;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RequiredArgsConstructor
public class SessionStreamManager {
    /**
     * The parent session of this stream manager.
     */
    @NonNull
    private SignalRSession parentSession;

    /**
     * The list of incoming streams for this session. These streams are used in two
     * scenarios:
     * <p>
     * 1. When the server sends a streamable invocation, the server will create a
     * new stream and receive items from the client.
     * <p>
     * 2. When the client invokes a method which has streamables as parameters, the
     * server will use these streams to send items to the client.
     */
    @Getter(AccessLevel.NONE)
    private Map<String, SignalRIncomingStream<?>> incomingStreams = new ConcurrentHashMap<>();

    /**
     * The list of outgoing streams for this session.
     * <p>
     * These streams are used when the server invokes a method which has streamables
     * as parameters. The server will send items to these streams, and the client
     * will receive them.
     * <p>
     * The key is the stream ID, and the value is the Multi which will emit the
     * items.
     * <p>
     * Unlike the incoming streams, we use a Multi here, because we have no control
     * over the emitting, rather we simply subscribe to the Multi and send the items
     * as they come.
     */
    private Map<String, Multi<?>> outgoingStreams = new ConcurrentHashMap<>();


    /**
     * This will send a stream to the client with the given ID.
     *
     * @param streamId - The ID of the stream
     * @param multi    - The stream to send
     */
    protected void sendOutStream(String streamId, Multi<?> multi) {
        this.outgoingStreams.put(streamId, multi);

        multi.subscribe().with(item -> {
            StreamItem streamItem = new StreamItem(streamId, item);
            parentSession.sendMessage(streamItem);
        }, e -> {
            Completion completion = new Completion(streamId, null, e.getMessage());
            parentSession.sendMessage(completion);
            this.outgoingStreams.remove(streamId);
        }, () -> {
            Completion completion = new Completion(streamId, null, null);
            parentSession.sendMessage(completion);
            this.outgoingStreams.remove(streamId);
        });
    }

    /**
     * This will create a new incoming stream with the given ID.
     *
     * @param streamId - The ID of the stream
     * @param stream   - The stream to add
     */
    public void newIncomingStream(String streamId, SignalRIncomingStream<?> stream) {
        incomingStreams.put(streamId, stream);
    }

    /**
     * This will remove an incoming stream with the given ID.
     *
     * @param streamId - The ID of the stream
     * @return - The stream that was removed
     */
    public SignalRIncomingStream<?> removeIncomingStream(String streamId) {
        return incomingStreams.remove(streamId);
    }

    /**
     * This will get a stream with the given ID.
     *
     * @param streamId - The ID of the stream
     * @return - The stream with the given ID
     */
    public SignalRIncomingStream<?> getIncomingStream(String streamId) {
        return incomingStreams.get(streamId);
    }

    /**
     * This will check if the session has an incoming stream with the given ID.
     *
     * @param streamId - The ID of the stream
     * @return - True if the session has an incoming stream with the given ID, false otherwise
     */
    public boolean hasIncomingStream(String streamId) {
        return incomingStreams.containsKey(streamId);
    }
}
