package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import io.smallrye.mutiny.Multi;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.util.JavaTypeDataFaker.CompositeTestObject;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Type;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class MultiGeneratorTests extends GenericGeneratorTest<MultiMethodGenerator> {

    @Override
    public Type getHandledType() {
        return Type.getType(Multi.class);
    }

    @Override
    public MultiMethodGenerator createClientMethodGenerator() {
        return new MultiMethodGenerator();
    }

    @Override
    @SuppressWarnings("java:S1172") // Unused parameters for SonarLint
    public Object getMethodContainer() {
        return new Object() {
            // @formatter:off
            Multi<Integer> primitiveWrapperNoParamMethod(SignalRSession session) { return null; }
            Multi<int[]> primitiveArrayNoParamMethod(SignalRSession session) { return null; }
            Multi<Integer[]> primitiveWrapperArrayNoParamMethod(SignalRSession session) { return null; }
            Multi<String> stringNoParamMethod(SignalRSession session) { return null; }
            Multi<String[]> stringArrayNoParamMethod(SignalRSession session) { return null; }
            Multi<List<String>> listNoParamMethod(SignalRSession session) { return null; }
            Multi<Map<String, String>> setNoParamMethod(SignalRSession session) { return null; }
            Multi<Map<String, String>> mapNoParamMethod(SignalRSession session) { return null; }
            Multi<CompositeTestObject> compositeNoParamMethod(SignalRSession session) { return null; }
            Multi<CompositeTestObject[]> compositeArrayNoParamMethod(SignalRSession session) { return null; }

            Multi<Integer> primitiveWrapperMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<int[]> primitiveArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<String> stringMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<String[]> stringArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<List<String>> listMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<Map<String, String>> setMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<Map<String, String>> mapMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<CompositeTestObject> compositeMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }

            Multi<Integer> primitiveWrapperMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<int[]> primitiveArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<String> stringMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<String[]> stringArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<List<String>> listMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<Map<String, String>> setMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<Map<String, String>> mapMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<CompositeTestObject> compositeMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }

            Multi<Integer> primitiveWrapperMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<int[]> primitiveArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<String> stringMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<String[]> stringArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<List<String>> listMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<Map<String, String>> setMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<Map<String, String>> mapMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<CompositeTestObject> compositeMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }

            Multi<Integer> primitiveWrapperMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<int[]> primitiveArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<String> stringMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<String[]> stringArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<List<String>> listMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<Map<String, String>> setMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<Map<String, String>> mapMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<CompositeTestObject> compositeMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }

            Multi<Integer> primitiveWrapperMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<int[]> primitiveArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<String> stringMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<String[]> stringArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<List<String>> listMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<Map<String, String>> setMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<Map<String, String>> mapMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<CompositeTestObject> compositeMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }

            Multi<Integer> primitiveWrapperMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<int[]> primitiveArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<String> stringMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<String[]> stringArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<List<String>> listMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<Map<String, String>> setMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<Map<String, String>> mapMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<CompositeTestObject> compositeMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }

            Multi<Integer> primitiveWrapperMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<int[]> primitiveArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<String> stringMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<String[]> stringArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<List<String>> listMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<Map<String, String>> setMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<Map<String, String>> mapMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<CompositeTestObject> compositeMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }

            Multi<Integer> primitiveWrapperMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<int[]> primitiveArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<String> stringMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<String[]> stringArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<List<String>> listMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<Map<String, String>> setMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<Map<String, String>> mapMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<CompositeTestObject> compositeMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }

            Multi<Integer> primitiveWrapperMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<int[]> primitiveArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<String> stringMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<String[]> stringArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<List<String>> listMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<Map<String, String>> setMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<Map<String, String>> mapMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<CompositeTestObject> compositeMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }

            Multi<Integer> primitiveWrapperMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<int[]> primitiveArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<String> stringMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<String[]> stringArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<List<String>> listMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<Map<String, String>> setMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<Map<String, String>> mapMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<CompositeTestObject> compositeMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }

            Multi<Integer> primitiveWrapperMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<int[]> primitiveArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<Integer[]> primitiveWrapperArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<String> stringMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<String[]> stringArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<List<String>> listMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<Map<String, String>> setMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<Map<String, String>> mapMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<CompositeTestObject> compositeMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Multi<CompositeTestObject[]> compositeArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            // @formatter:on
        };
    }

    @Override
    public Object unwrapMethodInvocationResult(Object result) {
        Multi<?> future = (Multi<?>) result;
        return future.collect().asList().await().indefinitely().getFirst();
    }

    @Override
    public boolean expectsStreamable() {
        return true;
    }
}
