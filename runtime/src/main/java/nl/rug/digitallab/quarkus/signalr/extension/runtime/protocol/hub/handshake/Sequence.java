package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.handshake;

import lombok.Data;
import lombok.EqualsAndHashCode;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacketType;

/**
 * Sequence packet is used to acknowledge the last received packet in the case
 * of a reconnection.
 * 
 * @see <a href=
 *      "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#sequence-message-encoding">
 *      Sequence message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Sequence extends SignalRPacket {
    /** The sequence number of the last received packet. */
    private int sequenceNumber;

    /**
     * Instantiates a new sequence packet.
     */
    public Sequence() {
        super(SignalRPacketType.SEQUENCE);
    }

    /**
     * Instantiates a new sequence packet.
     * 
     * @param sequenceNumber - The sequence number of the last received packet.
     */
    public Sequence(int sequenceNumber) {
        super(SignalRPacketType.SEQUENCE);
        this.sequenceNumber = sequenceNumber;
    }
}
