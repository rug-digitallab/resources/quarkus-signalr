# SignalR Kotlin

This is the repository which contains the Quarkus extension for the SignalR implementation

# Overview of structure

```mermaid
graph TD
    A[SignalR] --> |Establishes| B[SignalRSession]
    A --> |Uses| C[SignalRConnector]
    A --> |Defines Methods| H[SignalRHub]
    H --> |Backs Methods| C

    subgraph Handlers
        D[SignalRSSEHandler]
        E[SignalRPostHandler]
        F[SignalRNegotiationHandler]
        G[SignalRLongPollingHandler]
    end

    C --> |Communicates using| Handlers
    I[SignalRRecorder] --> |Sets up with Vert.x hook| Handlers
```

# Overview of protocol

```mermaid
sequenceDiagram
    participant Client
    participant Server
    Client->>Server: Handshake Request
    Server-->>Client: Handshake Response
    Client->>Server: Negotiation Request
    Server-->>Client: Negotiation Response
    loop Periodic Pings
        Client->>Server: Ping
        Server-->>Client: Ping
        Client->Server: Invocations & Replies
    end
```

# Usage

## Hubs
Classes annotated with `@SignalRHub("/path/to/hub")` are automatically registered as SignalR hubs.
The path is the URL path that the hub will be available at.

```java
@SignalRHub("/hubs/example")
public class ExampleSimpleHub() {
    
}
```

## Server Methods
Within a hub, methods annotated with `@ServerMethod` are registered as server methods,
which can be invoked by clients. Since all methods are executed on a given SignalRSession, **the first argument must be the session**.

```java   
@SignalRHub("/hubs/example")
public class ExampleSimpleHub() {
    @ServerMethod
    public String simpleServerMethod(SignalRSession session) {
        return "Hello, World";
    }
}
```

## Client Methods
To ensure type safety, you can define abstract methods
within the hub which are annotated with `@ClientMethod`:
```java
@SignalRHub("/hubs/example")
public abstract class ExampleSimpleHub() {
    @ClientMethod
    public abstract void simpleClientMethod(SignalRSession session);
}
```

This method can then be invoked by the server (with client methods also annotated with SignalRSession):

```java
@SignalRHub("/hubs/example")
public abstract class ExampleSimpleHub() {
    
    @ServerMethod
    public void invokeClientMethod(SignalRSession session) {
        simpleClientMethod(session);
    }

    @ClientMethod
    public abstract void simpleClientMethod(SignalRSession session);
}
```

## Examples

### Single Result

#### Example 1
Simple method which returns a greeting based on the name provided
```java
// Server
@ServerMethod
public String singleResult(SignalRSession session, String name) {
    return "Hello " + name;
}
```

```
// Client Output
[C->S]  Sending argument: User
[S->C]  Hello User
```

#### Example 2
Single result method that has an error
```java
// Server
@ServerMethod
public Integer singleResultError(SignalRSession session, String name) throws Exception {
    throw new Exception(name + ", this is an error");
}
```

```
// Client Output
[C->S]  Sending argument: User
[S->C]  Error: User, this is an error // error caught by the client
```

### Streaming
The extension also supports streamable arguments and methods

#### Example 1
Streamable arguments with a single result:
```java
// Server
@ServerMethod
public Uni<Integer> streamingSum(SignalRSession session, Multi<Integer> numbers) {
    return numbers.collect()
        .in(AtomicInteger::new, AtomicInteger::addAndGet)
        .map(AtomicInteger::get);
}
```

```
// Client Output
// One sec delay between sending each item
[C->S] Sending Argument: 1
[C->S] Sending Argument: 2
[C->S] Sending Argument: 3
[C->S] Sending Argument: 4
[C->S] Sending Argument: 5
[C->S] Stream completed
[S->C]  15
Stream completed
```

#### Example 2
Streamable method and arguments with intermediate results:
```java
// Server
@ServerMethod
public Multi<Integer> streamingSumWithIntermediate(SignalRSession session, Multi<Integer> numbers) {
    AtomicInteger runningTotal = new AtomicInteger(0);
    return numbers
        .onItem().transform(running:addAndGet);
}
```

```
// Client Output
// One sec delay between sending each item
[C->S] Sending Argument: 1
[S->C] 1
[C->S] Sending Argument: 2
[S->C] 3
[C->S] Sending Argument: 3
[S->C] 6
[C->S] Sending Argument: 4
[S->C] 10
[C->S] Sending Argument: 5
[S->C] 15
[C->S] Stream completed
Stream completed
```

