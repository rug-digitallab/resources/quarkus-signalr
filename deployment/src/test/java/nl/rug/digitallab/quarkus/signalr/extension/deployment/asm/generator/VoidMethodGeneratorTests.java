package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import nl.rug.digitallab.quarkus.signalr.extension.deployment.util.JavaTypeDataFaker.CompositeTestObject;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Type;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNull;

public class VoidMethodGeneratorTests extends GenericGeneratorTest<VoidMethodGenerator> {

    @Override
    public Type getHandledType() {
        return Type.VOID_TYPE;
    }

    @Override
    public VoidMethodGenerator createClientMethodGenerator() {
        return new VoidMethodGenerator();
    }

    @Override
    @SuppressWarnings({"java:S1172", "java:S1186"}) // Unused parameters & empty bodies for SonarLint
    public Object getMethodContainer() {
        return new Object() {
            // @formatter:off
            void voidNoParamMethod(SignalRSession session) { }
            void voidMethodPrimitiveParam(SignalRSession session, int primitiveParam) { }
            void voidMethodPrimitiveWrappedParam(SignalRSession session, Integer primitiveWrappedParam) { }
            void voidMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { }
            void voidMethodPrimitiveArrayWrappedParam(SignalRSession session, Integer[] primitiveArrayWrappedParam) { }
            void voidMethodStringParam(SignalRSession session, String stringParam) { }
            void voidMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { }
            void voidMethodListParam(SignalRSession session, List<String> listParam) { }
            void voidMethodSetParam(SignalRSession session, Set<String> setParam) { }
            void voidMethodMapParam(SignalRSession session, Map<String, String> mapParam) { }
            void voidMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { }
            void voidMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { }
            // @formatter:on
        };
    }

    @Override
    public Object unwrapMethodInvocationResult(Object result) {
        assertNull(result);
        return null;
    }

    @Override
    public boolean expectsStreamable() {
        return false;
    }
}
