package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.generator;

import io.smallrye.mutiny.Uni;
import nl.rug.digitallab.quarkus.signalr.extension.deployment.util.JavaTypeDataFaker.CompositeTestObject;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;
import org.objectweb.asm.Type;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class UniGeneratorTests extends GenericGeneratorTest<UniMethodGenerator> {

    @Override
    public Type getHandledType() {
        return Type.getType(Uni.class);
    }

    @Override
    public UniMethodGenerator createClientMethodGenerator() {
        return new UniMethodGenerator();
    }

    @Override
    @SuppressWarnings("java:S1172") // Unused parameters for SonarLint
    public Object getMethodContainer() {
        return new Object() {
            // @formatter:off
            Uni<Integer> primitiveWrapperNoParamMethod(SignalRSession session) { return null; }
            Uni<int[]> primitiveArrayNoParamMethod(SignalRSession session) { return null; }
            Uni<Integer[]> primitiveWrapperArrayNoParamMethod(SignalRSession session) { return null; }
            Uni<String> stringNoParamMethod(SignalRSession session) { return null; }
            Uni<String[]> stringArrayNoParamMethod(SignalRSession session) { return null; }
            Uni<List<String>> listNoParamMethod(SignalRSession session) { return null; }
            Uni<Map<String, String>> setNoParamMethod(SignalRSession session) { return null; }
            Uni<Map<String, String>> mapNoParamMethod(SignalRSession session) { return null; }
            Uni<CompositeTestObject> compositeNoParamMethod(SignalRSession session) { return null; }
            Uni<CompositeTestObject[]> compositeArrayNoParamMethod(SignalRSession session) { return null; }

            Uni<Integer> primitiveWrapperMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<int[]> primitiveArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<String> stringMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<String[]> stringArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<List<String>> listMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<Map<String, String>> setMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<Map<String, String>> mapMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<CompositeTestObject> compositeMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodPrimitiveParam(SignalRSession session, int primitiveParam) { return null; }

            Uni<Integer> primitiveWrapperMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<int[]> primitiveArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<String> stringMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<String[]> stringArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<List<String>> listMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<Map<String, String>> setMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<Map<String, String>> mapMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<CompositeTestObject> compositeMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodPrimitiveWrapperParam(SignalRSession session, Integer primitiveWrapperParam) { return null; }

            Uni<Integer> primitiveWrapperMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<int[]> primitiveArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<String> stringMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<String[]> stringArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<List<String>> listMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<Map<String, String>> setMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<Map<String, String>> mapMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<CompositeTestObject> compositeMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodPrimitiveArrayParam(SignalRSession session, int[] primitiveArrayParam) { return null; }

            Uni<Integer> primitiveWrapperMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<int[]> primitiveArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<String> stringMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<String[]> stringArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<List<String>> listMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<Map<String, String>> setMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<Map<String, String>> mapMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<CompositeTestObject> compositeMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodPrimitiveWrapperArrayParam(SignalRSession session, Integer[] primitiveWrapperArrayParam) { return null; }

            Uni<Integer> primitiveWrapperMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<int[]> primitiveArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<String> stringMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<String[]> stringArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<List<String>> listMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<Map<String, String>> setMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<Map<String, String>> mapMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<CompositeTestObject> compositeMethodStringParam(SignalRSession session, String stringParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodStringParam(SignalRSession session, String stringParam) { return null; }

            Uni<Integer> primitiveWrapperMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<int[]> primitiveArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<String> stringMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<String[]> stringArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<List<String>> listMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<Map<String, String>> setMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<Map<String, String>> mapMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<CompositeTestObject> compositeMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodStringArrayParam(SignalRSession session, String[] stringArrayParam) { return null; }

            Uni<Integer> primitiveWrapperMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<int[]> primitiveArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<String> stringMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<String[]> stringArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<List<String>> listMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<Map<String, String>> setMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<Map<String, String>> mapMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<CompositeTestObject> compositeMethodListParam(SignalRSession session, List<String> listParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodListParam(SignalRSession session, List<String> listParam) { return null; }

            Uni<Integer> primitiveWrapperMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<int[]> primitiveArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<String> stringMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<String[]> stringArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<List<String>> listMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<Map<String, String>> setMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<Map<String, String>> mapMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<CompositeTestObject> compositeMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodSetParam(SignalRSession session, Set<String> setParam) { return null; }

            Uni<Integer> primitiveWrapperMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<int[]> primitiveArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<String> stringMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<String[]> stringArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<List<String>> listMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<Map<String, String>> setMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<Map<String, String>> mapMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<CompositeTestObject> compositeMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodMapParam(SignalRSession session, Map<String, String> mapParam) { return null; }

            Uni<Integer> primitiveWrapperMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<int[]> primitiveArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<String> stringMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<String[]> stringArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<List<String>> listMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<Map<String, String>> setMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<Map<String, String>> mapMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<CompositeTestObject> compositeMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodCompositeParam(SignalRSession session, CompositeTestObject compositeParam) { return null; }

            Uni<Integer> primitiveWrapperMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<int[]> primitiveArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<Integer[]> primitiveWrapperArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<String> stringMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<String[]> stringArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<List<String>> listMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<Map<String, String>> setMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<Map<String, String>> mapMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<CompositeTestObject> compositeMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            Uni<CompositeTestObject[]> compositeArrayMethodCompositeArrayParam(SignalRSession session, CompositeTestObject[] compositeArrayParam) { return null; }
            // @formatter:on
        };
    }

    @Override
    public Object unwrapMethodInvocationResult(Object result) {
        Uni<?> future = (Uni<?>) result;
        return future.await().indefinitely();
    }

    @Override
    public boolean expectsStreamable() {
        return false;
    }
}
