package nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.invocation;

import lombok.Data;
import lombok.EqualsAndHashCode;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacketType;

/**
 * This is the CancelInvocation message, it is used to cancel an invocation.
 * 
 * @see <a href=
 *      "https://github.com/dotnet/aspnetcore/blob/main/src/SignalR/docs/specs/HubProtocol.md#cancelinvocation-message-encoding">
 *      CancelInvocation message encoding</a>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CancelInvocation extends SignalRPacket {
    /** The invocation ID of the invocation to cancel. */
    private String invocationId;

    /**
     * Instantiates a new cancel invocation packet.
     */
    public CancelInvocation() {
        super(SignalRPacketType.CANCEL_INVOCATION);
    }

    /**
     * Instantiates a new cancel invocation packet.
     * 
     * @param invocationId - The invocation ID of the invocation to cancel.
     */
    public CancelInvocation(String invocationId) {
        super(SignalRPacketType.CANCEL_INVOCATION);
        this.invocationId = invocationId;
    }
}
