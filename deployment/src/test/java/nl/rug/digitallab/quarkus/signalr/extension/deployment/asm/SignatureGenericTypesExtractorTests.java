package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm;

import lombok.SneakyThrows;
import org.jboss.jandex.Index;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.objectweb.asm.Type;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.*;

class SignatureGenericTypesExtractorTests {

    private Map<String, SignatureGenericTypesExtractor> extractors = new HashMap<>();

    @BeforeEach
    @SneakyThrows(IOException.class) // For Index.of
    void setup() {
        Index index = Index.of(TestClass.class);

        index.getClassByName(TestClass.class).methods().forEach(method ->
                extractors.put(method.name(), new SignatureGenericTypesExtractor(method.genericSignature())));
    }

    @AfterEach
    void tearDown() {
        extractors.clear();
    }

    @Test
    @DisplayName("Ensure that primitive type return types are correctly handled")
    void testPrimitiveSignatures() {
        Set<String> params = Set.of("byte", "char", "double", "float", "int", "long", "short", "boolean");

        for (String param : params) {
            List<String> extracted = extractors.get(param + "Method").getArguments();

            assertEquals(1, extracted.size());
            assertEquals(param, extracted.getFirst());
        }
    }

    @Test
    @DisplayName("Ensure that simple generic types are correctly handled")
    void testSimpleGenericSignatures() {
        SignatureGenericTypesExtractor extractor = extractors.get("simpleGenericMethod");
        List<String> extracted = extractor.getArguments();

        assertEquals(2, extracted.size());
        assertThat(extracted, contains(
                List.class.getName(),
                String.class.getName()
        ));

        extracted = extractor.getInnerArguments();
        assertEquals(1, extracted.size());
        assertEquals(String.class.getName(), extracted.getFirst());
    }

    @Test
    @DisplayName("Ensure that nested generic types are correctly handled")
    void testNestedGenericSignatures() {
        List<String> extracted = extractors.get("nestedGenericMethod").getArguments();

        assertEquals(3, extracted.size());
        assertThat(extracted, contains(
                List.class.getName(),
                List.class.getName(),
                String.class.getName()
        ));

        extracted = extractors.get("nestedGenericMethod").getInnerArguments();
        assertEquals(2, extracted.size());
        assertThat(extracted, contains(
                List.class.getName(),
                String.class.getName()
        ));

        extracted = extractors.get("nestedGenericMethod2").getArguments();
        assertEquals(4, extracted.size());
        assertThat(extracted, contains(
                Map.class.getName(),
                String.class.getName(),
                List.class.getName(),
                String.class.getName()
        ));

        extracted = extractors.get("nestedGenericMethod3").getArguments();
        assertEquals(8, extracted.size());

        assertThat(extracted, contains(
                Map.class.getName(),
                List.class.getName(),
                String.class.getName(),
                Map.class.getName(),
                Set.class.getName(),
                String.class.getName(),
                List.class.getName(),
                String.class.getName()
        ));
    }

    @Test
    @DisplayName("Ensure that non-generic types throw an exception when trying to get inner arguments")
    void testNonGenericSignatures() {
        SignatureGenericTypesExtractor extractor = extractors.get("intMethod");

        List<String> extracted = extractor.getArguments();
        assertEquals(1, extracted.size());
        assertEquals(int.class.getName(), extracted.getFirst());

        assertThrowsExactly(UnsupportedOperationException.class, extractor::getInnerArguments,
                "The type reference is not a generic type!");
    }

    @Test
    @DisplayName("Ensure that a simple array types is correctly handled")
    void testArraySignatures() {
        List<String> extracted = extractors.get("arrayMethod").getArguments();

        assertEquals(1, extracted.size());
        assertThat(extracted, contains(
                Type.getType(String[].class).getClassName()
        ));
    }

    @Test
    @DisplayName("Ensure that a multi-dimensional array types is correctly handled")
    void testMultiArraySignatures() {
        List<String> extracted = extractors.get("multiArrayMethod").getArguments();

        assertEquals(1, extracted.size());
        assertThat(extracted, contains(
                Type.getType(String[][][].class).getClassName()
        ));
    }

    @Test
    @DisplayName("Ensure that an array in a generic type is correctly handled")
    void testArrayInGenericSignatures() {
        List<String> extracted = extractors.get("arrayInGenericMethod").getArguments();

        assertEquals(2, extracted.size());
        assertThat(extracted, contains(
                List.class.getName(),
                Type.getType(String[][].class).getClassName()
        ));

        extracted = extractors.get("arrayInGenericMethod").getInnerArguments();
        assertEquals(1, extracted.size());
        assertThat(extracted, contains(
                Type.getType(String[][].class).getClassName()
        ));
    }

    @Test
    @DisplayName("Ensure that a generic array type is correctly handled")
    void testGenericArraySignatures() {
        List<String> extracted = extractors.get("genericArrayMethod").getArguments();

        assertEquals(2, extracted.size());
        assertThat(extracted, contains(
                Type.getType(List[].class).getClassName(),
                String.class.getName()
        ));

        extracted = extractors.get("genericArrayMethod").getInnerArguments();
        assertEquals(1, extracted.size());
        assertThat(extracted, contains(
                String.class.getName()
        ));
    }

    @Test
    @DisplayName("Ensure that a mixed array type is correctly handled")
    void testMixedArraySignatures() {
        List<String> extracted = extractors.get("mixedArrayMethod").getArguments();

        assertEquals(2, extracted.size());
        assertThat(extracted, contains(
                Type.getType(List[][].class).getClassName(),
                Type.getType(String[][][].class).getClassName()
        ));

        extracted = extractors.get("mixedArrayMethod").getInnerArguments();
        assertEquals(1, extracted.size());
        assertThat(extracted, contains(
                Type.getType(String[][][].class).getClassName()
        ));
    }

    @Test
    @DisplayName("Ensure that a multi-dimensional mixed array type is correctly handled")
    void testMultiMixedArraySignatures() {
        List<String> extracted = extractors.get("multiMixedArrayMethod").getArguments();

        assertEquals(3, extracted.size());
        assertThat(extracted, contains(
                Type.getType(List[][][].class).getClassName(),
                Type.getType(List[][].class).getClassName(),
                Type.getType(String[].class).getClassName()
        ));

        extracted = extractors.get("multiMixedArrayMethod").getInnerArguments();
        assertEquals(2, extracted.size());
        assertThat(extracted, contains(
                Type.getType(List[][].class).getClassName(),
                Type.getType(String[].class).getClassName()
        ));
    }

    @Test
    @DisplayName("Ensure that a multi-parameter generic, multi-dimensional array types is correctly handled")
    void testMultiGenericMultiArraySignatures() {
        List<String> extracted = extractors.get("multiGenericMultiArrayMethod").getArguments();

        assertEquals(8, extracted.size());
        assertThat(extracted, contains(
                Type.getType(Map[][][][][][].class).getClassName(),
                Type.getType(Set[][][].class).getClassName(),
                Type.getType(String[][].class).getClassName(),
                Type.getType(List.class).getClassName(),
                Type.getType(Map[][][][][].class).getClassName(),
                Type.getType(Set.class).getClassName(),
                Type.getType(String[].class).getClassName(),
                Type.getType(String[][][][].class).getClassName()
        ));
    }

    @Test
    @DisplayName("Ensure all primitives get correctly identifies in a return type")
    void testValidPrimitiveTypes() {
        String allowedCharacters = "BCDFIJSZ";

        List<Class<?>> primitives = List.of(
                byte.class,
                char.class,
                double.class,
                float.class,
                int.class,
                long.class,
                short.class,
                boolean.class
        );

        for (int i = 0; i < allowedCharacters.length(); i++) {
            char t = allowedCharacters.charAt(i);

            // Equivalent to a method which has no arguments and returns t
            String methodSignature = "()" + t;
            SignatureGenericTypesExtractor extractor = new SignatureGenericTypesExtractor(methodSignature);

            String type = extractor.getArguments().getFirst();
            assertEquals(primitives.get(i).getName(), type);
        }
    }

    @Test
    @DisplayName("Ensure any character which does not represent a primitive throws an exception")
    void testInvalidPrimitiveType() {
        String allowedCharacters = "BCDFIJSZ";

        for (char a = Character.MIN_VALUE; a < Character.MAX_VALUE; a++) {
            if (allowedCharacters.indexOf(a) != -1) {
                continue;
            }

            if (a == 'L' || a == '[' || a == 'T') {
                continue; // These are special cases (object type, array type, type variable)
            }

            String methodSignature = "()" + a;
            SignatureGenericTypesExtractor extractor = new SignatureGenericTypesExtractor(methodSignature);

            assertThrows(IllegalArgumentException.class, extractor::getArguments);
        }
    }

    // These have no reason to be abstract other than concise syntax (and does not affect the test)
    abstract static class TestClass {
        // @formatter:off
        public abstract byte byteMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract char charMethod(int someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract double doubleMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract float floatMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract int intMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract long longMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract short shortMethod(boolean someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract boolean booleanMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract List<String> simpleGenericMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract List<List<String>> nestedGenericMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract Map<String, List<String>> nestedGenericMethod2(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract Map<List<String>, Map<Set<String>, List<String>>> nestedGenericMethod3(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract String[] arrayMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract String[][][] multiArrayMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract List<String[][]> arrayInGenericMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract List<String>[] genericArrayMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract List<String[][][]>[][] mixedArrayMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract List<List<String[]>[][]>[][][] multiMixedArrayMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        public abstract Map<Set<String[][]>[][][], List<Map<Set<String[]>, String[][][][]>[][][][][]>>[][][][][][] multiGenericMultiArrayMethod(String someParam, List<String> anotherParam, Map<Set<String>, Integer> thirdParam);
        // @formatter:on
    }
}
