package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub;

import nl.rug.digitallab.quarkus.signalr.extension.runtime.protocol.hub.SignalRPacket;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * This interface is used to listen for incoming packets from the SignalR server.
 * 
 * @param <T> - The type of packet to listen for.
 */
public interface SignalRPacketListener<T extends SignalRPacket> {

    /**
     * This method is called when a packet is received from the SignalR server.
     * 
     * @param session - The session that received the packet.
     * @param packet - The packet that was received.
     */
    void onPacketReceived(SignalRSession session, T packet);
}
