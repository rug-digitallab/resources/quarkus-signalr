plugins {
    id("nl.rug.digitallab.gradle.plugin.java.project")
    id("maven-publish")
}

dependencies {
    val quarkusVersion: String by project
    val lombokVersion: String by project

    val hamcrestVersion: String by project
    val approvalTestsVersion: String by project
    val jsonAssertVersion: String by project

    // Required for running tests
    testImplementation("io.quarkus:quarkus-junit5-internal")
    testImplementation("org.hamcrest:hamcrest:${hamcrestVersion}")
    testImplementation("com.approvaltests:approvaltests:${approvalTestsVersion}")
    testImplementation("org.skyscreamer:jsonassert:${jsonAssertVersion}")

    // Lombok
    compileOnly("org.projectlombok:lombok:${lombokVersion}")
    annotationProcessor("org.projectlombok:lombok:${lombokVersion}")
    testCompileOnly("org.projectlombok:lombok:${lombokVersion}")
    testAnnotationProcessor("org.projectlombok:lombok:${lombokVersion}")

    // Everything related to quarkus
    implementation(project(":runtime"))
    implementation(platform("io.quarkus:quarkus-bom:${quarkusVersion}"))
    implementation("io.quarkus:quarkus-arc-deployment")
    implementation("io.quarkus:quarkus-vertx-http-deployment")
    implementation("io.quarkus:quarkus-jackson-deployment")
    implementation("io.quarkus:quarkus-quartz-deployment")
}

configure<PublishingExtension> {
    publications {
        create<MavenPublication>("signalrDeployment") {
            from(components["java"])
            artifactId = "signalr-deployment"
            groupId = project.property("projectGroupId") as String
        }
    }

    repositories {
        maven {
            url = project.uri(project.findProperty("mavenRepositoryUrl").toString())
            name = project.findProperty("mavenRepositoryName").toString()

            credentials(HttpHeaderCredentials::class.java) {
                name = project.findProperty("mavenRepositoryHeaderName").toString()
                value = project.findProperty("mavenRepositoryHeaderValue").toString()
            }

            authentication.apply {
                create("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()

    // Needed for some of the stress tests done with deeply nested types
    jvmArgs = listOf("-Xss16m")
}