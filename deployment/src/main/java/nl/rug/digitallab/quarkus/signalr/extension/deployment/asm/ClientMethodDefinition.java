package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jboss.jandex.AnnotationInstance;

import java.util.List;

/**
 * This class is used to represent a client method definition,
 * so we can store the method's information and re-create it later.
 */
@Data
@AllArgsConstructor
public class ClientMethodDefinition {
    /** The access flags for the method */
    private int access;

    /** The name of the method */
    private String name;

    /** The method's descriptor */
    private String descriptor;

    /** The method's signature */
    private String signature;

    /** The exceptions thrown by the method */
    private String[] exceptions;

    /**
     * The annotations present on the method.
     *
     * @see org.jboss.jandex.MethodInfo#annotations
     */
    private List<AnnotationInstance> methodAnnotations;
}
