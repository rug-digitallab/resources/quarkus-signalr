package nl.rug.digitallab.quarkus.signalr.extension.deployment.builditems;

import io.quarkus.builder.item.MultiBuildItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.transport.TransportType;

import java.util.Set;

/**
 * This represents a SignalR hub build item.
 * <p>
 * It's produced by the extension and consumed by the recorder.
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public final class SignalRHubBuildItem extends MultiBuildItem {
    /**
     * The class name of the hub.
     */
    private String className;

    /**
     * The path of the hub.
     */
    private String path;

    /**
     * The transports supported by the hub.
     */
    private Set<TransportType> transports;
}
