package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.stream;

import com.fasterxml.jackson.databind.JavaType;
import io.smallrye.mutiny.subscription.MultiEmitter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SessionStreamManager;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

/**
 * This class represents a stream of items which are received from the client.
 *
 * @param <T> - The type of the items.
 * @see SignalRSession#getStreamManager()
 * @see SessionStreamManager#getIncomingStream(String)
 */
@Getter
@AllArgsConstructor
public class SignalRIncomingStream<T> {
    /** The type of the items. */
    private JavaType itemType;

    /** The emitter which will trigger when an item is received. */
    private MultiEmitter<T> emitter;
}
