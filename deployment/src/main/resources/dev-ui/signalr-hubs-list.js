import {css, html, LitElement} from "lit";
import {columnBodyRenderer} from "@vaadin/grid/lit.js";
import {hubs} from "build-time-data";
import "@vaadin/grid";
import "@vaadin/vertical-layout";
import "qui-badge";
import "qui-ide-link";
import "@vaadin/icon";

/**
 * This component shows the registered hubs in the system.
 */
export class SignalRHubsList extends LitElement {
    /**
     * The styles for this component.
     */
    static styles = css`
        :host {
            display: block;
        }
    `;

    /**
     * The properties for this component.
     */
    static properties = {
        /**
         * The list of hubs to display.
         */
        _hubs: { type: Array },
    };

    /**
     * The constructor for this component.
     */
    constructor() {
        super();

        // Digs them out from the build-time data
        this._hubs = hubs;
    }

    /**
     * Renders the component.
     *
     * @returns - The HTML template for the component.
     */
    render() {
        return html`
            <vaadin-grid .items="${this._hubs}">
                <vaadin-grid-column path="className" header="Class"></vaadin-grid-column>
                <vaadin-grid-column path="path" header="Path"></vaadin-grid-column>
                <vaadin-grid-column header="Transports" ${columnBodyRenderer(this._renderTransports, [])}> </vaadin-grid-column>
            </vaadin-grid>
        `;
    }

    /**
     * This will render the little badges which show up in the table next to each endpoint.
     * @param {*} item - The item to render the transports for. (This matches the SignalRHubBuildItem class)
     * @returns - The HTML template for the transports.
     */
    _renderTransports(item) {
        return html`
            <vaading-vertical-layout>
                ${item.transports.map(
                    (transport) => html`
                        <qui-badge small pill>
                            ${transport
                                .split("_")
                                .map((part) => part.charAt(0).toUpperCase() + part.slice(1).toLowerCase())
                                .join(" ")}
                        </qui-badge>
                    `
                )}
            </vaading-vertical-layout>
        `;
    }
}

// This makes it available as a custom element to the browser
customElements.define("signalr-hubs-list", SignalRHubsList);
