package nl.rug.digitallab.quarkus.signalr.extension.runtime.exceptions.hub;

import lombok.Getter;

/**
 * This exception is thrown when an invocation with a specific id could not be found.
 * <p>
 * If the caller takes care to check the invocation id before sending, this exception should never be triggered.
 */
@Getter
public class InvocationNotFoundException extends RuntimeException {
    private final String invocationId;

    public InvocationNotFoundException(String invocationId) {
        super("Invocation with id %s could not be found".formatted(invocationId));
        this.invocationId = invocationId;
    }
}
