package nl.rug.digitallab.quarkus.signalr.extension.runtime.serialization;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.lang.reflect.Array;
import java.util.*;

/**
 * This class is used to build a {@link JavaType} from a list of classes. This
 * class works on the principle that, due to the fact that the number of
 * generics for a given class is known, the correct {@link JavaType} can be
 * recursively constructed from a flattened list of classes
 */
public class NestedTypeReferenceBuilder {
    /**
     * This is a cache of types that have already been built. The key is simply the
     * list of classes joined by newlines, as this is guaranteed to be unique
     */
    private static Map<String, JavaType> cachedTypes = new HashMap<>();

    private NestedTypeReferenceBuilder() {
        throw new UnsupportedOperationException("This class should not be instantiated!");
    }

    /**
     * This will build a {@link JavaType} from a list of classes. This will
     * recursively
     * build the type from the top of the queue, and will pop the queue as it goes.
     * <p>
     * The first call of this method expects the queue to contain the full list of
     * classes, and the source to be null. The source is only used for error
     * messages.
     *
     * @param types  - The queue of classes to build the type from
     * @param source - The source class, used for error messages
     * @return - The constructed {@link JavaType}
     */
    private static JavaType build(Queue<Class<?>> types, Class<?> source) {
        if (types.isEmpty()) {
            throw new IllegalArgumentException("Incomplete type reference! Missing arguments for " + source);
        }

        // Figure out how many generics the class in the front of the queue has
        Class<?> current = types.poll();
        int paramCount = current.getTypeParameters().length;

        // Base case, this is any class which is not generic
        if (paramCount == 0) {
            return TypeFactory.defaultInstance().constructType(current);
        }

        // Recursive case, build the type parameters
        JavaType[] typeParameters = new JavaType[paramCount];
        for (int i = 0; i < paramCount; i++) {
            typeParameters[i] = build(types, current);
        }

        // Construct the type
        return TypeFactory.defaultInstance().constructParametricType(current, typeParameters);
    }

    /**
     * This will build a {@link JavaType} from a list of classes.
     * <p>
     * As an example, if you have a class
     * <code> Map&lt;String, Foo&lt;T&gt;&gt; </code>,
     * you would pass in a list of classes like so:
     * <code> List.of(Map.class, String.class, Foo.class, T.class) </code>,
     * and it would produce the equivalent of
     * <code>TypeReference&lt;Map&lt;String, Foo&lt;T&gt;&gt;&gt;(){}.getType() </code>
     *
     * @param types - The list of classes to build the type from
     * @return - The constructed {@link JavaType}
     * @throws IllegalArgumentException - If there are too many/too few arguments
     *                                  for the type reference
     */
    public static JavaType build(List<Class<?>> types) {
        // Since newlines (spaces can appear in Kotlin classnames) cannot appear in
        // classnames, this is guaranteed to be unique
        String possibleKey = String.join("\n", types.stream().map(Class::getName).toArray(String[]::new));
        if (cachedTypes.containsKey(possibleKey)) {
            return cachedTypes.get(possibleKey);
        }

        Queue<Class<?>> typesQueue = new ArrayDeque<>(types);
        JavaType type = build(typesQueue, null);

        if (!typesQueue.isEmpty()) {
            throw new IllegalArgumentException("Too many arguments for type reference! Extra arguments: " + typesQueue);
        }

        cachedTypes.put(possibleKey, type);

        return type;
    }

    /**
     * This will build a {@link JavaType} from a list of classes.
     * <p>
     * As an example, if you have a class
     * <code> Map&lt;String, Foo&lt;T&gt;&gt; </code>,
     * you would pass in a list of classes like so:
     * <code> List.of("java.util.Map", "java.lang.String", "a.b.c.Foo", "e.f.g.T") </code>,
     * and it would produce the equivalent of
     * <code>TypeReference&lt;Map&lt;String, Foo&lt;T&gt;&gt;&gt;(){}.getType() </code>
     *
     * @param types - The list of classes to build the type from
     * @return - The constructed {@link JavaType}
     * @throws IllegalArgumentException - If there are too many/too few arguments
     *                                  for the type reference
     */
    public static JavaType buildFromNames(List<String> types) {
        List<Class<?>> calculatedTypes = new ArrayList<>(types.size());
        for (String type : types) {
            try {
                calculatedTypes.add(lookupClassByName(type));
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Could not find class " + type, e);
            }
        }

        return build(calculatedTypes);
    }

    private static Class<?> lookupClassByName(String name) throws ClassNotFoundException {
        if (name.endsWith("[]")) {
            Class<?> componentType = lookupClassByName(name.substring(0, name.length() - 2));
            return Array.newInstance(componentType, 0).getClass();
        }

        return switch (name) {
            case "int" -> int.class;
            case "long" -> long.class;
            case "short" -> short.class;
            case "byte" -> byte.class;
            case "char" -> char.class;
            case "float" -> float.class;
            case "double" -> double.class;
            case "boolean" -> boolean.class;
            default -> Class.forName(name);
        };
    }

    /**
     * This is a convenience method to build a {@link JavaType} from a list of
     * classes. This is equivalent to calling {@link #buildFromNames(List)} with the
     * classes as arguments.
     *
     * @param types - The list of classes to build the type from
     * @return - The constructed {@link JavaType}
     * @throws IllegalArgumentException - If there are too many/too few arguments
     *                                  for the type reference
     * @see #buildFromNames(List)
     */
    public static JavaType buildFromNames(String... types) {
        return buildFromNames(List.of(types));
    }
}
