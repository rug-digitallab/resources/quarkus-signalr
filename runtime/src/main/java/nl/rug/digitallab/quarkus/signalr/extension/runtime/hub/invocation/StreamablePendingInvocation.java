package nl.rug.digitallab.quarkus.signalr.extension.runtime.hub.invocation;


import com.fasterxml.jackson.databind.JavaType;
import io.smallrye.mutiny.Multi;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * This class represents a pending invocation that is waiting for a response.
 *
 * @param <T> - The type of the result of the invocation.
 */
@Getter
@AllArgsConstructor
public class StreamablePendingInvocation<T> {
    private JavaType type;
    private Multi<T> stream;
}
