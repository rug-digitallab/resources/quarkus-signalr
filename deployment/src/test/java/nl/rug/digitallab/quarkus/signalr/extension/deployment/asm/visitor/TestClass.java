package nl.rug.digitallab.quarkus.signalr.extension.deployment.asm.visitor;

import com.fasterxml.jackson.annotation.JsonMerge;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.OptBoolean;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.annotations.ClientMethod;
import nl.rug.digitallab.quarkus.signalr.extension.runtime.session.SignalRSession;

import java.lang.annotation.*;
import java.util.concurrent.CompletableFuture;

/**
 * This class is simply used for testing the generation of client methods.
 */
public abstract class TestClass {
    @ClientMethod
    public abstract void someVoidMethod(SignalRSession session);

    @ClientMethod
    public abstract CompletableFuture<String> someCompletableFutureMethod(SignalRSession session);

    @ClientMethod
    public abstract Uni<String> someUniMethod(SignalRSession session);

    @ClientMethod
    public abstract Multi<String> someMultiMethod(SignalRSession session);

    @ClientMethod
    public abstract Integer someBlockingMethod(SignalRSession session);

    @ClientMethod
    @JsonProperty(value = "someMethodWithAnnotations", defaultValue = "someDefaultValue")
    @JsonMerge(value = OptBoolean.DEFAULT)
    @SomeNestableAnnotation(value = 5, intValues = {1, 2, 3})
    @SomeNestableAnnotationAgain(value = @SomeNestableAnnotation(value = 10, intValues = {4, 5, 6}))
    @JsonSerialize(as = String.class)
    public abstract void someMethodWithAnnotations(
            @SomeNestableAnnotation(value = 5, intValues = {1, 2, 3})
            @SomeNestableAnnotationAgain(
                    value = @SomeNestableAnnotation(value = 10, intValues = {4, 5, 6}),
                    arrays = {
                            @SomeNestableAnnotation(value = 15, intValues = {7, 8, 9}),
                            @SomeNestableAnnotation(value = 20, intValues = {})
                    }
            )
            SignalRSession session
    );

    @Documented
    @Target({ElementType.METHOD, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface SomeNestableAnnotation {
        int value();

        int[] intValues();
    }

    @Documented
    @Target({ElementType.METHOD, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface SomeNestableAnnotationAgain {
        SomeNestableAnnotation value();

        SomeNestableAnnotation[] arrays() default {};
    }

}